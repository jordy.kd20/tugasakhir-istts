<?php

namespace App\Console\Commands;

use App\Models\htransaksi;
use Illuminate\Console\Command;

class cronjobadmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:reminderadmin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $jumlahpesan = htransaksi::where('status_pengiriman',0)->count();
        if($jumlahpesan!=0){
            $message = "Terdapat ".$jumlahpesan." pesanan yang belom diproses. admin diharapkan segera memproses pesanan tersebut" ;
            $number = "+6282231023890";
            $this->sendText($number, $message);
        }
    }

    public function send_request($data, $url){

        $id = "3765";
        $key = "2a81512bb263277eb7ceaba17c86d8389a6b4909";

        $url = $url.'/'.$id.'/'.$key;
        $ch = curl_init( $url );
        curl_setopt( $ch, CURLOPT_POST, 1);
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt( $ch, CURLOPT_HEADER, 0);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
    
        $response = curl_exec( $ch );
       
    }

    public function sendText($number, $message){
        $data['number'] = $number;
        $data['message'] = $message;
        $url = "https://onyxberry.com/services/wapi/api2/sendText";
        return $this->send_request($data, $url);
    }
}
