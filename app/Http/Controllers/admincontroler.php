<?php

namespace App\Http\Controllers;

use App\Models\asset;
use App\Models\dpembelian;
use App\Models\dretur;
use App\Models\dtransaksi;
use App\Models\hpembelian;
use App\Models\hretur;
use App\Models\htransaksi;
use App\Models\kategori;
use App\Models\pegawai;
use App\Models\produk;
use Carbon\Carbon;
use Alert;
use App\Models\pecatatanstok;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use PhpParser\Node\Stmt\Return_;
use Symfony\Component\HttpFoundation\RequestStack;

class admincontroler extends Controller
{
    public function listbarang(){
        $countproduk = produk::count();
        if($countproduk!=0){
            $dataproduk['dataproduk'] = produk::get();
            $namakategori['namakategori'] = kategori::get();
            return view('admin/indexkaryawan',['isi'=> 1])->with($namakategori)->with($dataproduk);
        }
        else{
            $namakategori['namakategori'] = kategori::get();
            return view('admin/indexkaryawan',['isi'=> 0])->with($namakategori);
            
        }
        
    }

    public function updateproduk($id){
       $dataproduk['dataproduk'] =  produk::get()->where('id_produk',$id);
       $namakategori['namakategori'] = kategori::get();
       $fotoproduk = produk::where('id_produk',$id)->value('foto_produk');
       return view("admin/updateproduk",["foto"=>$fotoproduk])->with($dataproduk)->with($namakategori);
    }
    
    public function submitupdateproduk(Request $req){
        $idproduk = $req->id_produk;
        $warnalampu = $req->warna_lampu;
        $flux = $req->flux_produk;
        $stok = $req->stok_produk;
        $jumlahperdus = $req->jumlahperdus;
        $harga = $req->harga_produk;
        $beratproduk =$req->berat_produk;

        produk::where('id_produk',$idproduk)->update([
            'warna_lampu' => $warnalampu,
            'flux' => $flux,
            'stok_produk' => $stok,
            'jumlahperdus' => $jumlahperdus,
            'harga_produk' => $harga,
            'berat_produk' => $beratproduk
        ]);
        return redirect('/admin');
    }

    public function disabledproduk($id){
        produk::where('id_produk',$id)->update(['status_produk'=>1]);
        return redirect('/admin');
    }

    public function aktifproduk($id){
        produk::where('id_produk',$id)->update(['status_produk'=>0]);
        return redirect('/admin');
    }

    public function listasset(){

        $countassset = asset::get()->count();
        if($countassset!=0){
            $dataasset['dataasset'] = asset::get();
            return view('admin/listasset',["isi"=>1])->with($dataasset);
        }
        else{
            return view('admin/listasset',["isi"=>0]);
        }

        
    }


    public function tambahasset(Request $req){
        $namaaset = $req->nama_asset;
        $jumlahasset = $req->jumlahasset;
        $harga = $req->harga_asset;
        $keadaanasset = $req->keadaan;
        $keterangan = $req->keterangan_asset;


        $assetbaru = new asset;
        $assetbaru->nama_asset = $namaaset;
        $assetbaru->jumlah_asset = $jumlahasset;
        $assetbaru->harga_asset = $harga;
        $assetbaru->keadaan_asset = $keadaanasset;
        $assetbaru->keterangan_asset = $keterangan;
        $assetbaru->save();

        if(Session::has('loginadmin')){
            return \redirect('admin/listasset');
        }
        else if(Session::has('loginowner')){
            return \redirect('owner/listasset');
        }
    }

    public function tambahkategori(Request $req){
        $namakategori = $req->nama_kategori;
        
        $cek = kategori::where('nama_kategori',$namakategori)->count();
        if($cek==0){
            $kategoribaru = new kategori;
            $kategoribaru->nama_kategori = $namakategori;
            $kategoribaru->save();
            return redirect('/admin');
        }
        else{
            return redirect('/admin');
        }

    }

    public function tambahbarang(Request $req){
        $namabarang = $req->nama_barang;
        $idbarang = $req->idbarang;
        $hargaproduk = $req->hargaproduk;
        $watt = $req->watt;
        $tegangan = $req->tegangan;
        $warnalampu = $req->warna_lampu;
        $flux = $req->flux;
        $mereklampu = $req->mereklampu;
        $stok = $req->stokbarang;
        $jumlahperdus = $req->jumlahperdus;
        $kategoriproduk = $req->kategori;
        $namafile = "produk/".$idbarang.".".$req->file("foto")->getClientOriginalExtension();
        //$tgl = Carbon::now()->isoFormat("DD/MM/YYYY");
        $req->file("foto")->move(public_path("/produk"),$idbarang.".".$req->file("foto")->getClientOriginalExtension());

        $countproduk = produk::get('id_produk')->where('id_produk',$idbarang)->count();
        if($countproduk==0){
            $newproduk = new produk;
            $newproduk->id_produk = $idbarang;
            $newproduk->nama_produk = $namabarang;
            $newproduk->harga_produk = $hargaproduk;
            $newproduk->daya = $watt;
            $newproduk->tegangan = $tegangan;
            $newproduk->warna_lampu = $warnalampu;
            $newproduk->flux = $flux;
            $newproduk->merek = $mereklampu;
            $newproduk->stok_produk = $stok;
            $newproduk->jumlahperdus = $jumlahperdus;
            $newproduk->kategori_produk = $kategoriproduk;
            $newproduk->berat_produk = $req->beratproduk;
            $newproduk->foto_produk = $namafile;
            $newproduk->status_produk = 1;
            $newproduk->save();
            return redirect('/admin');
        }
        else{
            return redirect('/admin');
        }
    }

    public function searchproduk(Request $req){
        $text = $req->inputsearch;
        
        if($text!=""){
            $dataproduk['dataproduk'] = DB::table('produk')->where('nama_produk','like','%'.$text.'%')->get();
        }
        else{
            $dataproduk['dataproduk'] = produk::get();
        }
        $countproduk = produk::count();
        $namakategori['namakategori'] = kategori::get();
        if($countproduk!=0){
            return view('admin/indexkaryawan',['isi'=> 1])->with($namakategori)->with($dataproduk);
        }
        else{
            return view('admin/indexkaryawan',['isi'=> 0])->with($namakategori);
        }
    }

    public function updateasset($id){
        $dataasset['dataasset'] = asset::get()->where('id_asset',$id);
        return view("/admin/updateasset")->with($dataasset);
    }

    public function submitasset(Request $req){
        $idasset = $req->id_asset;
        $jumlahasset = $req->jumlahasset;
        $hargaasset = $req->harga_asset;
        $keadaan = $req->keadaan;
        $keterangan = $req->keterangan_asset;

        asset::where('id_asset',$idasset)->update([
            'jumlah_asset' => $jumlahasset,
            'harga_asset'  => $hargaasset,
            'keadaan_asset'=> $keadaan,
            'keterangan_asset'=> $keterangan
        ]);

        if(Session::has('loginadmin')){
            return \redirect('admin/listasset');
        }
        else if(Session::has('loginowner')){
            return \redirect('owner/listasset');
        }
       
    }

    public function searchasset(Request $req){
        $text = $req->inputsearch;
        if($text!=""){
            $dataasset['dataasset'] = DB::table('asset')->where('nama_asset','like','%'.$text.'%')->get();
        }
        else{
            $dataasset['dataasset'] = asset::get();
        }
        $countassset = asset::get()->count();
        if($countassset!=0){
            
            return view('admin/listasset',["isi"=>1])->with($dataasset);
        }
        else{
            return view('admin/listasset',["isi"=>0]);
        }
    }

    public function pembelianproduk(){

        $produk['dataproduk'] = produk::get();
        $kategori['datakategori'] = kategori::get();

        
        return view('admin/pembelianproduk')->with($produk)->with($kategori);
    }
    
    public function cekpembelian(){
        dd(Session::get('pembelian'));
    }

    public function addbeliproduk(Request $req){
        $id_produk = $req->pilih_produk;
        $jumlahpembeliaan = $req->jumlah_pembelian_produk;
        $pembelian = Session::get('pembelian');
        $jumlahperdus = produk::select('jumlahperdus')->where('id_produk',$id_produk)->first();
        if(isset($pembelian[Session::get('active')])){
            if(isset($pembelian[Session::get('active')][$id_produk])){
                $pembelian[Session::get('active')][$id_produk]['qtypembelian'] += $jumlahpembeliaan;   
            }else{
                $pembelian[Session::get('active')][$id_produk] = array('id_produk' => $id_produk,'jumlahperdus' => $jumlahperdus->jumlahperdus,'qtypembelian' => $jumlahpembeliaan);
            }
        }else{
            $pembelian[Session::get('active')][$id_produk] = array('id_produk' => $id_produk,'jumlahperdus' => $jumlahperdus->jumlahperdus, 'qtypembelian' => $jumlahpembeliaan );
        }
        Session::put('pembelian',$pembelian);
        return $this->pembelianproduk();

        // dd($id_produk);

    }

    public function editpembelian(Request $req){
        if($req->jumlah_dus != 0){
            $pembelian = Session::get('pembelian');
             //$stokproduk->stok_produk;
            $stokproduk = produk::select('stok_produk')->where('id_produk',$req->id_produk)->first();
            $jumlahperdus = $pembelian[Session::get('active')][$req->id_produk]['jumlahperdus'];
            $jumlahbeli = $req->jumlah_dus * $jumlahperdus;
            $pembelian[Session::get('active')][$req->id_produk]['qtypembelian'] = $req->jumlah_dus;
            Session::put('pembelian', $pembelian);
            return $this->pembelianproduk();
        }
        else{
            //tidak boleh kosong
            //dd('stok');
            return $this->pembelianproduk();
        }
    }

    public function hapuspembelian($id){
        $data = Session::get('pembelian');
        unset($data[Session::get('active')][$id]);
        Session::forget('pembelian');
        Session::put('pembelian',$data);
        return $this->pembelianproduk();
    }

    public function checkoutpembelian(Request $req){
        // $cekhpembelian = hpembelian::count();
        
            $tempId = "PK";
            $temp = explode("_",DB::table('hpembelian')->where("id_hpembelian","like","%".$tempId."%")->max('id_hpembelian'));
            $countData = $temp[1]+1;
            if($countData < 10){
                $tempId = $tempId."_00".$countData;
            }else if($countData < 100){
                $tempId = $tempId."_0".$countData;
            }else{
                $tempId = $tempId."_".$countData;
            }
        
       
        $data = Session::get('pembelian');
        foreach($data as $key => $item){
            if($key == Session::get('active')){
                foreach($item as $datapembelian){
                    $totalbeli = 0;
                    $newdpembelian = new dpembelian;
                    $newdpembelian->id_hpembelian = $tempId;
                    $newdpembelian->id_produk = $datapembelian['id_produk'];
                    $newdpembelian->qtypembelian = $datapembelian['qtypembelian'];
                    $totalbeli = $datapembelian['qtypembelian'] * $datapembelian['jumlahperdus'];
                    $newdpembelian->qtytotal = $totalbeli;
                    $newdpembelian->save();
                }
            }
        }
            $newhpembelian = new hpembelian;
            $newhpembelian->id_hpembelian = $tempId;
            $newhpembelian->tgl_pembelian = Carbon::now();
            $newhpembelian->userpegawai = "4";
            $newhpembelian->keterangan = "";
            $newhpembelian->status = 0;
            $newhpembelian->save();
    

        unset($data[Session::get('active')]);
        Session::forget('pembelian');
        Session::put('pembelian',$data);
        return redirect('/admin/listpembelian');
       
    }

    public function listpembelian(){
        //status 0 barang belom diterima
        //status 1 barang diterima digudang
        //pegawai yang tertulis wajib menerima dan mengecek kelengkapan barang
        //yang dipesan
        $hpembelian['hpembelian'] = hpembelian::where('status',0)->get(); 
        // $tgl = hpembelian::where('id_hpembelian','PK_003')->value('tgl_pembelian');
        
        
        return view('admin/listpembelian',["isi"=>1])->with($hpembelian);
    }

    public function detailpembelian($id){
        $datadpembelian['datadpembelian'] = dpembelian::where('id_hpembelian',$id)->get();
        return view('admin/detailpembelian',['idhpembelian'=>$id])->with($datadpembelian);
    }

    public function printdetailpembelian($id){
        // return view('admin/invoicepenerimaanbarang');
        $tglpembelian = hpembelian::where('id_hpembelian',$id)->value('tgl_pembelian');
        $datadpembelian['datapembelian'] = dpembelian::where('id_hpembelian',$id)->get();
        return view('admin/invoicepenerimaanbarang',['idhpembelian'=>$id,'tglpembelian'=>$tglpembelian])->with($datadpembelian);
    }

    public function updatestatuslistpembelian($id){

        $dataproduk = dpembelian::where('id_hpembelian',$id)->get();
        foreach($dataproduk as $item){

            $newpecatatan = new pecatatanstok;
            $newpecatatan->id_produk = $item->id_produk;
            $newpecatatan->jumlah = $item->qtytotal;
            $newpecatatan->keterangan = "Penambahan Produk dari pembelian";
            $newpecatatan->created_at = Carbon::now();
            $newpecatatan->status = 1;
            $newpecatatan->save();

            $stok = produk::where('id_produk',$item->id_produk)->value('stok_produk');
            $tambahproduk = $stok + $item->qtytotal;
            produk::where('id_produk',$item->id_produk)->update(['stok_produk'=>$tambahproduk]);  
        }
        hpembelian::where('id_hpembelian',$id)->update(['status'=>1]);
        Alert::success('Berhasil', 'Berhasil Update Pembelian' );
        return redirect('/admin/listpembelian');
        

    }

    public function listpembeliandistributor(){

        $counthdistributor = htransaksi::count();
        if($counthdistributor!=0){
            $hproses['hproses'] = htransaksi::where('status_pengiriman', 0)->get();
            $hdikirim['hdikirim'] = htransaksi::where('status_pengiriman', 1)->get();
            $hselesai['hselesai'] = htransaksi::where('status_pengiriman', 2)->get();

            return view('admin/listpembeliandistributor',['counthdistributor'=>$counthdistributor])->with($hproses)->with($hdikirim)->with($hselesai);
        }
        else{
            return view('admin/listpembeliandistributor',['counthdistributor'=>$counthdistributor]);
        }
        // return view('admin/listpembeliandistributor');
    }

    public function detailpesanan($id){

        $dtrans['datadtrans'] = dtransaksi::where('id_htransaksi',$id)->get();
        $grandtotal = htransaksi::where('id_htransaksi',$id)->value('grand_total');
        $ongkir = htransaksi::where('id_htransaksi',$id)->value('hargaongkir');
        $paymentmethod = htransaksi::where('id_htransaksi',$id)->value('methodpembayaran');
        $fotopembayaran = htransaksi::where('id_htransaksi',$id)->value('fotopembayaran');
        return view('detailpesananproduk',['grandtotal'=>$grandtotal, 'payment'=>$paymentmethod, 'fotopembayaran'=>$fotopembayaran,'hargaongkir'=>$ongkir,'idhtrans'=> $id])->with($dtrans);

    }

    public  function detailpesananhutang($id){
        $dtrans['datadtrans'] = dtransaksi::where('id_htransaksi',$id)->get();
        $grandtotal = htransaksi::where('id_htransaksi',$id)->value('grand_total');
        $ongkir = htransaksi::where('id_htransaksi',$id)->value('hargaongkir');
        $paymentmethod = htransaksi::where('id_htransaksi',$id)->value('methodpembayaran');
        $fotopembayaran = htransaksi::where('id_htransaksi',$id)->value('fotopembayaran');
        
        return view('admin/detailpesananhutang',['grandtotal'=>$grandtotal, 'payment'=>$paymentmethod, 'fotopembayaran'=>$fotopembayaran,'hargaongkir'=>$ongkir,'idhtrans'=> $id])->with($dtrans);
    }

    public function statuspengiriman($idhtrans,$status){
        if($status == 1){
            if(Session::has('loginadmin')){
                $barang = dtransaksi::where('id_htransaksi',$idhtrans)->get();
                foreach($barang as $item){
                    $stokproduk = produk::where('id_produk', $item->id_produk)->value('stok_produk');
                    $kurangstok = $stokproduk - $item->totalqty;
                    produk::where('id_produk', $item->id_produk)->update(['stok_produk' => $kurangstok]);

                    $pencatatanstok = new pecatatanstok;
                    $pencatatanstok->id_produk = $item->id_produk;
                    $pencatatanstok->jumlah = $item->totalqty;
                    $pencatatanstok->keterangan = "dibeli Customer";
                    $pencatatanstok->created_at = Carbon::now();
                    $pencatatanstok->status = 0;
                    $pencatatanstok->save();
                }
                htransaksi::where('id_htransaksi',$idhtrans)->update(['status_pengiriman'=>$status]);
                return redirect('/admin/listpembeliandistributor');
            }
            
        }
        else{
            htransaksi::where('id_htransaksi',$idhtrans)->update(['status_pengiriman'=>$status]);
            if(Session::has('loginadmin')){
                return redirect('/admin/listpembeliandistributor');
            }
            else if(Session::has('distributoractive')){
                return redirect('/distributor/pembelian');
            }
        }
       
       
    }

    public function listhutang(){

        $counthutang = htransaksi::where('status_bayar',0)->count();

        if($counthutang!=0){
            $listhutang["listhutang"] = htransaksi::where('status_bayar',0)->where('status_pengiriman','>',0 )->where('status_pengiriman','<',3 )->get();
            return view('admin/listhutangdistributor',['count'=>$counthutang])->with($listhutang);
        }
        else{
            return view('admin/listhutangdistributor',['count'=>$counthutang]);
        }
    }

    public function listretur(){

        $countretur = hretur::count();
        if($countretur!=0){
            $hreturproses["hreturproses"] = hretur::where('status_pengiriman',0)->get();
            $hreturdikirim["hreturdikirim"] = hretur::where('status_pengiriman',1)->get();
            $hreturselesai['hreturselesai'] =hretur::where('status_pengiriman',2)->get();
            return view('/admin/listreturdistributor',['countretur'=>$countretur])->with($hreturproses)->with($hreturdikirim)->with($hreturselesai);
        }
        else{
            return view('/admin/listreturdistributor',['countretur'=>$countretur]);
        }    
    }

    public function detailretur($id){
        $dretur['dataretur'] = dretur::where('id_hretur',$id)->get();
        $idhretur = $id;
        return view('detailretur',['idhretur'=>$idhretur])->with($dretur);
    }

    public function statuspengirimanretur($idhretur,$status){
       
        if(Session::has('loginadmin')){
            if($status == 1){
                $idhtrans = hretur::where('id_hretur',$idhretur)->value('id_htransaksi');
                $barang = dretur::where('id_hretur',$idhretur)->get();
                foreach($barang as $item){
                    $stokproduk = produk::where('id_produk', $item->id_produk)->value('stok_produk');
                    $kurangstok = $stokproduk - $item->qtyretur;
                    produk::where('id_produk', $item->id_produk)->update(['stok_produk' => $kurangstok]);

                    $pencatatanstok = new pecatatanstok;
                    $pencatatanstok->id_produk = $item->id_produk;
                    $pencatatanstok->jumlah = $item->qtyretur;
                    $pencatatanstok->keterangan = "Retur id ".$idhtrans." produk Customer";
                    $pencatatanstok->created_at = Carbon::now();
                    $pencatatanstok->status = 0;
                    $pencatatanstok->save();
                }
                $idhtrans = hretur::where('id_hretur',$idhretur)->value('id_htransaksi');
                htransaksi::where('id_htransaksi',$idhtrans)->update(['status_retur'=>1]);
                hretur::where('id_hretur',$idhretur)->update(['status_pengiriman'=>$status]);
                return redirect('/admin/listretur');
            }
            else{
                hretur::where('id_hretur',$idhretur)->update(['status_pengiriman'=>$status]);
                return redirect('/admin/listretur');
            }
            
        }
        else if(Session::has('distributoractive')){
            hretur::where('id_hretur',$idhretur)->update(['status_pengiriman'=>$status]);
            return redirect('/distributor/listretur');
        }
    }

    public function updatepembayaranhutang(Request $req){
        $idnota = $req->id_nota;

        $kurangbayar = $req->kurang_bayar;
        $jumlahbayar = $req->jumlahdibayarkan;
        if(file_exists($req->file('fotopembayaran'))){
            if($jumlahbayar<$kurangbayar){
                // dd("kurang");
                $kurangbayarnew = $kurangbayar-$jumlahbayar;
                $namafile = "buktibayar/".$idnota.".".$req->file("fotopembayaran")->getClientOriginalExtension();
                $req->file("fotopembayaran")->move(public_path("/buktibayar"),$idnota.".".$req->file("fotopembayaran")->getClientOriginalExtension());
                htransaksi::where('id_htransaksi',$idnota)->update(['kurang_bayar'=>$kurangbayarnew,'fotopembayaran'=>$namafile]);
                Alert::success('Berhasil', 'Berhasil Update Pembayaran' );
                return redirect('/admin/listhutang');
                
            }
            else if($jumlahbayar>$kurangbayar){
                // dd("kelebihan");
                Alert::warning('Pembayaran', 'Jumlah yang dibayarkan lebih besar' );
                return redirect('/admin/listhutang');
               
            }
            else if($jumlahbayar==$kurangbayar){
                $kurangbayarnew = $kurangbayar-$jumlahbayar;
                $namafile = "buktibayar/".$idnota.".".$req->file("fotopembayaran")->getClientOriginalExtension();
                $req->file("fotopembayaran")->move(public_path("/buktibayar"),$idnota.".".$req->file("fotopembayaran")->getClientOriginalExtension());               
                htransaksi::where('id_htransaksi',$idnota)->update(['kurang_bayar'=>$kurangbayarnew,'status_bayar'=>1,'fotopembayaran'=>$namafile]);
                Alert::success('Berhasil', 'Berhasil Melakukan Pembayaran' );
                return redirect('/admin/listhutang');
            };
        }
        else{
            if($jumlahbayar<$kurangbayar){
                // dd("kurang");
                $kurangbayarnew = $kurangbayar-$jumlahbayar;
                htransaksi::where('id_htransaksi',$idnota)->update(['kurang_bayar'=>$kurangbayarnew]);
                Alert::success('Berhasil', 'Berhasil Update Pembayaran' );
                return redirect('/admin/listhutang');
                
            }
            else if($jumlahbayar>$kurangbayar){
                // dd("kelebihan");
                Alert::warning('Pembayaran', 'Jumlah yang dibayarkan lebih besar' );
                return redirect('/admin/listhutang');
               
            }
            else if($jumlahbayar==$kurangbayar){
                $kurangbayarnew = $kurangbayar-$jumlahbayar;
                htransaksi::where('id_htransaksi',$idnota)->update(['kurang_bayar'=>$kurangbayarnew,'status_bayar'=>1]);
                Alert::success('Berhasil', 'Berhasil Melakukan Pembayaran' );
                return redirect('/admin/listhutang');
            };
        }
    }

    public function penguranganstok(){

        $datakategori['datakategori'] = kategori::get();
        $dataproduk['dataproduk'] = produk::where('status_produk',1)->get();

        return view('/admin/penguranganstok')->with($datakategori)->with($dataproduk);
    }

    public function submitpenguranganproduk(Request $req){
        
        $idproduk = $req->pilih_produk;
        $keterangan = $req->keterangan;
        $jumlah = $req->jumlah_pengurangan_produk;

        $stokproduk = produk::where('id_produk',$idproduk)->value('stok_produk');
        if($jumlah<=$stokproduk){

            $newpecatatan = new pecatatanstok;
            $newpecatatan->id_produk = $idproduk;
            $newpecatatan->jumlah = $jumlah;
            $newpecatatan->keterangan = $keterangan;
            $newpecatatan->created_at = Carbon::now();
            $newpecatatan->status = 0;
            $newpecatatan->save();

            $kurangproduk = $stokproduk-$jumlah;

            produk::where('id_produk',$idproduk)->update(['stok_produk'=>$kurangproduk]);


            Alert::success('Berhasil', 'Berhasil Mengurangi Produk' );
            return redirect('/admin/penguranganstok');
        }
        else{
            Alert::error('Erorr', 'Jumlah Stok Tidak Cukup' );
            return redirect('/admin/penguranganstok');
        }


    }


    public function cetaknotatransaksi($id){

        $datadtrans['datadtrans'] = dtransaksi::where('id_htransaksi',$id)->get();
        $status = htransaksi::where('id_htransaksi',$id)->value('status_bayar');
        $subtotal = htransaksi::where('id_htransaksi',$id)->value('grand_total');
        return view('/admin/notainvoicepenjualan',['id'=>$id,'status'=>$status,'subtotal'=>$subtotal])->with($datadtrans);

    }

    public function filtertgltransaksi(Request $req){
        $tgldari = $req->tgldari;
        $tglsampai = $req->tglsampai;
        if($tglsampai>$tgldari){
            $counthdistributor = htransaksi::count();
            if($counthdistributor!=0){
                $hproses['hproses'] = htransaksi::where('status_pengiriman', 0)->orderByDesc('id_htransaksi')->get();
                $hdikirim['hdikirim'] = htransaksi::where('status_pengiriman', 1)->get();
                $hselesai['hselesai'] = htransaksi::whereBetween('tgl_transaksi', [$tgldari, $tglsampai])->where('status_pengiriman', 2)->get();
              
                return view('admin/listpembeliandistributor',['counthdistributor'=>$counthdistributor])->with($hproses)->with($hdikirim)->with($hselesai);
            }
            else{
                return view('admin/listpembeliandistributor',['counthdistributor'=>$counthdistributor]);
            }
        }
        else{
            $counthdistributor = htransaksi::count();
            $hproses['hproses'] = htransaksi::where('status_pengiriman', 0)->get();
            $hdikirim['hdikirim'] = htransaksi::where('status_pengiriman', 1)->get();
            $hselesai['hselesai'] = htransaksi::where('status_pengiriman', 2)->get();
            Alert::error('Error','Input Tanggal Salah');
            return view('admin/listpembeliandistributor',['counthdistributor'=>$counthdistributor])->with($hproses)->with($hdikirim)->with($hselesai);
           
        }
    }

    public function filtertglpembelian(Request $req){
        $tgldari = $req->tgldari;
        $tglsampai = $req->tglsampai;
        if($tglsampai>=$tgldari){
            $hpembelian['hpembelian'] = hpembelian::whereBetween('tgl_pembelian',[$tgldari,$tglsampai])->where('status',0)->get(); 
            return view('admin/listpembelian',["isi"=>1])->with($hpembelian);
           
        }
        else{
            $hpembelian['hpembelian'] = hpembelian::where('status',0)->get();
            Alert::error('Error','Input Tanggal Salah'); 
            return view('admin/listpembelian',["isi"=>1])->with($hpembelian);
           
        }


    }

    public function cetaknotaretur($id){
        $datadretur['datadretur'] = dretur::where('id_hretur',$id)->get();
        return view('/admin/notacetakretur',['id'=>$id])->with($datadretur);
    }

    public function stokreport(){
        $datastok['pencatatanstok'] = pecatatanstok::get();
        return view('/admin/stokreport',['isi'=>1])->with($datastok);
    }


    public function filtertgltstok(Request $req){
        $tgldari = $req->tgldari;
        $tglsampai = $req->tglsampai;

        if($tglsampai>=$tgldari){
            $datastok['pencatatanstok'] = pecatatanstok::whereBetween('created_at',[$tgldari,$tglsampai])->get();
            return view('/admin/stokreport',['isi'=>1,'tgldari'=>$tgldari, 'tglsampai'=>$tglsampai])->with($datastok);
        }
        else{
            $datastok['pencatatanstok'] = pecatatanstok::get();
            Alert::Error('Error','Input Tanggal Salah');
            return view('/admin/stokreport',['isi'=>1])->with($datastok);
        }
    }


}
