<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Courier;
use App\Models\distributor;
use App\Models\produk;
use App\Models\Province;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Kavist\RajaOngkir\Facades\RajaOngkir;
use Monolog\Handler\ElasticsearchHandler;
// use RealRashid\SweetAlert\Facades\Alert;
use Alert;
use App\Models\dretur;
use App\Models\dtransaksi;
use App\Models\hpembelian;
use App\Models\hretur;
use App\Models\htransaksi;
use App\Models\kategori;
use App\Models\pecatatanstok;
use Illuminate\Support\Carbon;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Crypt;
use Monolog\Handler\IFTTTHandler;
use RealRashid\SweetAlert\Facades\Alert as FacadesAlert;

class distributorcontroler extends Controller
{
    public function logoutdistributor()
    {
        Session::remove('googleid');
        Session::remove('distributoractive');
        Session::remove('logindistributor');
        Session::remove('distributorid');
        Session::remove('distributorfoto');
        return redirect('/');
    }


    public function profile()
    {
        $userid = Session::get('distributorid');
        if (isset($userid)) {
            $dataprofile['dataprofile'] = distributor::where('id_user', $userid)->get();
            return view('distributor/updateprofiledistributor')->with($dataprofile);
        }
    }

    public function updateprofile(Request $req)
    {
        $cekidgoogle = distributor::Select('id_google')->where('id_user', Session::get('distributorid'))->first();
        $nomor = $req->nomor_distributor;
        if (substr(trim($nomor), 0, 3) == '+62') {
            $nomorfix = $nomor;
        } else if (substr(trim($nomor), 0, 1) == '0') {
            $nomorfix = '+62' . substr(trim($nomor), 1);
        }
        if (isset($cekidgoogle['id_google'])) {

            distributor::where('id_user', Session::get('distributorid'))->update(['nama_user' => $req->nama_distributor, 'nama_toko' => $req->nama_toko, 'email_user' => $req->email_distributor, 'no_tlp_distributor' => $nomorfix, 'alamat_toko' => $req->alamat_distributor]);
            return redirect('/distributor/indexdistributor');
        } else {
            distributor::where('id_user', Session::get('distributorid'))->update(['nama_user' => $req->nama_distributor, 'nama_toko' => $req->nama_toko, 'email_user' => $req->email_distributor, 'no_tlp_distributor' => $nomorfix, 'alamat_toko' => $req->alamat_distributor]);
            return redirect('/distributor/indexdistributor');
        }
    }

    public function registrasidistributor(Request $req)
    {
        // dd($req->all());
        $validasi = $req->validate(
            [
                'nama_distributor' => 'required',
                'nama_toko' => 'required',
                'email_distributor' => 'required|email',
                'alamat_distributor' => 'required',
                'Nomor_distributor' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:12|max:14',
                'password_repeat'=>'same:password'
            ],
            [
                'Nomor_distributor' => 'harus berisi 12 angka dimulai dari 08',
                'password_repeat'=>'harus sama dengan password'
        ]);

        $pass = $req->password;
        $cpass = $req->password_repeat;

        $count = distributor::where('email_user', $req->email_distributor)->count();
        if ($count == 0) {
            if ($pass == $cpass) {
                $nama = $req->nama_distributor;
                $namatoko = $req->nama_toko;
                $email = $req->email_distributor;
                $alamat = $req->alamat_distributor;
                $nomor = $req->Nomor_distributor;
                $passbaru = Hash::make($pass);
                if (substr(trim($nomor), 0, 3) == '+62') {
                    $nomorfix = $nomor;
                } else if (substr(trim($nomor), 0, 1) == '0') {
                    $nomorfix = '+62' . substr(trim($nomor), 1);
                }
                $newdistributor = new distributor;
                $newdistributor->email_user = $email;
                $newdistributor->nama_user = $nama;
                $newdistributor->password_user = $passbaru;
                $newdistributor->nama_toko = $namatoko;
                $newdistributor->no_tlp_distributor = $nomorfix;
                $newdistributor->alamat_toko = $alamat;
                $newdistributor->foto_user = "asset/img/avatars/avatar1.jpeg";
                $newdistributor->status_user = 1;
                $newdistributor->save();
                Alert::success('success', 'berhasil register');
                return redirect("/");
            } else {
                Alert::error('Error', 'password tidak sama');
                return redirect("/distributor/register");
            }

            // Session::put('distributoractive',$nama);
            // return $this->berhasillogingoogle();
        } else {
            Alert::error('Error', 'Email Telah Digunakan');
            return redirect("/distributor/register");
        }
    }

    public function loginemail(Request $req)
    {

        $cekid = distributor::select('id_user')->where("email_user", $req->email)->where('status_user',1)->count();
        if ($cekid != 0) {
            $passuser = distributor::select('password_user')->where("email_user", $req->email)->first();
            if (Hash::check($req->password, $passuser->password_user)) {
                $cekgoogle = distributor::where("email_user", $req->email)->value('id_google');
                if ($cekgoogle == "") {
                    $namauser = distributor::select('nama_user')->where("email_user", $req->email)->first();
                    Session::put('distributoractive', $namauser->nama_user);
                    return redirect('/distributor/indexdistributor');
                } else {
                    Alert::error('Error', 'Login Menggunakan Google');
                    return redirect("/");
                }
            } else {
                // $valuetgl = hpembelian::where('id_hpembelian','PK_001')->value('tgl_pembelian');
                // Alert::error('Error', 'Password anda salah '. date('d F Y', strtotime($valuetgl)));
                //tambah tgl
                // Carbon::now()->addMonths(2)->isoFormat('DD MMMM Y')
                Alert::error('Error', 'Password anda salah ');
                return redirect("/");
            }
        } else {
            Alert::error('Error', 'Password anda salah');
            return redirect("/");
        }
    }

    public function berhasillogingoogle()
    {

        $idgoogleuser = Session::get('googleid');
        $datakategori['kategori'] = kategori::get();

        if (isset($idgoogleuser)) {
            
            $findfoto = distributor::Select('foto_user')->Where('id_google', $idgoogleuser)->first();
            $nama = distributor::Select('nama_user')->Where('id_google', $idgoogleuser)->first();
            $userid = distributor::Select('id_user')->Where('nama_user', $nama['nama_user'])->first();
            Session::put('distributoractive', $nama['nama_user']);
            Session::put('distributorid', $userid['id_user']);
            Session::put('distributorfoto', $findfoto->foto_user);
        } else {
            $namadisemail = Session::get('distributoractive');
            $userid = distributor::Select('id_user')->Where('nama_user', $namadisemail)->value('id_user');
            $findfoto = distributor::Select('foto_user')->Where('id_user', $userid)->value('foto_user');
            Session::put('distributorid', $userid);
            Session::put('distributorfoto', $findfoto);
            // dd($findfoto['foto_user']);
        }

        Session::put('logindistributor', 'masuk');
        $countproduk = produk::count();
        if ($countproduk != 0) {
            $dataproduk['dataproduk'] = produk::get()->where('status_produk', 1);
            return view("distributor/indexdistributor", ['foto' => Session::get('distributorfoto'), 'isi' => 1])->with($dataproduk)->with($datakategori);
        } else {
            return view("distributor/indexdistributor", ['foto' => $findfoto, 'isi' => 0])->with($datakategori);
        }
    }

    public function cekcart()
    {
        return Session::get('cart');
    }

    public function cart()
    {
        $couriers = Courier::pluck('title', 'code');
        $provinces = Province::pluck('title', 'province_id');

        return view('distributor/cart', compact('couriers', 'provinces'), ["isi" => 0]);
    }

    public function getCities($id)
    {
        $city = City::where('province_id', $id)->pluck('title', 'city_id');
        return json_encode($city);
    }


    public function detailproduk($id)
    {

        $idproduk = $id;
        $nama_produk = produk::where('id_produk', $id)->value('nama_produk');
        $harga_produk = produk::where('id_produk', $id)->value('harga_produk');
        $daya = produk::where('id_produk', $id)->value('daya');
        $warna_lampu = produk::where('id_produk', $id)->value('warna_lampu');
        $merek = produk::where('id_produk', $id)->value('merek');
        $stok_produk = produk::where('id_produk', $id)->value('stok_produk');
        $jumlahperdus = produk::where('id_produk', $id)->value('jumlahperdus');
        $foto_produk = produk::where('id_produk', $id)->value('foto_produk');

        return view('/distributor/detailproduk', [
            'id_produk' => $id, 'nama_produk' => $nama_produk,
            'harga_produk' => $harga_produk, 'daya' => $daya, 'warna_lampu' => $warna_lampu,
            'merek' => $merek, 'stok_produk' => $stok_produk, 'jumlahperdus' => $jumlahperdus, 'foto_produk' => $foto_produk
        ]);
    }


    public function jqongkir($city,$courierid,$berat){

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.rajaongkir.com/starter/cost",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "origin=501&destination=114&weight=1700&courier=jne",
        CURLOPT_HTTPHEADER => array(
            "content-type: application/x-www-form-urlencoded",
            "key: 9c7c9beaade1292e3190b779ad028986"
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
            echo "cURL Error #:" . $err;
            } else {
            // 
            $temp = (array)json_decode($response,true)["rajaongkir"]["results"];
            $temp1 = $temp['0']['costs'];
            dd($temp1);
        }

        // $citiid = City::where('title',$city)->value('city_id');
        // $cost= RajaOngkir::ongkosKirim([
        //     'origin'        => 444,     // ID kota/kabupaten asal
        //     'destination'   => $citiid,      // ID kota/kabupaten tujuan
        //     'weight'        => $berat,   // berat barang dalam gram
        //     'courier'       => $courierid,    // kode kurir pengiriman: ['jne', 'tiki', 'pos'] untuk starter
        // ])->get();

        // $ongkir= $cost[0]['costs'];

        // return json_encode($ongkir);
    }

    public function submitongkir(Request $req)
    {
        $couriers = Courier::pluck('title', 'code');
        $provinces = Province::pluck('title', 'province_id');
        $citiid = City::where('title', $req->city_destination)->value('city_id');

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.rajaongkir.com/starter/cost",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "origin=444&destination=".$citiid."&weight=".$req->totalberat."&courier=".$req->courier,
        CURLOPT_HTTPHEADER => array(
            "content-type: application/x-www-form-urlencoded",
            "key: 9c7c9beaade1292e3190b779ad028986"
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
            echo "cURL Error #:" . $err;
            } else {
            // 
            $temp = (array)json_decode($response,true)["rajaongkir"]["results"];
            $ongkir['costongkir'] = $temp['0']['costs'];
           
        }
        return view('distributor/cart', compact('couriers', 'provinces'), ["isi" => 1])->with($ongkir);
        //    dd($cost);

        // dd($citiid);
    }


    public function addtocart($id)
    {
        //pengecekan apakah produk stok cukup atau tidak
        $cart = Session::get('cart');
        $namaproduk = produk::where('id_produk', $id)->value('nama_produk');
        $jumlahperdus = produk::select('jumlahperdus')->where('id_produk', $id)->first();
        $harga = produk::select('harga_produk')->where('id_produk', $id)->first();
        $jumlahstok = produk::where('id_produk', $id)->value('stok_produk');
        if ($jumlahperdus->jumlahperdus < $jumlahstok) {
            if (isset($cart[Session::get('distributoractive')])) {
                if (isset($cart[Session::get('distributoractive')][$id])) {
                    $tempdus = $cart[Session::get('distributoractive')][$id]['qtylusin'] + 1;
                    $tempjumlah = $tempdus * $jumlahperdus->jumlahperdus;
                    if($jumlahstok>=$tempjumlah){
                        $cart[Session::get('distributoractive')][$id]['qtylusin'] += 1;
                    }
                    else{
                        Alert::error('Error', 'Stok Produk Tidak Cukup ' . $namaproduk);
                        return $this->berhasillogingoogle();
                    }   
                } else {
                    $cart[Session::get('distributoractive')][$id] = array(
                        'id_produk' => $id, 'harga_produk' => $harga->harga_produk,
                        'jumlahperdus' => $jumlahperdus->jumlahperdus, 'qtylusin' => 1,
                        'qtysatuan' => 0
                    );
                }
            } else {
                $cart[Session::get('distributoractive')][$id] = array(
                    'id_produk' => $id, 'harga_produk' => $harga->harga_produk,
                    'jumlahperdus' => $jumlahperdus->jumlahperdus, 'qtylusin' => 1,
                    'qtysatuan' => 0
                );
            }
            Session::put('cart', $cart);
            Alert::success('Berhasil', 'Berhasil Menambahkan Barang ' . $namaproduk);
            return $this->berhasillogingoogle();
        } else {
            Alert::error('Error', 'Stok Produk Kosong ' . $namaproduk);
            return $this->berhasillogingoogle();
        }
    }

    public function tambahcart($id)
    {
        $cart = Session::get('cart');
        $jumlahstok = produk::where('id_produk', $id)->value('stok_produk');
        $jumlahperdus = produk::select('jumlahperdus')->where('id_produk', $id)->value('jumlahperdus');

        if($jumlahstok>$jumlahperdus){
            $tempdus = $cart[Session::get('distributoractive')][$id]['qtylusin'] + 1;
            $tempjumlah = $tempdus * $jumlahperdus;
            if($jumlahstok>=$tempjumlah){
                $cart[Session::get('distributoractive')][$id]['qtylusin'] += 1;
                Session::put('cart', $cart);
                return $this->cart();
            }
            else{
                Alert::error('Error', 'Stok Produk Tidak Cukup ');
                return $this->cart();
            }   
        }
        else{
            Alert::error('Error', 'Stok Produk Kosong ');
            return $this->cart();
        }
        
    }
    public function kurangcart($id)
    {
        $cart = Session::get('cart');
        if ($cart[Session::get('distributoractive')][$id]['qtylusin'] > 1) {
            $cart[Session::get('distributoractive')][$id]['qtylusin'] -= 1;
            Session::put('cart', $cart);
            return $this->cart();
        } else {
            return $this->cart();
        }
    }

    public function hapuscart($id){

    }

    public function editcart(Request $req)
    {
        if ($req->jumlah_dus != 0 || $req->jumlah_satuan != 0) {
            $cart = Session::get('cart');
            //$stokproduk->stok_produk;
            $stokproduk = produk::select('stok_produk')->where('id_produk', $req->id_produk)->first();
            $jumlahperdus = $cart[Session::get('distributoractive')][$req->id_produk]['jumlahperdus'];
            $jumlahbeli = $req->jumlah_satuan + ($req->jumlah_dus * $jumlahperdus);

            if ($stokproduk->stok_produk > $jumlahbeli) {
                $cart[Session::get('distributoractive')][$req->id_produk]['qtylusin'] = $req->jumlah_dus;
                $cart[Session::get('distributoractive')][$req->id_produk]['qtysatuan'] = $req->jumlah_satuan;
                Session::put('cart', $cart);
                return $this->cart();
            } else {
                //stok tidak cukup
                return $this->cart();
            }
        } else {
            //tidak boleh kosong
            return $this->cart();
        }
    }


    public function checkout(Request $req)
    {
        dd(Session::get('cart'));
        $totalberat = 0;
        $subtotal = 0;
        $hargaongkir = $req->layananpengirim;
        Session::remove('hargaongkir');
        Session::put('hargaongkir', $hargaongkir);
        if (Session::has('cart')) {
            foreach (Session::get('cart') as $key => $item) {
                $subtotal = 0;
                $totalberat = 0;
                if ($key == Session::get('distributoractive')) { 
                    foreach ($item as $databarang) {
                        $totalbarang = 0;
                        $totalharga = 0;
                        $beratbarang = 0;
                        $beratbarang = DB::table('produk')->where('id_produk', $databarang['id_produk'])->value('berat_produk');
                        $totalbarang = $databarang['qtysatuan'] + ($databarang['qtylusin'] * $databarang['jumlahperdus']);
                        $totalharga = $totalbarang * $databarang['harga_produk'];
                        $totalberat += $beratbarang * $totalbarang;
                        $subtotal += $totalharga;
                    }
                }
            }
        }
        $fixtotal = 0;
        $dppaylater = 0;
        $fixtotal = $hargaongkir + $subtotal;
        $dppaylater = $fixtotal * 0.2;

        $iddistributor = Session::get('distributorid');
        $email = distributor::where("id_user", $iddistributor)->value('email_user');
        $no_tlp = distributor::where('id_user', $iddistributor)->value('no_tlp_distributor');

        // Set your Merchant Server Key
        \Midtrans\Config::$serverKey = 'SB-Mid-server-y-Ta1FxB9rbYeBLywM4Jh-o1';
        // Set to Development/Sandbox Environment (default). Set to true for Production Environment (accept real transaction).
        \Midtrans\Config::$isProduction = false;
        // Set sanitization on (default)
        \Midtrans\Config::$isSanitized = true;
        // Set 3DS transaction for credit card to true
        \Midtrans\Config::$is3ds = true;

        $params = array(
            'transaction_details' => array(
                'order_id' => rand(),
                'gross_amount' => $fixtotal,
            ),
            'customer_details' => array(
                'first_name' => Session::get('distributoractive'),
                'last_name' => '',
                'email' => $email,
                'phone' => $no_tlp,
            ),
        );

        $snapToken = \Midtrans\Snap::getSnapToken($params);



        //dd($fixtotal);
        return view("distributor/checkout", ["hargatotal" => $fixtotal, "hargadp" => $dppaylater, "snap_token" => $snapToken]);
    }


    public function hapuscarttrans($id){
        $data = Session::get('cart');
        unset($data[Session::get('distributoractive')][$id]);
        Session::forget('cart');
        Session::put('cart', $data);
        return redirect('/distributor/returproduk');
    }

    //pembayaran midtrans
    public function successpayment()
    {

        $tempId = "CK";
        $temp = explode("_", DB::table('htransaksi')->where("id_htransaksi", "like", "%" . $tempId . "%")->max('id_htransaksi'));
        $countData = $temp[1] + 1;
        if ($countData < 10) {
            $tempId = $tempId . "_00" . $countData;
        } else if ($countData < 100) {
            $tempId = $tempId . "_0" . $countData;
        } else {
            $tempId = $tempId . "_" . $countData;
        }
        $grandtotal = 0;
        if (Session::has('cart')) {
            foreach (Session::get('cart') as $key => $item) {
                if ($key == Session::get('distributoractive')) {
                    foreach ($item as $datapesanan) {
                        $totalharga = 0;
                        $qtysatuan = $datapesanan['qtysatuan'];
                        $qtyperdus = $datapesanan['qtylusin'];
                        $jumlahperdus = $datapesanan['jumlahperdus'];
                        $totalqty = $qtysatuan + ($qtyperdus * $jumlahperdus);
                        $totalharga = $totalqty * $datapesanan['harga_produk'];
                        $grandtotal += $totalharga;
                        $dtransbaru = new dtransaksi;
                        $dtransbaru->id_htransaksi = $tempId;
                        $dtransbaru->id_produk = $datapesanan['id_produk'];
                        $dtransbaru->qtysatuanproduk = $qtysatuan;
                        $dtransbaru->qtyperdusproduk = $qtyperdus;
                        $dtransbaru->totalqty = $totalqty;
                        $dtransbaru->hargaperproduk = $datapesanan['harga_produk'];
                        $dtransbaru->totalharga = $totalharga;
                        $dtransbaru->save();
                        
                    }
                    $htransbaru = new htransaksi;
                    $htransbaru->id_htransaksi = $tempId;
                    $htransbaru->id_userdistributor = Session::get('distributorid');
                    $htransbaru->hargaongkir = Session::get('hargaongkir');
                    $htransbaru->grand_total = $grandtotal + Session::get('hargaongkir');
                    $htransbaru->kurang_bayar = 0;
                    $htransbaru->tgl_transaksi = Carbon::now();
                    $htransbaru->tgl_jatuh_tempo = Carbon::now();
                    $htransbaru->methodpembayaran = 1;
                    $htransbaru->status_pengiriman = 0;
                    $htransbaru->status_bayar = 1;
                    $htransbaru->save();
                }
            }
        }

        $data = Session::get('cart');
        unset($data[Session::get('distributoractive')]);
        Session::forget('cart');
        Session::remove('hargaongkir');
        Session::put('cart', $data);
        Alert::success('Berhasil', 'Pembayaran Berhasil');
        return $this->berhasillogingoogle();
    }

    //pembayaran dibelakang
    public function pembayaranupload(Request $req)
    {

        $tempId = "CK";
        $temp = explode("_", DB::table('htransaksi')->where("id_htransaksi", "like", "%" . $tempId . "%")->max('id_htransaksi'));
        $countData = $temp[1] + 1;
        if ($countData < 10) {
            $tempId = $tempId . "_00" . $countData;
        } else if ($countData < 100) {
            $tempId = $tempId . "_0" . $countData;
        } else {
            $tempId = $tempId . "_" . $countData;
        }

        $grandtotal = 0;

        if (Session::has('cart')) {
            foreach (Session::get('cart') as $key => $item) {
                if ($key == Session::get('distributoractive')) {
                    foreach ($item as $datapesanan) {
                        $totalharga = 0;
                        $qtysatuan = $datapesanan['qtysatuan'];
                        $qtyperdus = $datapesanan['qtylusin'];
                        $jumlahperdus = $datapesanan['jumlahperdus'];
                        $totalqty = $qtysatuan + ($qtyperdus * $jumlahperdus);
                        $totalharga = $totalqty * $datapesanan['harga_produk'];
                        $grandtotal += $totalharga;
                        $dtransbaru = new dtransaksi;
                        $dtransbaru->id_htransaksi = $tempId;
                        $dtransbaru->id_produk = $datapesanan['id_produk'];
                        $dtransbaru->qtysatuanproduk = $qtysatuan;
                        $dtransbaru->qtyperdusproduk = $qtyperdus;
                        $dtransbaru->totalqty = $totalqty;
                        $dtransbaru->hargaperproduk = $datapesanan['harga_produk'];
                        $dtransbaru->totalharga = $totalharga;
                        $dtransbaru->save();
                        $stokproduk = 0;
                        $kurangstok = 0;
                        $stokproduk = produk::where('id_produk', $datapesanan['id_produk'])->value('stok_produk');
                        $kurangstok = $stokproduk - $totalqty;
                        produk::where('id_produk', $datapesanan['id_produk'])->update(['stok_produk' => $kurangstok]);
                        $pencatatanstok = new pecatatanstok;
                        $pencatatanstok->id_produk = $datapesanan['id_produk'];
                        $pencatatanstok->jumlah = $totalqty;
                        $pencatatanstok->keterangan = "dibeli Customer";
                        $pencatatanstok->created_at = Carbon::now();
                        $pencatatanstok->status = 0;
                        $pencatatanstok->save();
                    }
                    $totalsemua = $grandtotal + Session::get('hargaongkir');
                    $sudahbayar = $totalsemua * 0.2;
                    $kurangbayar = $totalsemua - $sudahbayar;
                    $namafile = "buktibayar/" . $tempId . "." . $req->file("foto")->getClientOriginalExtension();
                    $req->file("foto")->move(public_path("/buktibayar"), $tempId . "." . $req->file("foto")->getClientOriginalExtension());
                    $htransbaru = new htransaksi;
                    $htransbaru->id_htransaksi = $tempId;
                    $htransbaru->id_userdistributor = Session::get('distributorid');
                    $htransbaru->hargaongkir = Session::get('hargaongkir');
                    $htransbaru->grand_total = $totalsemua;
                    $htransbaru->kurang_bayar = $kurangbayar;
                    $htransbaru->tgl_transaksi = Carbon::now();
                    $htransbaru->tgl_jatuh_tempo = Carbon::now()->addMonth(2);
                    $htransbaru->methodpembayaran = 0;
                    $htransbaru->fotopembayaran = $namafile;
                    $htransbaru->status_pengiriman = 0;
                    $htransbaru->status_bayar = 0;
                    $htransbaru->save();
                }
            }
        }

        $data = Session::get('cart');
        unset($data[Session::get('distributoractive')]);
        Session::forget('cart');
        Session::remove('hargaongkir');
        Session::put('cart', $data);
        Alert::success('Berhasil', 'Pembayaran Berhasil');
        return $this->berhasillogingoogle();
    }


    public function pembeliandistributor()
    {

        $userid = Session::get('distributorid');
        $countpembelian = htransaksi::where('id_userdistributor', $userid)->count();
        if ($countpembelian != 0) {
            $countproses = htransaksi::where('id_userdistributor', $userid)->where('status_pengiriman', 0)->count();
            $countdikirim = htransaksi::where('id_userdistributor', $userid)->where('status_pengiriman', 1)->count();
            $countselesai = htransaksi::where('id_userdistributor', $userid)->where('status_pengiriman', '>',1)->count();
        } else {
            $countpembelian = 0;
            $countproses = 0;
            $countdikirim = 0;
            $countselesai = 0;
        }
        $hprosespembelian['hproses'] = htransaksi::where('id_userdistributor', $userid)->where('status_pengiriman', 0)->get();
        $hdikirimpembelian['hdikirim'] = htransaksi::where('id_userdistributor', $userid)->where('status_pengiriman', 1)->get();
        $hselesaipembelian['hselesai'] = htransaksi::where('id_userdistributor', $userid)->where('status_pengiriman', '>', 1)->get();
        return view('distributor/pembelian', ['countproses' => $countproses, 'countdikirim' => $countdikirim, 'countselesai' => $countselesai, 'countpembelian' => $countpembelian])->with($hprosespembelian)->with($hdikirimpembelian)->with($hselesaipembelian);
    }

    public function returproduk()
    {
        if (Session::has('idnota')) {
            $user = htransaksi::where('id_htransaksi', Session::get('idnota'))->value('id_userdistributor');

            if ($user == Session::get('distributorid')) {
                $datapesanan['datapesanan'] = dtransaksi::where('id_htransaksi', Session::get('idnota'))->get();
                return view('distributor/returproduk', ['inputnota' => 1])->with($datapesanan);
            } else {
                return view('distributor/returproduk', ['inputnota' => 0]);
            }
        }
        return view('distributor/returproduk', ['inputnota' => 0]);
    }

    public function inputretur(Request $req)
    {

        $cek = htransaksi::where('id_htransaksi', $req->nota)->where('status_pengiriman', 2)->where('status_retur',0)->count();
        Session::remove('idnota');
        Session::put('idnota', $req->nota);
        if ($cek != 0) {
            $user = htransaksi::where('id_htransaksi', $req->nota)->value('id_userdistributor');
            if ($user == Session::get('distributorid')) {
                $datapesanan['datapesanan'] = dtransaksi::where('id_htransaksi', $req->nota)->get();
                return view('distributor/returproduk', ['inputnota' => 1])->with($datapesanan);
            } else {
                Alert::error('Error', 'ID Nota Salah');
                return view('distributor/returproduk', ['inputnota' => 0]);
            }
        } else {
            Alert::error('Error', 'Pesanan Belom Selesai atau Produk Telah diretur');
            return view('distributor/returproduk', ['inputnota' => 0]);
        }
    }

    public function addreturproduk(Request $req)
    {

        $id = $req->idprodukretur;
        $jumlah_retur = $req->jumlah_retur;
        $qtyproduk = dtransaksi::where('id_htransaksi', Session::get('idnota'))->where('id_produk', $id)->value('totalqty');
        if ($qtyproduk >= $jumlah_retur) {
            $cart = Session::get('returbarang');
            if (isset($cart[Session::get('distributoractive')])) {
                if (isset($cart[Session::get('distributoractive')][$id])) {
                    $tempjumlah = $cart[Session::get('distributoractive')][$id]['qtyretur'] + $jumlah_retur;
                    if($tempjumlah<=$qtyproduk){
                        $cart[Session::get('distributoractive')][$id]['qtyretur'] += $jumlah_retur;
                    }
                    else{
                        Alert::warning('warning', 'Jumlah Retur Lebih banyak Dari Pembelian');
                        return redirect('/distributor/returproduk');
                    }
                    
                } else {
                    $cart[Session::get('distributoractive')][$id] = array('id_produk' => $id, 'qtyretur' => $jumlah_retur);
                }
            } else {
                $cart[Session::get('distributoractive')][$id] = array('id_produk' => $id, 'qtyretur' => $jumlah_retur);
            }

            Session::put('returbarang', $cart);
            Alert::success('Berhasil', 'Berhasil add Retur');
            return redirect('/distributor/returproduk');
        } else {
            Alert::warning('warning', 'Jumlah Retur Lebih banyak Dari Pembelian');
            return redirect('/distributor/returproduk');
        }
        // 
    }

    public function hapusaddproduk($id)
    {
        $data = Session::get('returbarang');
        unset($data[Session::get('distributoractive')][$id]);
        Session::forget('returbarang');
        Session::put('returbarang', $data);
        return redirect('/distributor/returproduk');
    }

    public function submitretur(Request $req)
    {
        $tempId = "RB";
        $temp = explode("_", DB::table('hretur')->where("id_hretur", "like", "%" . $tempId . "%")->max('id_hretur'));
        $countData = $temp[1] + 1;
        if ($countData < 10) {
            $tempId = $tempId . "_00" . $countData;
        } else if ($countData < 100) {
            $tempId = $tempId . "_0" . $countData;
        } else {
            $tempId = $tempId . "_" . $countData;
        }
        if (Session::has('returbarang')) {
            foreach (Session::get('returbarang') as $key => $item) {
                if ($key == Session::get('distributoractive')) {
                    foreach ($item as $dataretur) {
                        $dreturabaru = new dretur;
                        $dreturabaru->id_hretur = $tempId;
                        $dreturabaru->id_produk = $dataretur['id_produk'];
                        $dreturabaru->qtyretur = $dataretur['qtyretur'];
                        $dreturabaru->save();
                    }
                    $namafile = "buktiretur/" . $tempId . "." . $req->file("imageretur")->getClientOriginalExtension();
                    $req->file("imageretur")->move(public_path("/buktiretur"), $tempId . "." . $req->file("imageretur")->getClientOriginalExtension());
                    $hreturbaru = new hretur;
                    $hreturbaru->id_hretur = $tempId;
                    $hreturbaru->id_userdistributor = Session::get('distributorid');
                    $hreturbaru->id_htransaksi = Session::get('idnota');
                    $hreturbaru->tgl_retur = Carbon::now();
                    $hreturbaru->fotobuktiretur = $namafile;
                    $hreturbaru->status_pengiriman = 0;
                    $hreturbaru->save();
                }
            }
        }

        $data = Session::get('returbarang');
        unset($data[Session::get('distributoractive')]);
        Session::forget('returbarang');
        Session::remove('idnota');
        Session::put('returbarang', $data);
        Alert::success('Berhasil', 'Berhasil Mengajukan Retur');
        return redirect('/distributor/returproduk');
    }

    public function cetaknotatransaksi($id)
    {

        $datadtrans['datadtrans'] = dtransaksi::where('id_htransaksi', $id)->get();
        $status = htransaksi::where('id_htransaksi', $id)->value('status_bayar');
        $subtotal = htransaksi::where('id_htransaksi', $id)->value('grand_total');
        return view('/distributor/cetaknotatransaksi', ['id' => $id, 'status' => $status, 'subtotal' => $subtotal])->with($datadtrans);
    }

    public function searchproduk(Request $req)
    {
        $datakategori['kategori'] = kategori::get();

        if (isset($req->searchproduk)) {
            $text = $req->searchproduk;
            $ctrproduk = produk::where('status_produk', 1)->where('nama_produk', 'like', '%' . $text . '%')->count();
            if($ctrproduk!=0){
                $dataproduk['dataproduk'] = produk::where('status_produk', 1)->where('nama_produk', 'like', '%' . $text . '%')->get();
                return view("distributor/indexdistributor", ['foto' => Session::get('distributorfoto'), 'isi' => 1])->with($dataproduk)->with($datakategori);
            }
            else{
                $dataproduk['dataproduk'] = produk::get()->where('status_produk', 1);
                Alert::Error('Error','produk yang dicari tidak ada');
                return view("distributor/indexdistributor", ['foto' => Session::get('distributorfoto'), 'isi' => 1])->with($dataproduk)->with($datakategori);
            }
           
        } else {
            $dataproduk['dataproduk'] = produk::get()->where('status_produk', 1);
            return view("distributor/indexdistributor", ['foto' => Session::get('distributorfoto'), 'isi' => 1])->with($dataproduk)->with($datakategori);
        }
    }

    public function sortkategori(Request $req)
    {
        $datakategori['kategori'] = kategori::get();

        // dd($req->kategori);
        if ($req->kategori == "reset") {
            $dataproduk['dataproduk'] = produk::get()->where('status_produk', 1);
            // dd($req->kategori);
            return view("distributor/indexdistributor", ['foto' => Session::get('distributorfoto'), 'isi' => 1])->with($dataproduk)->with($datakategori);
        } else {
            $dataproduk['dataproduk'] = produk::get()->where('status_produk', 1)->where('kategori_produk', $req->kategori);
            // dd($req->kategori);
            return view("distributor/indexdistributor", ['foto' => Session::get('distributorfoto'), 'isi' => 1])->with($dataproduk)->with($datakategori);
        }
    }

    public function listretur(){


        $userid = Session::get('distributorid');
        $countpembelian = hretur::where('id_userdistributor', $userid)->count();
        if ($countpembelian != 0) {
            $countproses = hretur::where('id_userdistributor', $userid)->where('status_pengiriman', 0)->count();
            $countdikirim = hretur::where('id_userdistributor', $userid)->where('status_pengiriman', 1)->count();
            $countselesai = hretur::where('id_userdistributor', $userid)->where('status_pengiriman', [2,3])->count();
        } else {
            $countpembelian = 0;
            $countproses = 0;
            $countdikirim = 0;
            $countselesai = 0;
        }
        $hprosespembelian['hproses'] = hretur::where('status_pengiriman', 0)->where('id_userdistributor',Session::get('distributorid'))->get();
        $hdikirimpembelian['hdikirim'] = hretur::where('status_pengiriman', 1)->where('id_userdistributor',Session::get('distributorid'))->get();
        $hselesaipembelian['hselesai'] = hretur::where('status_pengiriman', [2,3])->where('id_userdistributor',Session::get('distributorid'))->get();
        return view('distributor/listretur', ['countproses' => $countproses, 'countdikirim' => $countdikirim, 'countselesai' => $countselesai, 'countpembelian' => $countpembelian])->with($hprosespembelian)->with($hdikirimpembelian)->with($hselesaipembelian);
    }

    public function filtertglretur(Request $req){
        $tgldari = $req->tgldari;
        $tglsampai = $req->tglsampai;
        if($tglsampai>$tgldari){
            $userid = Session::get('distributorid');
            $countpembelian = hretur::where('id_userdistributor', $userid)->count();
            if ($countpembelian != 0) {
                $countproses = hretur::where('id_userdistributor', $userid)->where('status_pengiriman', 0)->count();
                $countdikirim = hretur::where('id_userdistributor', $userid)->where('status_pengiriman', 1)->count();
                $countselesai = hretur::where('id_userdistributor', $userid)->where('status_pengiriman', 2)->count();
            } else {
                $countpembelian = 0;
                $countproses = 0;
                $countdikirim = 0;
                $countselesai = 0;
            }
            $hprosespembelian['hproses'] = hretur::where('status_pengiriman', 0)->where('id_userdistributor',Session::get('distributorid'))->get();
            $hdikirimpembelian['hdikirim'] = hretur::where('status_pengiriman', 1)->where('id_userdistributor',Session::get('distributorid'))->get();
            $hselesaipembelian['hselesai'] = hretur::whereBetween('tgl_retur',[$tgldari,$tglsampai])->where('status_pengiriman', 2)->where('id_userdistributor',Session::get('distributorid'))->get();
            return view('distributor/listretur', ['countproses' => $countproses, 'countdikirim' => $countdikirim, 'countselesai' => $countselesai, 'countpembelian' => $countpembelian])->with($hprosespembelian)->with($hdikirimpembelian)->with($hselesaipembelian);
        }
        else{
            Alert::Error('Error','Input Tanggal Salah');
            return redirect('/distributor/listretur');
        }
    }

    public function filtertgltransaksi(Request $req){
        $tgldari = $req->tgldari;
        $tglsampai = $req->tglsampai;
        if($tglsampai>$tgldari){
            $userid = Session::get('distributorid');
            $countpembelian = htransaksi::where('id_userdistributor', $userid)->count();
            if ($countpembelian != 0) {
                $countproses = htransaksi::where('id_userdistributor', $userid)->where('status_pengiriman', 0)->count();
                $countdikirim = htransaksi::where('id_userdistributor', $userid)->where('status_pengiriman', 1)->count();
                $countselesai = htransaksi::where('id_userdistributor', $userid)->where('status_pengiriman', 2)->count();
            } else {
                $countpembelian = 0;
                $countproses = 0;
                $countdikirim = 0;
                $countselesai = 0;
            }
            $hprosespembelian['hproses'] = htransaksi::where('id_userdistributor', $userid)->where('status_pengiriman', 0)->get();
            $hdikirimpembelian['hdikirim'] = htransaksi::where('id_userdistributor', $userid)->where('status_pengiriman', 1)->get();
            $hselesaipembelian['hselesai'] = htransaksi::whereBetween('tgl_transaksi',[$tgldari,$tglsampai])->where('id_userdistributor', $userid)->where('status_pengiriman', 2)->get();
            return view('distributor/pembelian', ['countproses' => $countproses, 'countdikirim' => $countdikirim, 'countselesai' => $countselesai, 'countpembelian' => $countpembelian])->with($hprosespembelian)->with($hdikirimpembelian)->with($hselesaipembelian);
        }
        else{
            Alert::Error('Error','Input Tanggal Salah');
            return redirect('/distributor/pembelian');
        }
    }

    public function cetaknotaretur($id){
        $datadretur['datadretur'] = dretur::where('id_hretur',$id)->get();
        return view('/distributor/cetaknotaretur',['id'=>$id])->with($datadretur); 
    }
}
