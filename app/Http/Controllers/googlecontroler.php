<?php

namespace App\Http\Controllers;

use App\Models\distributor;
use App\Models\pegawai;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Laravel\Socialite\Facades\Socialite;

class googlecontroler extends Controller
{
    //
    public function redirectToGoogle(){
        return Socialite::driver('google')->redirect();
    }

    // public function berhasillogin($idgoogleuser,$nama){
    //     $findfoto = distributor::Select('foto_user')->Where('id_google',$idgoogleuser)->first();
        
    //     // dd($findfoto['foto_user']);
    //     Session::put('distributoractive',$nama);
    //     Session::put('photodistributor',$findfoto['foto_user']);
        
    //     Session::put('logindistributor','masuk');
    //     return view("indexdistributor");
    // }

    public function handleGoogleCallback(){
       
      
        try {
            $user = Socialite::driver('google')->stateless()->user();
            $finduser = distributor::where('id_google',$user->getId())->first();
            $idgoogle = $user->getId();
                $email = $user->getEmail();
                $nama = $user->getName();
                $foto = $user->user['picture'];
            if(isset($finduser)){
                Session::put('googleid',$idgoogle);
                return redirect("distributor/indexdistributor");               
            }
            else{
                $userbaru = new distributor();
                $userbaru->id_google = $user->getId();
                $userbaru->email_user = $user->getEmail();
                $userbaru->nama_user = $user->getName();
                $userbaru->password_user = "";
                $userbaru->nama_toko = "";
                $userbaru->no_tlp_distributor = "";
                $userbaru->alamat_toko = "";
                $userbaru->foto_user = $user->getAvatar();
                $userbaru->status_user = 1;
                $userbaru->save();
                Session::put('googleid',$idgoogle);
                return redirect("distributor/indexdistributor");
            }
        } catch (Exception $e) {
            dd($e->getMessage());
        }

    }

   

} 
