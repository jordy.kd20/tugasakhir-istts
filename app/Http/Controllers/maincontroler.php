<?php

namespace App\Http\Controllers;

use App\Models\pegawai;
use Illuminate\Hashing\BcryptHasher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Alert;
use App\Models\distributor;
use Illuminate\Routing\Route;

class maincontroler extends Controller
{

    public function logout(){
        Session::remove('active');
        Session::remove('loginadmin');
        Session::remove('loginkaryawan');
        Session::remove('loginowner');
        Session::remove('idpegawai');
        return \redirect("/dashboard");
    }
    
    public function loginpegawai(Request $req){
        $validateData = $req->validate(
            [
                "username"=>"required",
                "password"=>"required"
            ],
            [
                "required"=>"Attribut tidak boleh kosong",
                "password"=>"salahh"
            ]
        );

        if(Session::has('loginowner')){
            return \redirect("owner/");
        }
        else if(Session::has('loginadmin')){
            return \redirect("admin/");
        }
        else{
            $username = $req->username;
            $password = $req->password;
            $passworduser = pegawai::select('password_pegawai')->where('username_pegawai',$username)->first();
            $cekuser = pegawai::where('username_pegawai',$username)->count();
            $statususer = pegawai::where('username_pegawai',$username)->value('status');           
            if($cekuser!=0){
                if(Hash::check($password,$passworduser->password_pegawai)){
                    $cekjabatan = pegawai::select('jabatan')->where('username_pegawai',$username)->first();
                    $idpegawai = pegawai::where('username_pegawai',$username)->value('id_pegawai');
                    if($cekjabatan->jabatan == "owner"){
                        $namapegawai = pegawai::select('nama_pegawai')->where('username_pegawai',$username)->first();
                        Session::put('active',$namapegawai->nama_pegawai);
                        Session::put('loginowner','masuk'); 
                        Session::put('idpegawai',$idpegawai);                    
                        return \redirect("owner/");
                    }
                    if($cekjabatan->jabatan == "admin"){
                        if($statususer==1){
                            $namapegawai = pegawai::select('nama_pegawai')->where('username_pegawai',$username)->first();
                            Session::put('active',$namapegawai->nama_pegawai);
                            Session::put('loginadmin','masuk');
                            Session::put('idpegawai',$idpegawai);     
                            return \redirect ("admin/");
                        }
                        else{
                            return redirect("/dashboard");
                        } 
                    }
                    
                }
                else{
                    return redirect("/dashboard");
                }
            }
            else{
                return redirect("/dashboard");
            }
        }
    }

    public function loginmasuk(){
        if(Session::has('loginowner')){
            return \redirect("owner/");
        }
        else if(Session::has('loginadmin')){
            return \redirect("admin/");
        }
        else{
            return view('loginadmin');
        }
        
    }


    public function cekpass(){
       
        

    }

    public function showprofile(){

        $datapegawai['datapegawai'] =pegawai::where('id_pegawai',Session::get('idpegawai'))->get();
        return view('updateprofilepegawai')->with($datapegawai);


    }

    public function updateprofile(Request $req){

        if(isset($req->password_baru)){
            if($req->password_baru == $req->confirm_password){
                $newpass = hash::make($req->password_baru); 
                pegawai::where('id_pegawai',Session::get('idpegawai'))->update(['email_pegawai'=>$req->email_pegawai,'alamat_pegawai'=>$req->alamat_pegawai,'telephone_pegawai'=>$req->nomor_pegawai,'password_pegawai'=>$newpass]);
            }
        }
        else{
            pegawai::where('id_pegawai',Session::get('idpegawai'))->update(['email_pegawai'=>$req->email_pegawai,'alamat_pegawai'=>$req->alamat_pegawai,'telephone_pegawai'=>$req->nomor_pegawai]);
        }
        if(Session::has('loginadmin')){
            Alert::success('Berhasil', 'Berhasil Update Profile' );
            return redirect('/admin');
        }
        elseif(Session::has('loginowner')){
            Alert::success('Berhasil', 'Berhasil Update Profile' );
            return redirect('/owner');
        }

    }


    public function lupapasword($id, $pass){
        $passbaru = hash::make($pass);
        distributor::where('id_user',$id)->update(['password_user'=>$passbaru]);
        return redirect('/');
    }

}
