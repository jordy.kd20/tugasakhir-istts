<?php

namespace App\Http\Controllers;

use App\Models\asset;
use App\Models\distributor;
use App\Models\hpembelian;
use App\Models\htransaksi;
use App\Models\kategori;
use App\Models\pecatatanstok;
use App\Models\pegawai;
use App\Models\produk;
use Dompdf\Adapter\PDFLib;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use PDF;
use RealRashid\SweetAlert\Facades\Alert;

class ownercontroler extends Controller
{
    public function indexowner(){
        $datanama = Session::get('active');
        return view("owwner/indexowner");
    }

    public function transaksidistributor(){
        $counthdistributor = htransaksi::count();
        if($counthdistributor!=0){
            $htrans['hproses'] = htransaksi::whereIn('status_pengiriman', [1,2])->orderBy('tgl_transaksi','desc')->get();
            return view('owner/indexowner',['counthdistributor'=>$counthdistributor])->with($htrans);
            // dd($htrans);
        }
        else{
            return view('owner/indexowner',['counthdistributor'=>$counthdistributor]);
        }
    }


    public function listkaryawan(){

        $countkaryawan = pegawai::get()->whereNotIn("jabatan","owner")->count();
        if($countkaryawan>0){
            $datakaryawan['datakaryawan'] = pegawai::get()->whereNotIn("jabatan","owner"); 
            return view("owner/listpegawai",["isi"=>1])->with($datakaryawan);
            
        }
        else{
            return view("owner/listpegawai",["isi"=>0]);
            
        }
        return view('owner/listpegawai');
    }


    public function listbarang(){
        $countproduk = produk::count();
        if($countproduk!=0){
            $dataproduk['dataproduk'] = produk::get();
           
            return view('owner/listbarangowner',['isi'=> 1])->with($dataproduk);
        }
        else{
           
            return view('owner/listbarangowner',['isi'=> 0]);
            
        }
    }

    public function listdistributor(){
        $datadistributor['datadistributor'] = distributor::get();
        return view('owner/listdistributor',['isi'=> 1])->with($datadistributor);
    }

    public function disabledpegawai($id){
        pegawai::where('id_pegawai',$id)->update(['status' => 0]);
        return $this->listkaryawan();
    }

    public function aktifpegawai($id){
        pegawai::where('id_pegawai',$id)->update(['status' => 1]);
        return $this->listkaryawan();
    }

    public function disableddistributor($id){
        distributor::where('id_user',$id)->update(['status_user'=>0]);
        return redirect('/owner/listdistributor');
    }

    public function aktifdistributor($id){
        distributor::where('id_user',$id)->update(['status_user'=>1]);
        return redirect('/owner/listdistributor');
    }

    public function updateproduk($id){
        $dataproduk['dataproduk'] =  produk::get()->where('id_produk',$id);
        $namakategori['namakategori'] = kategori::get();
        $fotoproduk = produk::where('id_produk',$id)->value('foto_produk');
        return view("owner/updateproduk",["foto"=>$fotoproduk])->with($dataproduk)->with($namakategori);
     }
     
     public function submitupdateproduk(Request $req){
         $idproduk = $req->id_produk;
         $warnalampu = $req->warna_lampu;
         $flux = $req->flux_produk;
         $stok = $req->stok_produk;
         $jumlahperdus = $req->jumlahperdus;
         $harga = $req->harga_produk;
         $beratproduk =$req->berat_produk;
 
         produk::where('id_produk',$idproduk)->update([
             'warna_lampu' => $warnalampu,
             'flux' => $flux,
             'stok_produk' => $stok,
             'jumlahperdus' => $jumlahperdus,
             'harga_produk' => $harga,
             'berat_produk' => $beratproduk
         ]);
         return $this->listbarang();
     }
 
     public function disabledproduk($id){
         produk::where('id_produk',$id)->update(['status_produk'=>1]);
         return $this->listbarang();
     }
 
     public function aktifproduk($id){
         produk::where('id_produk',$id)->update(['status_produk'=>0]);
         return $this->listbarang();
     }

    public function registerpegawai(Request $req){
        $validasi = $req->validate(
            [
                'password_repeat'=>'same:password'
            ],
            [
                'password_repeat'=>'harus sama dengan password'
            ]);
            
            $pass = $req->password;
            $cpass = $req->password_repeat;
    
            if($pass == $cpass){
                $nama = $req->nama;
                $email = $req->email;
                $alamat = $req->alamat;
                $telephone = $req->nomor_hp;
                $jabatan = $req->jabatan;
                $username = $req->username;
                $data = pegawai::where('username_pegawai',$username)->count();
                if($data==0){
                    $userbaru = new pegawai;
                    $userbaru->email_pegawai = $email;
                    $userbaru->username_pegawai = $username;
                    $userbaru->nama_pegawai = $nama;
                    $userbaru->alamat_pegawai = $alamat;
                    $passbaru = Hash::make($pass);
                    $userbaru->password_pegawai = $passbaru;
                    $userbaru->telephone_pegawai = $telephone;
                    $userbaru->jabatan = $jabatan;
                    $userbaru->status = 1;
                    $userbaru->save();
                    return redirect("/owner/listkaryawan");
                }
                else{
                    Alert::Error('Error','username sudah ada');
                    return redirect("/owner/listkaryawan");
                }
    
            }
    }

    public function stokreport(){
        $datastok['pencatatanstok'] = pecatatanstok::get();
        return view('/owner/stokreport',['isi'=>1])->with($datastok);
    }

    public function listhutang(){

        $counthutang = htransaksi::where('status_bayar',0)->count();

        if($counthutang!=0){
            $listhutang["listhutang"] = htransaksi::where('status_bayar',0)->where('status_pengiriman','>',0 )->get();
            return view('owner/listhutangdistributor',['count'=>$counthutang])->with($listhutang);
        }
        else{
            return view('owner/listhutangdistributor',['count'=>$counthutang]);
        }
    }

    public function filtertgltstok(Request $req){
        $tgldari = $req->tgldari;
        $tglsampai = $req->tglsampai;

        if($tglsampai>=$tgldari){
            $datastok['pencatatanstok'] = pecatatanstok::whereBetween('created_at',[$tgldari,$tglsampai])->get();
            return view('/owner/stokreport',['isi'=>1,'tgldari'=>$tgldari, 'tglsampai'=>$tglsampai])->with($datastok);
        }
        else{
            $datastok['pencatatanstok'] = pecatatanstok::get();
            Alert::Error('Error','Input Tanggal Salah');
            return view('/owner/stokreport',['isi'=>1])->with($datastok);
        }
    }

    public function filtertglttransaksi(Request $req){
        $tgldari = $req->tgldari;
        $tglsampai = $req->tglsampai;
        $counthdistributor = htransaksi::count();


        if($tglsampai>=$tgldari){
            if($counthdistributor!=0){
                $htrans['hproses'] = htransaksi::WhereBetween('tgl_transaksi',[$tgldari,$tglsampai])->whereIn('status_pengiriman', [1,2])->orderBy('tgl_transaksi','desc')->get();
                return view('owner/indexowner',['counthdistributor'=>$counthdistributor])->with($htrans);
                // dd($htrans);
            }
            else{
                return view('owner/indexowner',['counthdistributor'=>$counthdistributor]);
            }
        }
        else{
            $htrans['hproses'] = htransaksi::whereIn('status_pengiriman', [1,2])->orderBy('tgl_transaksi','desc')->get();
            Alert::Error('Error', 'Input Tanggal Salah');
            return view('owner/indexowner',['counthdistributor'=>$counthdistributor])->with($htrans);
        }

    }

    public function listpembelian(){
        $hpembelian['hpembelian'] = hpembelian::get(); 
        return view('owner/listpembelianproduk',["isi"=>1])->with($hpembelian);
    }

    public function filtertglpembelian(Request $req){
        $tgldari  = $req->tgldari;
        $tglsampai = $req->tglsampai;

        if($tglsampai>=$tgldari){
            $hpembelian['hpembelian'] = hpembelian::WhereBetween('tgl_pembelian',[$tgldari,$tglsampai])->get(); 
            return view('owner/listpembelianproduk',["isi"=>1])->with($hpembelian);
        }
        else{

        }

    }

    public function pdftransaksidistributor(Request $req){

        if(isset($req->tgldari) && isset($req->tglsampai)){
            if($req->tglsampai>= $req->tgldari){
                $countdata = htransaksi::whereBetween('tgl_transaksi',[$req->tgldari, $req->tglsampai])->count();
                if($countdata!=0){
                    $datahtrans = htransaksi::whereBetween('tgl_transaksi',[$req->tgldari, $req->tglsampai])->get();
                    Session::remove('tgldari');
                    Session::remove('tglsampai');
                    Session::put('tgldari',$req->tgldari);
                    Session::put('tglsampai',$req->tglsampai);
                    view()->share('datahtrans',$datahtrans);                   
                    $pdf = PDF::loadview('owner/Laporantransaksidistributor')->setPaper('A4','Portrait');
                    return $pdf->download('LaporanTransaksi.pdf');
                    return redirect('/owner');   
                }
                else{
                    Alert::Error('Error','Data yang diinput kosong');
                    return redirect('/owner');   
                }
               
            }
            else{
                Alert::Error('Error','Input Tanggal Salah');
                return redirect('/owner');
            }
        }   
    }

    public function listasset(){
        $countassset = asset::get()->count();
        if($countassset!=0){
            $dataasset['dataasset'] = asset::get();
            return view('owner/listasset',["isi"=>1])->with($dataasset);
        }
        else{
            return view('owner/listasset',["isi"=>0]);
        }
    }

    
}
