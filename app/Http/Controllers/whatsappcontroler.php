<?php

namespace App\Http\Controllers;

use App\Models\htransaksi;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;


class whatsappcontroler extends Controller
{
    public function send_request($data, $url){

        $id = "3765";
        $key = "2a81512bb263277eb7ceaba17c86d8389a6b4909";

        $url = $url.'/'.$id.'/'.$key;
        $ch = curl_init( $url );
        curl_setopt( $ch, CURLOPT_POST, 1);
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt( $ch, CURLOPT_HEADER, 0);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec( $ch );

    }

    public function sendText($number, $message){
        $data['number'] = $number;
        $data['message'] = $message;
        $url = "https://onyxberry.com/services/wapi/api2/sendText";
        return $this->send_request($data, $url);
    }

    public function sentwa(){

        $number = "+628123518888";
        $message = "Hallo paaa ini pakai Laravel";

        $this->sendText($number,$message);

    }


    public function sentreminder(){
        $counhutang = htransaksi::where('status_bayar',0)->whereDate('tgl_jatuh_tempo',Carbon::now())->count();
        if($counhutang!=0){
            $htranshutang = htransaksi::where('status_bayar',0)->whereDate('tgl_jatuh_tempo',Carbon::now())->get();
            $tglsekarang = Date('d F Y',strtotime(Carbon::now()));
            $count = 0;
            foreach ($htranshutang as $item){
                $tgljatuhtempo = Date('d F Y',strtotime($item->tgl_jatuh_tempo));
                if($tglsekarang == $tgljatuhtempo){
                    $number = DB::table('user_distributor')->where('id_user',$item->id_userdistributor)->value('no_tlp_distributor');
                    $message = "Halo Kami Dari CV. Optimus Cahaya Abadi Ingin Mengingatkan untuk pembayaran dengan nota ".$item->id_htransaksi.
                    " dengan total yang harus dibayarkan sejumlah Rp. ".number_format($item->kurang_bayar) ." Agar segera dibayarkan. tanggal jatuh tempo ". $tgljatuhtempo. " pembayaran selambat - lambatnya 1 minggu dari pesan ini." ;
                    $this->sendText($number, $message);
                }
            }
            Alert::success('Berhasil', 'Berhasil Melakukan Pembayaran' );
            return redirect('/admin/listhutang');
        }
        else{
            return redirect('/admin/listhutang');
        }
    }



}
