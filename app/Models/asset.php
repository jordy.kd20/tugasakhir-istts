<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class asset extends Model
{
    protected $table = 'asset';

    protected $primaryKey = 'id_asset';

    protected $keyType = 'integer';
    public $timestamps = false;
}
