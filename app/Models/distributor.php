<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class distributor extends Model
{
    protected $table = 'user_distributor';

    protected $primaryKey = 'id_user';

    protected $keyType = 'integer';
    public $timestamps = false;
}
