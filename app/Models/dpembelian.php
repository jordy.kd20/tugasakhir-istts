<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class dpembelian extends Model
{
    protected $table = 'dpembelian';

    protected $primaryKey = 'id_dpembelian';

    protected $keyType = 'integer';
    public $timestamps = false;
}
