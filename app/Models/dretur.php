<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class dretur extends Model
{
    protected $table = 'dretur';

    protected $primaryKey = 'id_dretur';

    protected $keyType = 'integer';
    public $timestamps = false;
}
