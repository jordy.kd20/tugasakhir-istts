<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class dtransaksi extends Model
{
    protected $table = 'dtransaksi';

    protected $primaryKey = 'id_dtransaksi';

    protected $keyType = 'integer';
    public $timestamps = false;
}
