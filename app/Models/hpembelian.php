<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class hpembelian extends Model
{
    protected $table = 'hpembelian';

    protected $primaryKey = 'id_hpembelian';

    protected $keyType = 'string';
    public $timestamps = false;

    public function hpembelian(){
        return $this->hasMany(dpembelian::class);
    }
}
