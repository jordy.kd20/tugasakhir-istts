<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class hretur extends Model
{
    protected $table = 'hretur';

    protected $primaryKey = 'id_hretur';

    protected $keyType = 'string';
    public $timestamps = false;

    
}
