<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class htransaksi extends Model
{
    protected $table = 'htransaksi';

    protected $primaryKey = 'id_htransaksi';

    protected $keyType = 'string';
    public $timestamps = false;

   
}
