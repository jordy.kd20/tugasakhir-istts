<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class pecatatanstok extends Model
{
    protected $table = 'pencatatanstok';

    protected $primaryKey = 'id_pencatatanstok';

    protected $keyType = 'integer';
    public $timestamps = false;
}
