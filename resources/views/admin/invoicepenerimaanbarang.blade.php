@extends('template')
<head>
    <title>Invoice Penerimaan Barang</title>
</head>
@section('Content')
<body onload="window.print()">
    <div style="height: 29.7cm;">
        <div>
            <div class="row" style="padding-bottom: 10px;">
                <div style="text-align: center"><img src="{{url('asset/img/kopsurat.jpg')}}" alt="" srcset=""></div>
                
                {{-- <div class="col-2 d center align-items-center">
                    <div class="center"><img class="d-xl-flex" style="width: 100px;" src="{{url('asset/img/Logo.png')}}"></div>
                </div>
                <div class="col d right">
                    <div>
                        <div class="row">
                            <div class="col">
                                <h3 class="d right">CV. Optimus Cahaya Abadi</h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <p class="d right">Jl. Komp Pergudangan Margomulyo I - 12</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <p class="d right">Margomulyo - Surabaya</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <p class="d right">Telp : 03199025157</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <p class="d right">Tanggal Pembelian : {{$tglpembelian}}</p>
                            </div>
                        </div>
                    </div>
                </div> --}}
            </div>
        </div>
        <hr>
        <div>
            <h1 class="text-center" style="padding-bottom: 20px;">Invoice Penerimaan Barang</h1>
        </div>
        <div style="padding-top: 20px;">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>no</th>
                            <th>Nama Barang</th>
                            <th>Jumlah satuan</th>
                            <th>Jumlah per Dus</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $ctr = 1;
                        @endphp
                        @foreach ($datapembelian as $item)
                            <tr>
                                <td>{{$ctr}}</td>
                                <td>{{DB::table('produk')->where('id_produk',$item->id_produk)->value('nama_produk')}}</td>
                                <td>{{$item->qtytotal}}</td>
                                <td>
                                    @php
                                        $dus = DB::table('produk')->where('id_produk',$item->id_produk)->value('jumlahperdus');
                                        $totaldus = $item->qtytotal / $dus;
                                    @endphp
                                    {{$totaldus}}
                                </td>
                            </tr>
                            @php
                                $ctr +=1;
                            @endphp
                        @endforeach 
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="{{url('asset/js/bs-init.js')}}"></script>
    <script src="{{url('asset/js/theme.js')}}"></script>
    <script src="{{url('assets/bootstrap/js/bootstrap.min.css')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
</body>
@endsection