@extends('template')

<head>
    <title>
        List Pembelian Distributor
    </title>
</head>
@section('Content')

    <body id="page-top">
        <div id="wrapper">
            <nav class="navbar navbar-dark align-items-start sidebar sidebar-dark accordion bg-gradient-primary p-0"
                style="background: #171717;">
                <div class="container-fluid d-flex flex-column p-0"><a
                        class="navbar-brand d-flex justify-content-center align-items-center sidebar-brand m-0"
                        href="#">
                        <div class="sidebar-brand-icon rotate-n-15"><img src="{{ url('asset/img/Logo.png') }}"
                                style="width: 40px;height: 40px;transform: rotate(15deg);"></div>
                        <div class="sidebar-brand-text mx-3"><span style="font-size: 9px;">CV. Optimus Cahata ABADI</span>
                        </div>
                    </a>
                    <hr class="sidebar-divider my-0">
                    <ul class="navbar-nav text-light" id="accordionSidebar">
                        <li class="nav-item"><a class="nav-link" href="/admin"><i class="fa fa-list-ul"></i><span>List
                                    Barang</span></a></li>
                        <li class="nav-item"><a class="nav-link" href="/admin/listasset"><i
                                    class="fa fa-building"></i><span>List Asset</span></a>
                            <a class="nav-link" href="/admin/pembelianproduk"><i class="fa fa-cart-plus"></i><span>Pembelian
                                    produk</span></a>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="/admin/penguranganstok"><i
                                    class="fa fa-exchange"></i><span>Update Stok</span></a></li>
                                    <li class="nav-item"><a class="nav-link " href="/admin/stokreport"><i class="fa fa-exchange"></i><span>Laporan Stok</span></a></li>
                        <li class="nav-item"><a class="nav-link" href="/admin/listpembelian"><i
                                    class="fa fa-list-ul"></i><span>List Pembelian Produk</span></a></li>
                        <li class="nav-item"><a class="nav-link active" href="/admin/listpembeliandistributor"><i
                                    class="fa fa-dollar"></i><span>List Pembelian Distributor</span></a></li>
                        <li class="nav-item"><a class="nav-link" href="/admin/listretur"><i
                                    class="fa fa-info"></i><span>List Retur Distributor</span></a></li>
                        <li class="nav-item"><a class="nav-link" href="/admin/listhutang"><i
                                    class="fa fa-money"></i><span>Hutang Distributor</span></a></li>
                    </ul>
                    <div class="text-center d-none d-md-inline"><button class="btn rounded-circle border-0"
                            id="sidebarToggle" type="button"></button></div>
                </div>
            </nav>
            <div class="d-flex flex-column" id="content-wrapper">
                <div id="content">
                    <nav class="navbar navbar-light navbar-expand bg-white shadow mb-4 topbar static-top">
                        <div class="container-fluid"><button class="btn btn-link d-md-none rounded-circle me-3"
                                id="sidebarToggleTop" type="button"><i class="fas fa-bars"></i></button>
                            <ul class="navbar-nav flex-nowrap ms-auto">
                                <li class="nav-item dropdown d-sm-none no-arrow"><a class="dropdown-toggle nav-link"
                                        aria-expanded="false" data-bs-toggle="dropdown" href="#"><i
                                            class="fas fa-search"></i></a>
                                    <div class="dropdown-menu dropdown-menu-end p-3 animated--grow-in"
                                        aria-labelledby="searchDropdown">
                                        <form class="me-auto navbar-search w-100">
                                            <div class="input-group"><input class="bg-light form-control border-0 small"
                                                    type="text" placeholder="Search for ...">
                                                <div class="input-group-append"><button class="btn btn-primary py-0"
                                                        type="button"><i class="fas fa-search"></i></button></div>
                                            </div>
                                        </form>
                                    </div>
                                </li>
                                <li class="nav-item dropdown no-arrow">
                                    <div class="nav-item dropdown no-arrow"><a class="dropdown-toggle nav-link"
                                            aria-expanded="false" data-bs-toggle="dropdown" href="#"><span
                                                class="d-none d-lg-inline me-2 text-gray-600 small">{{ Session::get('active') }}</span><img
                                                class="border rounded-circle img-profile"
                                                src='{{ url('/asset/img/avatars/avatar1.jpeg') }}'></a>
                                        <div class="dropdown-menu shadow dropdown-menu-end animated--grow-in"><a
                                                class="dropdown-item" href="/showprofilepegawai"><i
                                                    class="fas fa-user fa-sm fa-fw me-2 text-gray-400"></i>&nbsp;Edit
                                                Profile</a><a class="dropdown-item" href="/logout"><i
                                                    class="fas fa-sign-out-alt fa-sm fa-fw me-2 text-gray-400"></i>&nbsp;Logout</a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </nav>
                    <div>
                        <ul class="nav nav-tabs" role="tablist"
                            style="border-color: rgb(255,255,255);border-top-color: rgb(133,;border-right-color: 135,;border-bottom-color: 150);border-left-color: 135,;">
                            <li class="nav-item" role="presentation"><a class="nav-link " role="tab"
                                    data-bs-toggle="tab" href="#tab-1" style="color: rgb(0,0,0);">Proses Pesanan</a>
                            </li>
                            <li class="nav-item" role="presentation"><a class="nav-link" role="tab"
                                    data-bs-toggle="tab" href="#tab-2" style="color: rgb(0,0,0);">Pesanan Dikrim</a>
                            </li>
                            <li class="nav-item" role="presentation"><a class="nav-link active" role="tab"
                                    data-bs-toggle="tab" href="#tab-3" style="color: rgb(0,0,0);">Selesai</a></li>

                        </ul>
                        <div class="tab-content">
                            {{-- Diproses --}}
                            <div class="tab-pane " role="tabpanel" id="tab-1">
                                <div>
                                    <div class="row row-cols-sm-2 row-cols-md-3 row-cols-xxl-4 justify-content-start align-items-start m-auto row-cols-1"
                                        style="padding-top: 10px;padding-bottom: 10px;">
                                        <div class="card-body">

                                            <div class="row">
                                                <table class="table table-striped center" id="myTable"
                                                    style="text-align: center">
                                                    <thead>
                                                        <tr>
                                                            <th>ID Pesanan</th>
                                                            <th>Nama Toko</th>
                                                            <th>Tanggal Transaksi</th>
                                                            <th>Tanggal Jatuh Tempo</th>
                                                            <th>Grand Total</th>
                                                            <th>Kurang Bayar</th>
                                                            <th>Status Pembayaran</th>
                                                            <th>Detail Produk</th>
                                                            <th>Print Nota</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @if ($counthdistributor != 0)
                                                            @foreach ($hproses as $item)
                                                                <tr>
                                                                    <td>{{ $item->id_htransaksi }}</td>
                                                                    <td>{{DB::table('user_distributor')->where('id_user',$item->id_userdistributor)->value('nama_toko')}}</td>
                                                                    <td>{{ Date('d F Y', strtotime($item->tgl_transaksi)) }}
                                                                    </td>
                                                                    <td>{{ Date('d F Y', strtotime($item->tgl_jatuh_tempo)) }}
                                                                    </td>
                                                                    <td>Rp. {{ number_format($item->grand_total) }}</td>
                                                                    <td>Rp. {{ number_format($item->kurang_bayar) }}</td>
                                                                    <td>
                                                                        @if ($item->status_bayar == 1)
                                                                            <div class="alert alert-success d-sm-flex d-lg-flex justify-content-sm-center align-items-sm-center justify-content-lg-center align-items-lg-center"
                                                                                role="alert"
                                                                                style="width: 200px;height: 35px;">
                                                                                <span><strong>Pembayaran
                                                                                        Lunas</strong></span></div>
                                                                        @else
                                                                            <div class="alert alert-danger d-sm-flex d-lg-flex justify-content-sm-center align-items-sm-center justify-content-lg-center align-items-lg-center"
                                                                                role="alert"
                                                                                style="width: 200px;height: 35px;">
                                                                                <span><strong>Pembayaran Belum
                                                                                        Lunas</strong></span></div>
                                                                        @endif
                                                                    </td>
                                                                    <td><a class="btn btn-primary"
                                                                            href="/detailpesanan/{{ $item->id_htransaksi }}">Detail
                                                                            Pembelian</a></td>
                                                                    <td>
                                                                        <a class="btn btn-warning" id="print"
                                                                            href="/admin/listpesanan/cetak/{{ $item->id_htransaksi }}" target="_blank">
                                                                            <i class="fa fa-print"></i>
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" role="tabpanel" id="tab-2">
                                <div>
                                    <div class="row row-cols-sm-2 row-cols-md-3 row-cols-xxl-4 justify-content-start align-items-start m-auto row-cols-1"
                                        style="padding-top: 10px;padding-bottom: 10px;">
                                        <div class="card-body">
                                            <div class="row">
                                                <table class="table table-striped center" id="example"
                                                    style="text-align: center">
                                                    <thead>
                                                        <tr>
                                                            <th>ID Pesanan</th>
                                                            <th>Nama Toko</th>
                                                            <th>Tanggal Transaksi</th>
                                                            <th>Tanggal Jatuh Tempo</th>
                                                            <th>Grand Total</th>
                                                            <th>Kurang Bayar</th>
                                                            <th>Status Pembayaran</th>
                                                            <th>Detail Produk</th>
                                                            <th>Print Nota</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @if ($counthdistributor != 0)
                                                            @foreach ($hselesai as $item)
                                                                <tr>
                                                                    <td>{{ $item->id_htransaksi }}</td>
                                                                    <td>{{DB::table('user_distributor')->where('id_user',$item->id_userdistributor)->value('nama_toko')}}</td>
                                                                    <td>{{ Date('d F Y', strtotime($item->tgl_transaksi)) }}
                                                                    </td>
                                                                    <td>{{ Date('d F Y', strtotime($item->tgl_jatuh_tempo)) }}
                                                                    </td>
                                                                    <td>Rp. {{ number_format($item->grand_total) }}</td>
                                                                    <td>Rp. {{ number_format($item->kurang_bayar) }}</td>
                                                                    <td>
                                                                        @if ($item->status_bayar == 1)
                                                                            <div class="alert alert-success d-sm-flex d-lg-flex justify-content-sm-center align-items-sm-center justify-content-lg-center align-items-lg-center"
                                                                                role="alert"
                                                                                style="width: 200px;height: 35px">
                                                                                <span><strong>Pembayaran
                                                                                        Lunas</strong></span></div>
                                                                        @else
                                                                            <div class="alert alert-danger d-sm-flex d-lg-flex justify-content-sm-center align-items-sm-center justify-content-lg-center align-items-lg-center"
                                                                                role="alert"
                                                                                style="width: 200px;height: 35px;">
                                                                                <span><strong>Pembayaran Belum
                                                                                        Lunas</strong></span></div>
                                                                        @endif
                                                                    </td>
                                                                    <td><a class="btn btn-primary"
                                                                            href="/detailpesanan/{{ $item->id_htransaksi }}">Detail
                                                                            Pembelian</a></td>
                                                                    <td>
                                                                        <a class="btn btn-warning"
                                                                            href="/admin/listpesanan/cetak/{{ $item->id_htransaksi }}" target="_blank">
                                                                            <i class="fa fa-print"></i>
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane active" role="tabpanel" id="tab-3">
                                {{-- Selesai --}}
                                <div>

                                    <div class="row p-3">
                                        <form action="/admin/filtertgltransaksi" method="post">
                                            @csrf
                                            <div class="input-group">
                                                <span class="input-group-text">Dari Tanggal</span><input type="date"
                                                    name="tgldari" id="">&nbsp;
                                                    - &nbsp;
                                                <span class="input-group-text">Sampai Tanggal</span><input type="date"
                                                    name="tglsampai" id="">
                                                &nbsp; <button type="submit" class="btn btn-primary">Pilih</button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="row row-cols-sm-2 row-cols-md-3 row-cols-xxl-4 justify-content-start align-items-start m-auto row-cols-1"
                                        style="padding-top: 10px;padding-bottom: 10px;">
                                        <div class="card-body">
                                            <div class="row">
                                                <table class="table table-striped center" id="selesai"
                                                    style="text-align: center">
                                                    <thead>
                                                        <tr>
                                                            <th>ID Pesanan</th>
                                                            <th>Nama Toko</th>
                                                            <th>Tanggal Transaksi</th>
                                                            <th>Tanggal Jatuh Tempo</th>
                                                            <th>Grand Total</th>
                                                            <th>Kurang Bayar</th>
                                                            <th>Status Pembayaran</th>
                                                            <th>Detail Produk</th>
                                                            <th>Print Nota</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @if ($counthdistributor != 0)
                                                            @foreach ($hselesai as $item)
                                                                <tr>
                                                                    <td>{{ $item->id_htransaksi }}</td>
                                                                    <td>{{DB::table('user_distributor')->where('id_user',$item->id_userdistributor)->value('nama_toko')}}</td>
                                                                    <td>{{ Date('d F Y', strtotime($item->tgl_transaksi)) }}
                                                                    </td>
                                                                    <td>{{ Date('d F Y', strtotime($item->tgl_jatuh_tempo)) }}
                                                                    </td>
                                                                    <td>Rp. {{ number_format($item->grand_total) }}</td>
                                                                    <td>Rp. {{ number_format($item->kurang_bayar) }}</td>
                                                                    <td>
                                                                        @if ($item->status_bayar == 1)
                                                                            <div class="alert alert-success d-sm-flex d-lg-flex justify-content-sm-center align-items-sm-center justify-content-lg-center align-items-lg-center"
                                                                                role="alert"
                                                                                style="width: 200px;height: 35px">
                                                                                <span><strong>Pembayaran
                                                                                        Lunas</strong></span></div>
                                                                        @else
                                                                            <div class="alert alert-danger d-sm-flex d-lg-flex justify-content-sm-center align-items-sm-center justify-content-lg-center align-items-lg-center"
                                                                                role="alert"
                                                                                style="width: 200px;height: 35px;">
                                                                                <span><strong>Pembayaran Belum
                                                                                        Lunas</strong></span></div>
                                                                        @endif
                                                                    </td>
                                                                    <td><a class="btn btn-primary"
                                                                            href="/detailpesanan/{{ $item->id_htransaksi }}">Detail
                                                                            Pembelian</a></td>
                                                                    <td>
                                                                        <a class="btn btn-warning"
                                                                            href="/admin/listpesanan/cetak/{{ $item->id_htransaksi }}" target="_blank">
                                                                            <i class="fa fa-print"></i>
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div><a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a>
        </div>
        <script>
            $(document).ready(function() {
                $('#myTable').DataTable();
            });
        </script>
        <script>
            $(document).ready(function() {
                $('#example').DataTable();
            });
        </script>
        <script>
            $(document).ready(function() {
                $('#selesai').DataTable();
            });
        </script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
        <script src="assets/js/bs-init.js?h=e2b0d57f2c4a9b0d13919304f87f79ae"></script>
        <script src="{{ url('asset/js/bs-init.js') }}"></script>
        <script src="{{ url('asset/js/theme.js') }}"></script>
        <script src="{{ url('assets/bootstrap/js/bootstrap.min.css') }}"></script>


        <script src="assets/js/theme.js?h=79f403485707cf2617c5bc5a2d386bb0"></script>


        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
        <script src="/assets/js/theme.js?h=79f403485707cf2617c5bc5a2d386bb0"></script>
    </body>
@endsection
