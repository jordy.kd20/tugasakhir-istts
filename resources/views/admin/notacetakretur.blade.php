@extends('template')
<head>
    <title>
        Cetak Nota Invoice
    </title>
</head>
@section('Content')
<body onload="window.print()">
    <div style="height: 20cm;">
        <div>
            <h1 class="text-center" style="padding-bottom: 20px;">Nota Invoice</h1>
        </div>
        <div>
            <div class="row">
                <div class="col  align-items-center">
                    <div>
                        <div class="row">
                            <div class="col">
                                <h5 class="text-center">CV Optimus Cahaya Abadi</h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <p class="text-center">JL Komp Pergudangan Margomulyo&nbsp;</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <p class="text-center">Jaya Blok I - 12&nbsp;</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <p style="text-align: center;">Margomulyo - Surabaya</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <p style="text-align: center;">Telp : 03199025157&nbsp;</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="row">
                        <div class="col-4  align-items-center">
                            <p class="align-items-right">ID NOTA :</p>
                        </div>
                        <div class="col">
                            <p>{{$id}}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-4 align-items-center">
                            <p class="align-items-xl-right">Tgl Retur :</p>
                        </div>
                        <div class="col">
                            <p>{{Date('d F Y', strtotime(DB::table('hretur')->where('id_hretur',$id)->value('tgl_retur')))}}</p>
                        </div>
                    </div>
                    <div class="row d-xl-flex justify-content-xl-center align-items-xl-center" style="width: 350px;">
                        <div class="col d-xl-flex justify-content-xl-center align-items-xl-center" style="border-style: double;">
                            <h3 style="text-align: center;">Untuk Customer</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="padding-top: 20px;">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Barang</th>
                            <th>Jumlah</th>
                        </tr>
                    </thead>
                    @php
                        $ctr = 1;
                    @endphp
                    <tbody>
                        @foreach ($datadretur as $item)
                            <tr>
                                <td>{{$ctr}}</td>
                                <td>{{DB::table('produk')->where('id_produk',$item->id_produk)->value('nama_produk')}}</td>
                                <td>{{$item->qtyretur}}</td>
                            </tr>
                            @php
                                $ctr +=1;
                            @endphp
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div style="height: 20cm;">
        <div>
            <h1 class="text-center" style="padding-bottom: 20px;">Nota Invoice</h1>
        </div>
        <div>
            <div class="row">
                <div class="col  align-items-center">
                    <div>
                        <div class="row">
                            <div class="col">
                                <h5 class="text-center">CV Optimus Cahaya Abadi</h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <p class="text-center">JL Komp Pergudangan Margomulyo&nbsp;</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <p class="text-center">Jaya Blok I - 12&nbsp;</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <p style="text-align: center;">Margomulyo - Surabaya</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <p style="text-align: center;">Telp : 03199025157&nbsp;</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="row">
                        <div class="col-4  align-items-center">
                            <p class="align-items-right">ID NOTA :</p>
                        </div>
                        <div class="col">
                            <p>{{$id}}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-4 align-items-center">
                            <p class="align-items-xl-right">Tgl Retur :</p>
                        </div>
                        <div class="col">
                            <p>{{Date('d F Y', strtotime(DB::table('hretur')->where('id_hretur',$id)->value('tgl_retur')))}}</p>
                        </div>
                    </div>
                    <div class="row d-xl-flex justify-content-xl-center align-items-xl-center" style="width: 350px;">
                        <div class="col d-xl-flex justify-content-xl-center align-items-xl-center" style="border-style: double;">
                            <h3 style="text-align: center;">Untuk Admin</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="padding-top: 20px;">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Barang</th>
                            <th>Jumlah</th>
                        </tr>
                    </thead>
                    @php
                        $ctr = 1;
                    @endphp
                    <tbody>
                        @foreach ($datadretur as $item)
                            <tr>
                                <td>{{$ctr}}</td>
                                <td>{{DB::table('produk')->where('id_produk',$item->id_produk)->value('nama_produk')}}</td>
                                <td>{{$item->qtyretur}}</td>
                            </tr>
                            @php
                                $ctr +=1;
                            @endphp
                        @endforeach
                        </tbody>
                </table>
            </div>
        </div>
    </div>
    <div style="height: 20cm;">
        <div>
            <h1 class="text-center" style="padding-bottom: 20px;">Nota Invoice</h1>
        </div>
        <div>
            <div class="row">
                <div class="col  align-items-center">
                    <div>
                        <div class="row">
                            <div class="col">
                                <h5 class="text-center">CV Optimus Cahaya Abadi</h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <p class="text-center">JL Komp Pergudangan Margomulyo&nbsp;</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <p class="text-center">Jaya Blok I - 12&nbsp;</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <p style="text-align: center;">Margomulyo - Surabaya</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <p style="text-align: center;">Telp : 03199025157&nbsp;</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="row">
                        <div class="col-4  align-items-center">
                            <p class="align-items-right">ID NOTA :</p>
                        </div>
                        <div class="col">
                            <p>{{$id}}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-4 align-items-center">
                            <p class="align-items-xl-right">Tgl Retur :</p>
                        </div>
                        <div class="col">
                            <p>{{Date('d F Y', strtotime(DB::table('hretur')->where('id_hretur',$id)->value('tgl_retur')))}}</p>
                        </div>
                    </div>
                    <div class="row d-xl-flex justify-content-xl-center align-items-xl-center" style="width: 350px;">
                        <div class="col d-xl-flex justify-content-xl-center align-items-xl-center" style="border-style: double;">
                            <h3 style="text-align: center;">Untuk Gudang</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="padding-top: 20px;">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Barang</th>
                            <th>Jumlah</th>
                        </tr>
                    </thead>
                    @php
                        $ctr = 1;
                    @endphp
                    <tbody>
                        @foreach ($datadretur as $item)
                            <tr>
                                <td>{{$ctr}}</td>
                                <td>{{DB::table('produk')->where('id_produk',$item->id_produk)->value('nama_produk')}}</td>
                                <td>{{$item->qtyretur}}</td>
                            </tr>
                            @php
                                $ctr +=1;
                            @endphp
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/bs-init.js?h=e2b0d57f2c4a9b0d13919304f87f79ae"></script>
    <script src="{{url('asset/js/bs-init.js')}}"></script>
    <script src="{{url('asset/js/theme.js')}}"></script>
    <script src="{{url('assets/bootstrap/js/bootstrap.min.css')}}"></script>
    
  
    <script src="assets/js/theme.js?h=79f403485707cf2617c5bc5a2d386bb0"></script>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="/assets/js/theme.js?h=79f403485707cf2617c5bc5a2d386bb0"></script>
</body>
@endsection