@extends('template')
<head>
    <title>Pembelian Produk Admin</title>
</head>
@section('Content')
<body id="page-top">
    <div id="wrapper">
        <nav class="navbar navbar-dark align-items-start sidebar sidebar-dark accordion bg-gradient-primary p-0" style="background: #171717;">
            <div class="container-fluid d-flex flex-column p-0"><a class="navbar-brand d-flex justify-content-center align-items-center sidebar-brand m-0" href="#">
                    <div class="sidebar-brand-icon rotate-n-15"><img src="{{url('asset/img/Logo.png')}}" style="width: 40px;height: 40px;transform: rotate(15deg);"></div>
                    <div class="sidebar-brand-text mx-3"><span style="font-size: 9px;">CV. Optimus Cahata ABADI</span></div>
                </a>
                <hr class="sidebar-divider my-0">
                <ul class="navbar-nav text-light" id="accordionSidebar">
                    <li class="nav-item"><a class="nav-link" href="/admin"><i class="fa fa-list-ul"></i><span>List Barang</span></a></li>
                    <li class="nav-item"><a class="nav-link" href="/admin/listasset"><i class="fa fa-building"></i><span>List Asset</span></a>
                        <a class="nav-link active" href="/admin/pembelianproduk"><i class="fa fa-cart-plus"></i><span>Pembelian produk</span></a></li>
                    <li class="nav-item"><a class="nav-link" href="/admin/penguranganstok"><i class="fa fa-exchange"></i><span>Update Stok</span></a></li>
                    <li class="nav-item"><a class="nav-link " href="/admin/stokreport"><i class="fa fa-exchange"></i><span>Laporan Stok</span></a></li>
                    <li class="nav-item"><a class="nav-link" href="/admin/listpembelian"><i class="fa fa-list-ul"></i><span>List Pembelian Produk</span></a></li>
                    <li class="nav-item"><a class="nav-link" href="/admin/listpembeliandistributor"><i class="fa fa-dollar"></i><span>List Pembelian Distributor</span></a></li>
                    <li class="nav-item"><a class="nav-link" href="/admin/listretur"><i class="fa fa-info"></i><span>List Retur Distributor</span></a></li>
                    <li class="nav-item"><a class="nav-link" href="/admin/listhutang"><i class="fa fa-money"></i><span>Hutang Distributor</span></a></li>
                </ul>
                <div class="text-center d-none d-md-inline"><button class="btn rounded-circle border-0" id="sidebarToggle" type="button"></button></div>
            </div>
        </nav>
        <div class="d-flex flex-column" id="content-wrapper">
            <div id="content">
                <nav class="navbar navbar-light navbar-expand bg-white shadow mb-4 topbar static-top">
                    <div class="container-fluid"><button class="btn btn-link d-md-none rounded-circle me-3" id="sidebarToggleTop" type="button"><i class="fas fa-bars"></i></button>
                        <ul class="navbar-nav flex-nowrap ms-auto">
                            <li class="nav-item dropdown d-sm-none no-arrow"><a class="dropdown-toggle nav-link" aria-expanded="false" data-bs-toggle="dropdown" href="#"><i class="fas fa-search"></i></a>
                                <div class="dropdown-menu dropdown-menu-end p-3 animated--grow-in" aria-labelledby="searchDropdown">
                                    <form class="me-auto navbar-search w-100">
                                        <div class="input-group"><input class="bg-light form-control border-0 small" type="text" placeholder="Search for ...">
                                            <div class="input-group-append"><button class="btn btn-primary py-0" type="button"><i class="fas fa-search"></i></button></div>
                                        </div>
                                    </form>
                                </div>
                            </li>
                            <li class="nav-item dropdown no-arrow">
                                <div class="nav-item dropdown no-arrow"><a class="dropdown-toggle nav-link" aria-expanded="false" data-bs-toggle="dropdown" href="#"><span class="d-none d-lg-inline me-2 text-gray-600 small">{{Session::get('active')}}</span><img class="border rounded-circle img-profile" src={{url('asset/img/avatars/avatar1.jpeg')}}></a>
                                    <div class="dropdown-menu shadow dropdown-menu-end animated--grow-in"><a class="dropdown-item" href="/showprofilepegawai"><i class="fas fa-user fa-sm fa-fw me-2 text-gray-400"></i>&nbsp;Edit Profile</a><a class="dropdown-item" href="/logout"><i class="fas fa-sign-out-alt fa-sm fa-fw me-2 text-gray-400"></i>&nbsp;Logout</a></div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
                <div class="container-fluid">
                    <h3 style="font-size: 38px;">Pembelian Produk</h3>               
                    <div class="card">
                        <div class="card-body">
                            <div>
                                <form action="/admin/addbeliproduk" method="POST" >
                                    @csrf
                                    
                                    <select class="form-select js-example-basic-single" name="pilih_produk" data-live-search="true">
                                        @foreach ($datakategori as $kategori)
                                            <optgroup label="{{$kategori->nama_kategori}}">
                                                    @foreach ($dataproduk as $item)
                                                        @if ($kategori->nama_kategori == $item->kategori_produk )
                                                            <option value="{{$item->id_produk}}">{{$item->nama_produk}}</option>
                                                        @endif
                                                    @endforeach
                                            </optgroup>
                                        @endforeach
                                        {{-- <optgroup label="Pilih Produk">
                                            <option value="12" selected="">This is item 1</option>
                                            <option value="13">This is item 2</option>
                                            <option value="14">This is item 3</option>
                                        </optgroup> --}}
                                    </select>
                                    
                                    <div class="row row-cols-1 row-cols-sm-1 row-cols-md-1 row-cols-lg-2 row-cols-xl-2 row-cols-xxl-2 d-lg-flex d-xl-flex d-xxl-flex align-items-md-end align-items-lg-end align-items-xl-end align-items-xxl-end">
                                        <div class="col">
                                            <div class="input-group" style="padding-top: 10px;"><span class="input-group-text">Jumlah Pembelian</span><input class="form-control" type="number"  name="jumlah_pembelian_produk"></div>
                                        </div>
                                        <div class="col">
                                            <div class="col d-xl-flex justify-content-xl-end align-items-xl-center"><button class="btn btn-primary" type="submit">Tambah Produk</button></div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive text-center shadow-sm d-flex d-xxl-flex table mt-2" id="dataTable-2" role="grid" aria-describedby="dataTable_info">
                                <table class="table table-hover table-bordered my-0" id="dataTable">
                                    <thead class="text-nowrap">
                                        <tr>
                                            <th style="padding-right: 40px;padding-left: 40px;">Nama Produk</th>
                                            <th>Jumlah per Lusin</th>
                                            <th>Jumlah Pembelian</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        @if (Session::has('pembelian'))
                                                @foreach (Session::get('pembelian') as $key => $item)
                                                    @if ($key == Session::get('active'))
                                                        @foreach ($item as $dataproduk)
                                                            @php
                                                                $namaproduk = DB::table('produk')->where('id_produk',$dataproduk['id_produk'])->value('nama_produk');
                                                                $totalbeli = $dataproduk['qtypembelian'] * $dataproduk['jumlahperdus'];
                                                            @endphp
                                                            <tr>
                                                                <td>{{$namaproduk}}</td>
                                                                <td>{{$dataproduk['qtypembelian']}}
                                                                <td>{{$totalbeli}}</td>
                                                                <td>
                                                                    <button class="btn btn-warning" data-mynama='{{$namaproduk}}' data-idproduk={{$dataproduk['id_produk']}} data-jumlahperdus={{$dataproduk['jumlahperdus']}} data-jumlah_lusin={{$dataproduk['qtypembelian']}} data-jumlah_total={{$totalbeli}} data-bs-toggle="modal" data-bs-target="#edit" > <i class="fa fa-edit"></i> </button>
                                                                    <a class="btn btn-danger" href="/admin/hapuspembelian/{{$dataproduk['id_produk']}}"><i class="fa fa-trash"></i></a>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @endif
                                                @endforeach
                                            {{-- @else
                                                <tr>
                                                    <td colspan="3" style="text-align: center">Tidak Ada Pembelian</td>
                                                </tr>
                                            @endif --}}
                                        @else
                                            <tr>
                                                <td colspan="3" style="text-align: center">Tidak Ada Pembelian</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col-md-6"></div>
                                <form action="/admin/checkoutpembelian" method="post">
                                    @csrf
                                    <div class="col-md-6 d-md-flex d-lg-flex d-xl-flex d-xxl-flex justify-content-md-end align-items-md-end justify-content-lg-end align-items-lg-end justify-content-xl-end align-items-xl-end justify-content-xxl-end align-items-xxl-end">
                                        <button class="btn btn-primary" type="submit">Order Produk</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="edit">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="/admin/editpembelian" method="post">
                            @csrf
                        <div class="modal-header">
                            <h4 class="modal-title">Edit Cart</h4><button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <div>
                                <div class="row">
                                    <div class="col"><label class="col-form-label">ID Barang&nbsp;</label></div>
                                    <div class="col-xxl-8"><input type="text" style="width: 300px;border-radius: 15px;" name="id_produk" id="id_produk" readonly=""></div>
                                </div>
                                <div class="row">
                                    <div class="col"><label class="col-form-label">Nama Barang&nbsp;</label></div>
                                    <div class="col-xxl-8"><input type="text" style="width: 300px;border-radius: 15px;" name="nama_produk" id="nama_produk" readonly=""></div>
                                </div>
                                <div class="row">
                                    <div class="col"><label class="col-form-label">jumlah/dus&nbsp;</label></div>
                                    <div class="col-xxl-8"><input type="text" style="width: 300px;border-radius: 15px;" name="jumlah_dus"  id='jumlah_dus' inputmode="numeric"></div>
                                </div>
                                <div class="row">
                                    <div class="col"><label class="col-form-label">Total Jumlah&nbsp;</label></div>
                                    <div class="col-xxl-8"><input type="text" style="width: 300px;border-radius: 15px;" name="jumlah_total"  id='jumlah_total' inputmode="numeric" readonly></div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer"><button class="btn btn-light" type="button" data-bs-dismiss="modal">Close</button><button class="btn btn-primary" type="submit">Submit</button></div>
                    </form>
                    </div>
                </div>
            </div>
            <footer class="bg-white sticky-footer">
                <div class="container my-auto">
                    <div class="text-center my-auto copyright"><span>Copyright © CV. Optimus Cahaya Abadi 2022</span></div>
                </div>
            </footer>
        </div><a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a>
    </div>

    <script>
        $(document).ready(function() {
            $('.js-example-basic-single').select2();
        });
    </script>
    <script>
        $('#edit').on('show.bs.modal', function(event) {
            var button = $(event.relatedTarget)
            var nama = button.data('mynama')
            var id = button.data('idproduk')
            
            var jumlah_dus = button.data('jumlah_lusin')
            var jumlah_total = button.data('jumlah_total')
            var jumlahperdus = button.data('jumlahperdus')
            var foto = "/produk/LED12WTRK.png"

            var total = (jumlah_dus*jumlahperdus)


            var modal =$(this)
            modal.find('.modal-body #nama_produk').val(nama);
            modal.find('.modal-body #jumlah_dus').val(jumlah_dus);
            modal.find('.modal-body #jumlah_total').val(total);
            modal.find('.modal-body #id_produk').val(id);
            // modal.find('.modal-body #showpic').val(foto);
            // $('.modal-body #showpic').attr('src') = foto;
        })
    </script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/bs-init.js?h=e2b0d57f2c4a9b0d13919304f87f79ae"></script>
    <script src="{{url('asset/js/bs-init.js')}}"></script>
    <script src="{{url('asset/js/theme.js')}}"></script>
    <script src="{{url('assets/bootstrap/js/bootstrap.min.css')}}"></script>
    
  
    <script src="assets/js/theme.js?h=79f403485707cf2617c5bc5a2d386bb0"></script>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="/assets/js/theme.js?h=79f403485707cf2617c5bc5a2d386bb0"></script>

</body>
@endsection