@extends('template')
<head>
    <title>Moving Barang</title>
</head>
@section('Content')
<body id="page-top">
    <div id="wrapper">
        <nav class="navbar navbar-dark align-items-start sidebar sidebar-dark accordion bg-gradient-primary p-0" style="padding-right: 0px;background: #171717;">
            <div class="container-fluid d-flex flex-column p-0"><a class="navbar-brand d-flex justify-content-center align-items-center sidebar-brand m-0" href="#">
                <div class="sidebar-brand-icon rotate-n-15" style="margin-right: -11px;"><img src="{{url('asset/img/Logo.png')}}" style="width: 40px;height: 40px;transform: rotate(15deg);"></div>
                <div class="sidebar-brand-text mx-3"><span style="font-size: 9px;">CV. OPTIMUS CAHAYA ABADI</span></div>
            </a>
            <ul class="navbar-nav text-light" id="accordionSidebar">
                <li class="nav-item"><a class="nav-link " href="/admin"><i class="fa fa-list-ul"></i><span>List Barang</span></a></li>
                    <li class="nav-item"><a class="nav-link" href="/admin/listasset"><i class="fa fa-building"></i><span>List Asset</span></a>
                        <a class="nav-link" href="/admin/pembelianproduk"><i class="fa fa-cart-plus"></i><span>Pembelian produk</span></a></li>
                    <li class="nav-item"><a class="nav-link" href="/admin/penguranganstok"><i class="fa fa-exchange"></i><span>Update Stok</span></a></li>
                    <li class="nav-item"><a class="nav-link active" href="/admin/stokreport"><i class="fa fa-exchange"></i><span>Laporan Stok</span></a></li>
                    <li class="nav-item"><a class="nav-link" href="/admin/listpembelian"><i class="fa fa-list-ul"></i><span>List Pembelian Produk</span></a></li>
                    <li class="nav-item"><a class="nav-link" href="/admin/listpembeliandistributor"><i class="fa fa-dollar"></i><span>List Pembelian Distributor</span></a></li>
                    <li class="nav-item"><a class="nav-link" href="/admin/listretur"><i class="fa fa-info"></i><span>List Retur Distributor</span></a></li>
                    <li class="nav-item"><a class="nav-link" href="/admin/listhutang"><i class="fa fa-money"></i><span>Hutang Distributor</span></a></li>
            </ul>
            <hr class="sidebar-divider my-0">
            <div class="text-center d-none d-md-inline"><button class="btn rounded-circle border-0" id="sidebarToggle" type="button"></button></div>
        </div>
        </nav>
        <div class="d-flex flex-column" id="content-wrapper">
            <div id="content">
                <nav class="navbar navbar-light navbar-expand bg-white shadow mb-4 topbar static-top">
                    <div class="container-fluid"><button class="btn btn-link d-md-none rounded-circle me-3" id="sidebarToggleTop" type="button"><i class="fas fa-bars"></i></button>
                        <ul class="navbar-nav flex-nowrap ms-auto">
                            <li class="nav-item dropdown d-sm-none no-arrow"><a class="dropdown-toggle nav-link" aria-expanded="false" data-bs-toggle="dropdown" href="#"><i class="fas fa-search"></i></a>
                                <div class="dropdown-menu dropdown-menu-end p-3 animated--grow-in" aria-labelledby="searchDropdown">
                                    <form class="me-auto navbar-search w-100">
                                        <div class="input-group"><input class="bg-light form-control border-0 small" type="text" placeholder="Search for ...">
                                            <div class="input-group-append"><button class="btn btn-primary py-0" type="button"><i class="fas fa-search"></i></button></div>
                                        </div>
                                    </form>
                                </div>
                            </li>
                            <li class="nav-item dropdown no-arrow">
                                <div class="nav-item dropdown no-arrow"><a class="dropdown-toggle nav-link" aria-expanded="false" data-bs-toggle="dropdown" href="#"><span class="d-none d-lg-inline me-2 text-gray-600 small">{{Session::get('active')}}</span><img class="border rounded-circle img-profile" src={{url('asset/img/avatars/avatar1.jpeg')}}></a>
                                    <div class="dropdown-menu shadow dropdown-menu-end animated--grow-in"><a class="dropdown-item" href="/showprofilepegawai"><i class="fas fa-user fa-sm fa-fw me-2 text-gray-400"></i>&nbsp;Edit Profile</a><a class="dropdown-item" href="/logout"><i class="fas fa-sign-out-alt fa-sm fa-fw me-2 text-gray-400"></i>&nbsp;Logout</a></div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
                <div class="container-fluid">
                    <h3 class="text-dark mb-4">Stok</h3>
                    <div class="row p-3">
                        <form action="/owner/filtertgltstok" method="post">
                            @csrf
                            <div class="input-group">
                                <span class="input-group-text">Dari Tanggal</span><input type="date"
                                    name="tgldari" id="">&nbsp;
                                    - &nbsp;
                                <span class="input-group-text">Sampai Tanggal</span><input type="date"
                                    name="tglsampai" id="">
                                &nbsp; <button type="submit" class="btn btn-primary">Pilih</button>
                                &nbsp; <a href="/owner/stokreport" class="btn btn-warning">Reset Filter</a>
                            </div>
                        </form>
                    </div>
                    <div class="card shadow">
                        {{-- <div class="card-header d-sm-flex py-3"><button class="btn btn-primary d-flex d-sm-flex d-md-flex d-lg-flex d-xl-flex d-xxl-flex flex-row-reverse justify-content-center align-items-center flex-sm-row-reverse flex-md-row-reverse flex-lg-row-reverse flex-xl-row-reverse justify-content-xl-center align-items-xl-center flex-xxl-row-reverse" type="button" data-bs-target="#modal-insert-barang" data-bs-toggle="modal" style="margin-right: 10px;margin-bottom: 10px;">Tambah Barang<i class="fa fa-plus" style="margin-left: 0px;margin-right: 10px;"></i></button><button class="btn btn-primary d-flex d-sm-flex d-md-flex d-lg-flex d-xl-flex d-xxl-flex flex-row-reverse justify-content-center align-items-center flex-sm-row-reverse flex-md-row-reverse flex-lg-row-reverse flex-xl-row-reverse justify-content-xl-center align-items-xl-center flex-xxl-row-reverse" type="button" data-bs-target="#modal-insert-kategori" data-bs-toggle="modal" style="margin-right: 10px;margin-bottom: 10px;">Tambah Kategori<i class="fa fa-plus" style="margin-left: 0px;margin-right: 10px;"></i></button></div> --}}
                        <div class="card-body">
                            <div class="table-responsive table " id="dataTable"  aria-describedby="dataTable_info">
                                <table class="table table-striped" id="example">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Barang</th>
                                            <th>Tanggal</th>
                                            <th>jumlah</th>
                                            <th>Keterangan</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                            @if ($isi==1)
                                            @php
                                                $ctr = 1;
                                            @endphp
                                                @foreach ($pencatatanstok as $item)
                                                <tr>
                                                    <td>{{$ctr}}</td>
                                                    <td>{{DB::table('produk')->where('id_produk',$item->id_produk)->value('nama_produk')}}</td>
                                                    <td>{{Date('d F Y',strtotime($item->created_at))}}</td>
                                                    <td>{{$item->jumlah}} @if ($item->status == 0)
                                                        <i class="fa fa-arrow-down" style="color:red"></i>
                                                    @else
                                                        <i class="fa fa-arrow-up" style="color:green"></i>
                                                    @endif</td>
                                                    <td>{{$item->keterangan}}</td>
                                                    <td> @if ($item->status == 0)
                                                        <div class="alert alert-danger d-sm-flex d-lg-flex justify-content-sm-center align-items-sm-center justify-content-lg-center align-items-lg-center" role="alert" style="width: 200px;height: 35px;"><span><strong>Barang Keluar</strong></span></div>
                                                    @else
                                                        <div class="alert alert-success d-sm-flex d-lg-flex justify-content-sm-center align-items-sm-center justify-content-lg-center align-items-lg-center" role="alert" style="width: 200px;height: 35px;"><span><strong>Barang Masuk</strong></span></div>
                                                    @endif</td>
                                                    
                                                </tr>
                                                @php
                                                    $ctr += 1;
                                                @endphp
                                                @endforeach
                                            @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            <footer class="bg-white sticky-footer">
                <div class="container my-auto">
                    <div class="text-center my-auto copyright"><span>Copyright © Elektronik Home 2021</span></div>
                </div>
            </footer>
        </div><a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a>
    </div>
    <script>
        $(document).ready(function () {
        $('#example').DataTable();
    });
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/bs-init.js?h=e2b0d57f2c4a9b0d13919304f87f79ae"></script>
    <script src="{{url('asset/js/bs-init.js')}}"></script>
    <script src="{{url('asset/js/theme.js')}}"></script>
    <script src="{{url('assets/bootstrap/js/bootstrap.min.css')}}"></script>
    
  
    <script src="assets/js/theme.js?h=79f403485707cf2617c5bc5a2d386bb0"></script>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="/assets/js/theme.js?h=79f403485707cf2617c5bc5a2d386bb0"></script>
</body>
@endsection