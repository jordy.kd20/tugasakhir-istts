@extends('template')
<head>
    <title>Update Produk</title>
</head>
@section('Content')
<body class="bg-gradient-primary">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-9 col-lg-12 col-xl-10" style="margin-top: 17px;">
                <div class="card shadow-lg o-hidden border-0 my-5">
                    <div class="card-body p-0">
                        @foreach ($dataasset as $item)
                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-flex">
                                <div class="flex-grow-1 bg-login-image"><img src="{{url('/asset/img/doodle.jpg')}}" style="width: 100%; "></div>
                            </div>
                            <div class="col-lg-6">
                                <div class="p-5" style="margin-top: 25px;margin-bottom: 25px;">
                                    <div class="text-center">
                                        <h4 class="text-dark mb-4">Update Produk</h4>
                                       
                                        <form method="POST" action="/admin/submitasset">
                                            @csrf
                                            <div class="input-group"><input class="form-control" type="hidden" name="id_asset" value="{{$item->id_asset}}"  ></div>
                                            <div class="input-group"><span class="input-group-text">Nama Asset</span><input class="form-control" type="text" name="nama_asset" value="{{$item->nama_asset}}" readonly ></div>
                                            <div class="input-group" style="padding-top: 10px;"><span class="input-group-text">jumlah</span><input class="form-control" type="number" name="jumlahasset" value="{{$item->jumlah_asset}}"></div>
                                            
                                            <div class="input-group" style="padding-top: 10px;"><span class="input-group-text">Harga Asset</span><input class="form-control" type="number" name="harga_asset" value="{{$item->harga_asset}}" ></div>
                                            <select class="form-select" name="keadaan" style="margin-top: 10px;">
                                                <option value="{{$item->keadaan_asset}}">{{$item->keadaan_asset}}</option>
                                                <option value="Baik" >Baik</option>
                                                <option value="Sedang Diperbaiki">Sedang Diperbaiki</option>
                                                <option value="Diganti">Diganti</option>
                                                <option value="Rusak">Rusak</option>
                                            </select>
                                            <div class="input-group" style="padding-top: 10px;"><span class="input-group-text">Keterangan</span><input class="form-control" type="text" name="keterangan_asset" value="{{$item->keterangan_asset}}"></div>
                                                
                                                <div class="input-group" style="padding-top: 10px;"></div>
                                                <div class="input-group" style="padding-top: 10px;"></div><button class="btn btn-primary d-block btn-user w-100" type="submit" style="margin-top: 12px;">Update</button>
                                            </form>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" role="dialog" tabindex="-1" id="modal-update-barang">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Update Barang</h4><button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer"><button class="btn btn-light" type="button" data-bs-dismiss="modal">Close</button><button class="btn btn-primary" type="button">Ubah</button></div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/bs-init.js?h=e2b0d57f2c4a9b0d13919304f87f79ae"></script>
    <script src="{{url('asset/js/bs-init.js')}}"></script>
    <script src="{{url('asset/js/theme.js')}}"></script>
    <script src="{{url('assets/bootstrap/js/bootstrap.min.css')}}"></script>
    
  
    <script src="assets/js/theme.js?h=79f403485707cf2617c5bc5a2d386bb0"></script>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="/assets/js/theme.js?h=79f403485707cf2617c5bc5a2d386bb0"></script>

</body>
@endsection
