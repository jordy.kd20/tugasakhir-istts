@extends('template')

<head>
    <title>Update Produk</title>
</head>
@section('Content')

    <body class="bg-gradient-primary">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-9 col-lg-12 col-xl-10" style="margin-top: 17px;">
                    <div class="card shadow-lg o-hidden border-0 my-5">
                        <div class="card-body p-0">
                            @foreach ($dataproduk as $item)
                                <div class="row">
                                    <div class="col-lg-6 d-none d-lg-flex">
                                        <div class="flex-grow-1 bg-login-image"><img src="{{ url("$foto") }}"
                                                style="height: 100%; "></div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="p-5" style="margin-top: 25px;margin-bottom: 25px;">
                                            <div class="text-center">
                                                <h4 class="text-dark mb-4">Update Produk</h4>

                                                <form method="POST" action="/admin/submitproduk">
                                                    @csrf
                                                    <div class="input-group" style="padding-top: 10px;"><span
                                                            class="input-group-text">Nama Produk</span><input
                                                            class="form-control" type="text" placeholder="Nama Produk"
                                                            name="nama_produk" value="{{ $item->nama_produk }}" readonly>
                                                    </div>

                                                    <div
                                                        class="row row-cols-1 row-cols-sm-1 row-cols-md-1 row-cols-lg-2 row-cols-xl-2 row-cols-xxl-2 d-lg-flex d-xl-flex d-xxl-flex align-items-md-end align-items-lg-end align-items-xl-end align-items-xxl-end">
                                                        <div class="col">
                                                            <div class="input-group" style="padding-top: 10px;"><span
                                                                    class="input-group-text">Daya (Watt)</span><input
                                                                    class="form-control" type="text" placeholder="Daya"
                                                                    name="daya_produk" value="{{ $item->daya }}" disabled>
                                                            </div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="input-group" style="padding-top: 10px;"><span
                                                                    class="input-group-text">Tegangan</span><input
                                                                    class="form-control" type="text"
                                                                    placeholder="Tegangan" name="tengangan_produk"
                                                                    value="{{ $item->tegangan }}" disabled></div>
                                                        </div>
                                                    </div>
                                                    <div
                                                        class="row row-cols-1 row-cols-sm-1 row-cols-md-1 row-cols-lg-2 row-cols-xl-2 row-cols-xxl-2 d-lg-flex d-xl-flex d-xxl-flex align-items-md-end align-items-lg-end align-items-xl-end align-items-xxl-end">
                                                        <div class="col">
                                                            <div class="input-group" style="padding-top: 10px;"><span
                                                                    class="input-group-text">Warna Lampu</span><input
                                                                    class="form-control" type="text"
                                                                    placeholder="Warna Lampu" name="warna_lampu"
                                                                    value="{{ $item->warna_lampu }}"></div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="input-group" style="padding-top: 10px;"><span
                                                                    class="input-group-text">Flux</span><input
                                                                    class="form-control" type="text" placeholder="Flux"
                                                                    name="flux_produk" value="{{ $item->flux }}"></div>
                                                        </div>
                                                    </div>
                                                    <div
                                                        class="row row-cols-1 row-cols-sm-1 row-cols-md-1 row-cols-lg-2 row-cols-xl-2 row-cols-xxl-2 d-lg-flex d-xl-flex d-xxl-flex align-items-md-end align-items-lg-end align-items-xl-end align-items-xxl-end">
                                                        <div class="col">
                                                            <div class="input-group" style="padding-top: 10px;"><span
                                                                    class="input-group-text">Stok</span><input
                                                                    class="form-control" type="text" placeholder="Stok"
                                                                    name="stok_produk" value="{{ $item->stok_produk }}">
                                                            </div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="input-group" style="padding-top: 10px;"><span
                                                                    class="input-group-text">Jumlah Per Dus</span><input
                                                                    class="form-control" type="text"
                                                                    placeholder="Jumlah Per Dus" name="jumlahperdus"
                                                                    value="{{ $item->jumlahperdus }}"></div>
                                                        </div>
                                                    </div>
                                                    <div
                                                        class="row row-cols-1 row-cols-sm-1 row-cols-md-1 row-cols-lg-2 row-cols-xl-2 row-cols-xxl-2 d-lg-flex d-xl-flex d-xxl-flex align-items-md-end align-items-lg-end align-items-xl-end align-items-xxl-end">
                                                        <div class="col">
                                                            <div class="input-group" style="padding-top: 10px;"><span
                                                                    class="input-group-text">Harga</span><input
                                                                    class="form-control" type="number" name="harga_produk"
                                                                    value="{{ $item->harga_produk }}"></div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="input-group" style="padding-top: 10px;"><span
                                                                    class="input-group-text">Berat Produk (g)</span><input
                                                                    class="form-control" type="number"
                                                                    placeholder="Berat Produk" name="berat_produk"
                                                                    value="{{ $item->berat_produk }}"></div>
                                                        </div>
                                                    </div>
                                                    {{-- <div class="input-group" style="padding-top: 10px;"><input class="form-control" type="text" placeholder="Harga" name="harga_produk"> --}}
                                                    <div class="input-group" style="padding-top: 10px;"><span
                                                            class="input-group-text">Merek</span><input
                                                            class="form-control" type="text" name="merek_produk"
                                                            value="{{ $item->merek }}" readonly></div>
                                                    <div class="input-group" style="padding-top: 10px;"><input
                                                            class="form-control" type="hidden" name="id_produk"
                                                            value="{{ $item->id_produk }}" readonly>
                                                        <div class="input-group" style="padding-top: 10px;">
                                                            <select class="form-select" name="kategori_produk"
                                                                style="margin-top: 10px;" disabled>
                                                                <option value="" selected>
                                                                    {{ $item->kategori_produk }}</option>
                                                                @foreach ($namakategori as $item)
                                                                    <option value="{{ $item->nama_kategori }}">
                                                                        {{ $item->nama_kategori }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>

                                                    </div>

                                                    <div class="input-group" style="padding-top: 10px;"></div>
                                                    <div class="input-group" style="padding-top: 10px;"></div><button
                                                        class="btn btn-primary d-block btn-user w-100" type="submit"
                                                        style="margin-top: 12px;">Update</button>
                                                </form>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" role="dialog" tabindex="-1" id="modal-update-barang">
            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Update Barang</h4><button type="button" class="btn-close"
                            data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body"></div>
                    <div class="modal-footer"><button class="btn btn-light" type="button"
                            data-bs-dismiss="modal">Close</button><button class="btn btn-primary"
                            type="button">Ubah</button></div>
                </div>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
        <script src="assets/js/bs-init.js?h=e2b0d57f2c4a9b0d13919304f87f79ae"></script>
        <script src="{{ url('asset/js/bs-init.js') }}"></script>
        <script src="{{ url('asset/js/theme.js') }}"></script>
        <script src="{{ url('assets/bootstrap/js/bootstrap.min.css') }}"></script>


        <script src="assets/js/theme.js?h=79f403485707cf2617c5bc5a2d386bb0"></script>


        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
        <script src="/assets/js/theme.js?h=79f403485707cf2617c5bc5a2d386bb0"></script>

    </body>
@endsection
