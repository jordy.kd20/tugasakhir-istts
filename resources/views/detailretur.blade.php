@extends('template')
<head>
    <title>Detail Retur</title>
    <style>
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
            }

            /* Modal Content (Image) */
            .modal-content {
            margin: auto;
            display: block;
            width: 80%;
            max-width: 300px;
            }

            /* Caption of Modal Image (Image Text) - Same Width as the Image */
            #caption {
            margin: auto;
            display: block;
            width: 80%;
            max-width: 300px;
            text-align: center;
            color: #ccc;
            padding: 10px 0;
            height: 150px;
            }

            /* Add Animation - Zoom in the Modal */
            .modal-content, #caption {
            animation-name: zoom;
            animation-duration: 0.6s;
            }

            @keyframes zoom {
            from {transform:scale(0)}
            to {transform:scale(1)}
            }

            /* The Close Button */
            .close {
            position: absolute;
            top: 15px;
            right: 35px;
            color: #f1f1f1;
            font-size: 40px;
            font-weight: bold;
            transition: 0.3s;
            }

            .close:hover,
            .close:focus {
            color: #bbb;
            text-decoration: none;
            cursor: pointer;
            }

            /* 100% Image Width on Smaller Screens */
            @media only screen and (max-height: 300px){
            .modal-content {
                width: 60%;
            }
            }
    </style>
</head>
@section('Content')
<body class="bg-gradient-primary">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-9 col-lg-12 col-xl-10" style="margin-top: 17px;">
                <div class="card shadow-lg o-hidden border-0 my-5">
                    <div class="card-body p-0">
                        <div class="row">
                            <div class="col">
                                @if (Session::has('loginadmin'))
                                    <div class="d-md-flex justify-content-md-start align-items-md-center" style="padding-top: 10px;padding-left: 10px;"><a class="btn btn-danger" href="/admin/listretur">Back</a></div>
                                @elseif(Session::has('distributoractive'))
                                    <div class="d-md-flex justify-content-md-start align-items-md-center" style="padding-top: 10px;padding-left: 10px;"><a class="btn btn-danger" href="/distributor/listretur">Back</a></div>
                                @endif
                            </div>
                            <div class="col">
                                {{-- 1 pesanan dikirim, 2 pesanan selesai, 3 pesanan ditolak --}}
                                <div class="d-md-flex justify-content-md-center align-items-md-center" style="padding-top: 10px;padding-left: 10px;">
                                    @if (Session::has('loginadmin'))
                                        @if (DB::table('hretur')->where('id_hretur',$idhretur)->value('status_pengiriman')==0)
                                            <div style="padding-right: 10px;"><a class="btn btn-danger" href="/admin/updatestatuspengirimanretur/{{$idhretur}}/3">Tolak Retur</a></div>
                                        @endif
                                        @if (DB::table('hretur')->where('id_hretur',$idhretur)->value('status_pengiriman')==0)
                                            <a class="btn btn-success" href="/admin/updatestatuspengirimanretur/{{$idhretur}}/1">Kirim Retur</a>
                                        @endif
                                        @if(DB::table('hretur')->where('id_hretur',$idhretur)->value('status_pengiriman')==1)
                                            <a class="btn btn-success" href="/admin/updatestatuspengirimanretur/{{$idhretur}}/2">Pesanan Diterima</a>
                                        @endif
                                    @elseif(Session::has('distributoractive'))
                                        @if (DB::table('hretur')->where('id_hretur',$idhretur)->value('status_pengiriman')==1)
                                            <a class="btn btn-success" href="/admin/updatestatuspengirimanretur/{{$idhretur}}/2">Pesanan Diterima</a>
                                        @else

                                        @endif
                                        
                                    @endif
                                    
                                    
                                </div>
                            </div>
                            <div class="col">
                                @if (Session::has('loginadmin'))
                                    <div class="d-flex justify-content-end align-items-center" style="padding-top: 10px;padding-right: 10px"><a class="btn btn-warning" target="_blank" href="/admin/cetaknotaretur/{{$idhretur}}" >Cetak Nota <i class="fa fa-print"></i></a></div>
                                @elseif(Session::has('distributoractive'))
                                    <div class="d-flex justify-content-end align-items-center" style="padding-top: 10px;padding-right: 10px"><a class="btn btn-warning" target="_blank" href="/distributor/cetaknotaretur/{{$idhretur}}">Cetak Nota <i class="fa fa-print"></i></a></div>
                                @endif
                                
                            </div>
                            
                        </div>
                        <div class="p-3" style="margin-top: 25px;margin-bottom: 25px;">
                            <div class="text-center">
                                <h4 class="text-dark mb-4" style="margin-bottom: 0px;">Detail Produk Retur</h4>
                            </div>
                        </div>
                        <section>
                            <div>
                               
                                <h4 class="d-sm-flex justify-content-sm-center align-items-sm-center" style="padding-bottom: 20px;">Bukti Retur</h4>
                                <div class="d-sm-flex justify-content-sm-center align-items-sm-center"><img id="myImg" class="d-sm-flex mb-3 col-sm-2" src={{url(DB::table('hretur')->where('id_hretur',$idhretur)->value('fotobuktiretur'))}}></div>
                              
                                <div>
                                    <div class="row row-cols-sm-2 row-cols-md-3 row-cols-xxl-4 justify-content-start align-items-start m-auto row-cols-1" style="padding-top: 10px;padding-bottom: 10px;">
                                        <div class="card-body">
                                            <div class="row">
                                                <table class="table table-striped center" id="example" style="text-align: center"> 
                                                    <thead>
                                                        <tr>
                                                            <th>No</th>
                                                            <th>Nama Produk</th>
                                                            <th>Jumlah Retur</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                            $ctr = 1;
                                                           
                                                        @endphp
                                                       @isset($dataretur)
                                                           @foreach ($dataretur as $item)
                                                               <tr>
                                                                    <td>{{$ctr}}</td>
                                                                    <td>{{DB::table('produk')->where('id_produk',$item->id_produk)->value('nama_produk')}}</td>
                                                                    <td>{{$item->qtyretur}}</td>
                                                               </tr>
                                                               @php
                                                                  $ctr+=1;
                                                                  
                                                               @endphp
                                                           @endforeach
                                                       @endisset
                                                    </tbody> 
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    {{-- <form action="/admin/updatestatuspengiriman" method="post">
                                        @csrf
                                        <input type="hidden" name="idhtrans" value="{{$idhtrans}}">
                                        <div style="width: 600px; padding-left: 20px; padding-top: 10px;padding-bottom: 10px;" class="input-group"><span class="input-group-text">Ubah Status Pengiriman</span><select class="form-control" name="statuspengiriman" id="">
                                            <option value="0">Diproses</option>
                                            <option value="1">Dikirim</option>
                                            <option value="2">Selesai</option>    
                                        </select> &nbsp; <button class="btn btn-primary" type="submit">Submit</button></div>
                                    </form> --}}
                                </div>
                                <div id="myModal" class="modal">

                                    <!-- The Close Button -->
                                    <span class="close">&times;</span>
                                  
                                    <!-- Modal Content (The Image) -->
                                    <img class="modal-content" id="img01">
                                  
                                    <!-- Modal Caption (Image Text) -->
                                    {{-- <div id="caption"></div> --}}
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('#example').DataTable();
        });

        var modal = document.getElementById("myModal");

        // Get the image and insert it inside the modal - use its "alt" text as a caption
        var img = document.getElementById("myImg");
        var modalImg = document.getElementById("img01");
        // var captionText = document.getElementById("caption");
        img.onclick = function(){
        modal.style.display = "block";
        modalImg.src = this.src;
        captionText.innerHTML = this.alt;
        }

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
        modal.style.display = "none";
        }
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="{{url('asset/js/bs-init.js')}}"></script>
    <script src="{{url('asset/js/theme.js')}}"></script>
    <script src="{{url('assets/bootstrap/js/bootstrap.min.css')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
</body>
@endsection