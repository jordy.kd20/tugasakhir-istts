@extends('template')
<head>
    <title>Keranjang</title>
    
</head>
@section('Content')
<body id="page-top">
    <div class="modal fade" role="dialog" tabindex="-1" id="modal-1">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-start">Pembayaran</h4><button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body d-xl-flex align-items-xl-center">
                    <div><a class="btn btn-primary" data-bs-toggle="collapse" aria-expanded="true" aria-controls="collapse-1" href="#collapse-1" role="button" style="margin-right: 20px;">Pembayaran Tunai</a>
                        <div class="collapse show" id="collapse-1">
                            <p>Collapse content.</p>
                        </div>
                    </div>
                    <div><a class="btn btn-primary" data-bs-toggle="collapse" aria-expanded="true" aria-controls="collapse-2" href="#collapse-2" role="button">Pembayaran QRISS</a>
                        <div class="collapse show" id="collapse-2">
                            <p>Collapse content.</p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer"><button class="btn btn-light" type="button" data-bs-dismiss="modal">Close</button><button class="btn btn-primary" type="button">Save</button></div>
            </div>
        </div>
    </div>
    <div id="wrapper">
        <nav class="navbar navbar-dark align-items-start sidebar sidebar-dark accordion bg-gradient-primary p-0" style="padding-right: 0px;background: #171717;">
            <div class="container-fluid d-flex flex-column p-0"><a class="navbar-brand d-flex justify-content-center align-items-center sidebar-brand m-0" href="#">
                    <div class="sidebar-brand-icon rotate-n-15" style="margin-right: -11px;"><img src="{{url('asset/img/Logo.png')}}" style="width: 40px;height: 40px;transform: rotate(15deg);"></div>
                    <div class="sidebar-brand-text mx-3"><span style="font-size: 9px;">CV. OPTIMUS CAHAYA ABADI</span></div>
                </a>
                <ul class="navbar-nav text-light" id="accordionSidebar">
                    <li class="nav-item"><a class="nav-link " href="/distributor/indexdistributor"><i class="far fa-money-bill-alt"></i><span>Transaksi</span></a></li>
                    <li class="nav-item"><a class="nav-link active" href="/distributor/cart"><i class="fa fa-shopping-cart"></i><span>Keranjang</span></a></li>
                    <li class="nav-item"><a class="nav-link" href="/distributor/pembelian"><i class="fa fa-dollar"></i><span>Pembelian</span></a></li>
                    <li class="nav-item"><a class="nav-link " href="/distributor/listretur"><i class="fa fa-list"></i><span>List Retur</span></a></li>
                    <li class="nav-item"><a class="nav-link" href="/distributor/returproduk"><i class="fa fa-sort"></i><span>Retur Barang</span></a></li>
                </ul>
                <hr class="sidebar-divider my-0">
                <div class="text-center d-none d-md-inline"><button class="btn rounded-circle border-0" id="sidebarToggle" type="button"></button></div>
            </div>
        </nav>
        <div class="d-flex flex-column" id="content-wrapper">
            <div id="content">
                <nav class="navbar navbar-light navbar-expand bg-white shadow mb-4 topbar static-top">
                    <div class="container-fluid"><button class="btn btn-link d-md-none rounded-circle me-3" id="sidebarToggleTop" type="button"><i class="fas fa-bars"></i></button>
                        <ul class="navbar-nav flex-nowrap ms-auto">
                            <li class="nav-item dropdown d-sm-none no-arrow"><a class="dropdown-toggle nav-link" aria-expanded="false" data-bs-toggle="dropdown" href="#"><i class="fas fa-search"></i></a>
                                <div class="dropdown-menu dropdown-menu-end p-3 animated--grow-in" aria-labelledby="searchDropdown">
                                    <form class="me-auto navbar-search w-100">
                                        <div class="input-group"><input class="bg-light form-control border-0 small" type="text" placeholder="Search for ...">
                                            <div class="input-group-append"><button class="btn btn-primary py-0" type="button"><i class="fas fa-search"></i></button></div>
                                        </div>
                                    </form>
                                </div>
                            </li>
                            <li class="nav-item dropdown no-arrow">
                                <div class="nav-item dropdown no-arrow"><a class="dropdown-toggle nav-link" aria-expanded="false" data-bs-toggle="dropdown" href="#"><span class="d-none d-lg-inline me-2 text-gray-600 small">{{Session::get('distributoractive')}}</span><img class="border rounded-circle img-profile" src="{{url(Session::get('distributorfoto'))}}"></a>
                                    <div class="dropdown-menu shadow dropdown-menu-end animated--grow-in"><a class="dropdown-item" href="/distributor/profile"><i class="fas fa-user fa-sm fa-fw me-2 text-gray-400"></i>&nbsp;Edit Profile</a><a class="dropdown-item" href="/distributor/logout"><i class="fas fa-sign-out-alt fa-sm fa-fw me-2 text-gray-400"></i>&nbsp;Logout</a></div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
                <div class="container-fluid">
                    <h3 class="text-dark mb-4">Keranjang</h3>
                    <div class="card shadow">
                        <div class="card-header py-3">
                            <div class="row">
                                <div class="col">
                                    <p class="text-primary m-0 fw-bold" style="color: rgb(78, 115, 223);">No nota</p>
                                </div>
                                <div class="col">
                                    <p class="text-end text-primary m-0 fw-bold">Tanggal Nota</p>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="text-md-end dataTables_filter" id="dataTable_filter"><input type="search" class="form-control form-control-sm" aria-controls="dataTable" placeholder="Search"></div>
                                </div>
                            </div>
                            <div class="table-responsive table mt-2" id="dataTable" role="grid" aria-describedby="dataTable_info">
                                <table class="table my-0" id="dataTable">
                                    <thead>
                                        <tr>
                                            <th style="width: 209px;">Barang</th>
                                            <th style="width: 155px;">Jumlah</th>
                                            <th style="width: 96px;">Harga</th>
                                            <th style="width: 118px;">Total</th>
                                            <th style="width: 118px;">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $subtotal = 0;
                                            $totalberat = 0;
                                        @endphp
                                        @if (Session::has('cart'))
                                            @foreach (Session::get('cart') as $key => $item)
                                                @if ($key == Session::get('distributoractive'))
                                                    
                                                    @foreach ($item as $databarang)
                                                        <tr>
                                                            @php
                                                                $namabarang = "";
                                                                $totalbarang = 0;
                                                                $totalharga = 0;
                                                                $namabarang = DB::table('produk')->where('id_produk',$databarang['id_produk'])->value('nama_produk');
                                                                $beratbarang = DB::table('produk')->where('id_produk', $databarang['id_produk'])->value('berat_produk');
                                                                $totalbarang = $databarang['qtysatuan'] + ($databarang['qtylusin'] * $databarang['jumlahperdus']);
                                                                $totalharga = $totalbarang * $databarang['harga_produk'];
                                                                $totalberat += $beratbarang * $totalbarang;
                                                                $subtotal += $totalharga
                                                            @endphp
                                                            <td>{{$namabarang}}</td>
                                                            <td>
                                                                <a href="/distributor/tambahcart/{{$databarang['id_produk']}}" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                                                                {{$totalbarang}}
                                                                <a href="/distributor/kurangcart/{{$databarang['id_produk']}}" class="btn btn-danger"><i class="fa fa-minus"></i></a>
                                                            </td>
                                                            <td>Rp. {{number_format($databarang['harga_produk']) }}</td>
                                                            <td>Rp. {{number_format($totalharga) }}</td>
                                                            <td>
                                                                <button class="btn btn-warning" data-mynama='{{$namabarang}}' data-idproduk={{$databarang['id_produk']}} data-jumlah_satuan={{$databarang['qtysatuan']}} data-jumlahperdus={{$databarang['jumlahperdus']}} data-jumlah_lusin={{$databarang['qtylusin']}} data-jumlah_total={{$totalbarang}} data-bs-toggle="modal" data-bs-target="#edit" > <i class="fa fa-edit"></i> </button>
                                                                <a class="btn btn-danger" href="/distributor/hapuscart/{{$databarang['id_produk']}}"><i class="fa fa-trash"></i></a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                            @endforeach
                                        @endif
                                    </tbody>
                                    <tfoot>
                                        <tr></tr>
                                    </tfoot>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col text-end d-flex d-sm-flex d-xxl-flex justify-content-end justify-content-sm-end align-items-sm-center justify-content-xxl-end align-items-xxl-center">
                                    <h5 class="text-primary d-xxl-flex align-items-xxl-center" style="margin-right: 15px;color: rgb(78,115,223);margin-top: 5px;">Total Belanja Rp. {{number_format($subtotal)}}</h5>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="margin-left: 14px;">
                <div class="row">
                    <div class="col" style="margin-left: 0px;margin-top: 0px;">
                        <h3 class="text-dark mb-4" style="width: 921px;margin-top: 0px;margin-left: 0px;">Pilih Pengiriman</h3>
                    </div>
                </div>
                <div class="row" style="margin-right: 0px;">
                    <div class="col">
                        <form method="post" action="/cekongkir">
                            @csrf
                            <label class="form-label">Provinsi&nbsp;</label>
                            <select class="form-select" name="province_destination" style="margin-bottom: 10px;">
                                <option value="">>---Provinsi--<</option>
                                @foreach ($provinces as $province => $value)
                                    <option value="{{$province}}">{{$value}}</option>
                                @endforeach
                                
                            </select>
                            <label class="form-label">Kota / Kabupaten</label>
                            <select class="form-select" name="city_destination" id="city_destination" style="margin-bottom: 10px;">
                                <option value="">>---Kota Tujuan--<</option>
                                
                            </select>
                            <label class="form-label">Kurir Pengiriman</label>
                            <select class="form-select" name="courier" style="margin-bottom: 10px;">
                                @foreach ($couriers as $courier => $value)
                                    <option value="{{$courier}}">{{$value}}</option>
                                @endforeach
                            </select><label class="form-label">Berat Total (gram) </label><input class="form-control"  style="margin-bottom: 10px;" id="totalberat" readonly name="totalberat" value="{{$totalberat}}">
                            <div class="row">
                                <div class="col d-flex justify-content-lg-end align-items-lg-center justify-content-xl-end align-items-xl-center justify-content-xxl-end align-items-xxl-center"><button class="btn btn-primary d-xxl-flex" type="submit" style="margin-left: 0px;border-radius: 17px;">Cek Ongkir</button></div>
                            </div>
                        </form>
                    </div>
                    <div class="col">
                        <form action="/distributor/checkout" method="POST">
                            @csrf
                            <label class="form-label">pilih jenis pengiriman</label>
                            <select class="form-select" name="layananpengirim">
                                @if ($isi == 0)
                                    <option value="0">>----Layanan Pengiriman----<</option>
                                @else
                                @php
                                    $id = 0;
                                @endphp
                                @foreach ($costongkir as $item)
                                <option value="{{$item['cost'][0]['value']}}">{{$item["service"]}} || Rp. {{number_format($item['cost'][0]['value'])}} || etd {{$item['cost'][0]['etd']}} hari</option>
                                    @php
                                       
                                    @endphp
                                @endforeach
                                @endif
                            </select>
                            <div class="row d-md-flex">
                                <div class="col-12 d-sm-flex d-md-flex justify-content-sm-start justify-content-md-end align-items-md-end" style="height: 285px;"><button class="btn btn-primary" type="submit" style="border-radius: 17px;">Checkout</button></div>
                            </div>
                            
                        </form>
                    </div>
                </div>
                <div class="modal fade" id="edit">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <form action="/distributor/editcart" method="post">
                                @csrf
                            <div class="modal-header">
                                <h4 class="modal-title">Edit Cart</h4><button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <div>
                                    <div class="row">
                                        <div class="col"><label class="col-form-label">ID Barang&nbsp;</label></div>
                                        <div class="col-xxl-8"><input type="text" style="width: 300px;border-radius: 15px;" name="id_produk" id="id_produk" readonly=""></div>
                                    </div>
                                    <div class="row">
                                        <div class="col"><label class="col-form-label">Nama Barang&nbsp;</label></div>
                                        <div class="col-xxl-8"><input type="text" style="width: 300px;border-radius: 15px;" name="nama_produk" id="nama_produk" readonly=""></div>
                                    </div>
                                    <div class="row">
                                        <div class="col"><label class="col-form-label">Jumlah/satuan &nbsp;</label></div>
                                        <div class="col-xxl-8"><input type="text" style="width: 300px;border-radius: 15px;" name="jumlah_satuan"  id='jumlah_satuan' inputmode="numeric"></div>
                                    </div>
                                    <div class="row">
                                        <div class="col"><label class="col-form-label">jumlah/dus&nbsp;</label></div>
                                        <div class="col-xxl-8"><input type="text" style="width: 300px;border-radius: 15px;" name="jumlah_dus"  id='jumlah_dus' inputmode="numeric"></div>
                                    </div>
                                    <div class="row">
                                        <div class="col"><label class="col-form-label">Total Jumlah&nbsp;</label></div>
                                        <div class="col-xxl-8"><input type="text" style="width: 300px;border-radius: 15px;" name="jumlah_total"  id='jumlah_total' inputmode="numeric"></div>
                                    </div>
    
                                    {{-- <div class="row">
                                        <div class="col"><img src="" id="showpic" style="width: 200px;height: 200px;"></div>
                                        <div class="col"></div>
                                    </div> --}}
                                </div>
                            </div>
                            <div class="modal-footer"><button class="btn btn-light" type="button" data-bs-dismiss="modal">Close</button><button class="btn btn-primary" type="submit">Submit</button></div>
                        </form>
                        </div>
                    </div>
            </div>
            
            </div>
            <footer class="bg-white sticky-footer">
                <div class="container my-auto">
                    <div class="text-center my-auto copyright"><span>Copyright © CV. Optimus Cahaya Abadi 2022</span></div>
                </div>
            </footer>
        </div><a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a>
    </div>

    <script>
        $(document).ready(function () {
            $('select').selectize({
                sortField: 'text'
            });
        });
    </script>

    <script>
        $('#edit').on('show.bs.modal', function(event) {
            var button = $(event.relatedTarget)
            var nama = button.data('mynama')
            var id = button.data('idproduk')
            var jumlah_beli = button.data('jumlah_satuan')
            var jumlah_dus = button.data('jumlah_lusin')
            var jumlah_total = button.data('jumlah_total')
            var jumlahperdus = button.data('jumlahperdus')
            var foto = "/produk/LED12WTRK.png"

            var total = (jumlah_dus*jumlahperdus) + jumlah_beli


            var modal =$(this)
            modal.find('.modal-body #nama_produk').val(nama);
            modal.find('.modal-body #jumlah_satuan').val(jumlah_beli);
            modal.find('.modal-body #jumlah_dus').val(jumlah_dus);
            modal.find('.modal-body #jumlah_total').val(total);
            modal.find('.modal-body #id_produk').val(id);
            // modal.find('.modal-body #showpic').val(foto);
            // $('.modal-body #showpic').attr('src') = foto;
        
            
        })
    </script>

<script>
    $(document).ready(function(){
        $('select[name= "courier"]').on('change',function(){
            let courierid = $(this).val();
            let cityid = $('#city_destination').val();
            var berat = $('#totalberat').val();   
            
            if(courierid){
                jQuery.ajax({
                    url: '/cekongkir/'+city+'/'+courierid+'/'+berat+'/submit',
                    type: "GET",
                    dataType:"json",
                    alert('masuk');
                    success:function(data){
                        alert('masuk');
                        // $('select[name="layananpengirim"]').empty();
                        // $.each(data,function(key, value){
                        //     $('select[name="layananpengirim"]').append('<option value"'+key+ '">'+ value + '</option>');
                        // });
                    },
                });
            }else{
                alert('else');
                $('select[name="layananpengirim"]').empty();    
            }
        });
    });
</script>
   
<script>
     $(document).ready(function(){
        $('select[name= "province_destination"]').on('change',function(){
            let provinceId = $(this).val();
            if(provinceId){
                jQuery.ajax({
                    url: '/province/'+provinceId+'/cities',
                    type: "GET",
                    dataType:"json",
                    success:function(data){
                        $('select[name="city_destination"]').empty();
                        $.each(data,function(key, value){
                            $('select[name="city_destination"]').append('<option value"'+key+ '">'+ value + '</option>');
                        });
                    },
                });
            }else{
                $('select[name="city_destination"]').empty();    
            }
        });
    });
</script>
    {{-- <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script> --}}
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="{{url('asset/js/bs-init.js')}}"></script>
    <script src="{{url('asset/js/theme.js')}}"></script>
    <script src="{{url('assets/bootstrap/js/bootstrap.min.css')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/bs-init.js?h=e2b0d57f2c4a9b0d13919304f87f79ae"></script>
    <script src="assets/js/theme.js?h=79f403485707cf2617c5bc5a2d386bb0"></script>
</body>
@endsection