@extends('template')
<head>
    <title>Checkout</title>
    <script type="text/javascript"
      src="https://app.sandbox.midtrans.com/snap/snap.js"
      data-client-key="SB-Mid-client-KLY9v7HM1kdT5UTS">
    </script>
</head>
@section('Content')
<body class="bg-gradient-primary">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-9 col-lg-12 col-xl-10" style="margin-top: 17px;">
                <div class="card shadow-lg o-hidden border-0 my-5">
                    <div class="card-body p-0">
                        <div class="p-5" style="margin-top: 25px;margin-bottom: 25px;">
                            <div class="text-center">
                                <h4 class="text-dark mb-4">Checkout Nota</h4>
                                <button id="btnTunai" class="btn btn-primary">Pembayaran Paylater</button>
                                <button id="btnQR" class="btn btn-primary">Pembayaran ewallet</button>
                                <div class="d-flex d-md-flex d-lg-flex d-xl-flex d-xxl-flex justify-content-center justify-content-md-center justify-content-lg-center justify-content-xl-center justify-content-xxl-center">
                                    <div id="kontenTunai" style="margin: 20px;">
                                        <form action="/distributor/pembayaranupload" method="post" enctype="multipart/form-data" class="border border-primary" style="width: 500px;border-width: 3px;border-style: solid;border-radius: 9px;min-width: 250px;padding: 20px;padding-top: 20px;padding-bottom: 20px;">
                                            @csrf
                                            <p style="margin-bottom: 0px;">Upload Bukti Pembayaran</p>
                                                <img class="img-preview img-fluid mb-3 col-sm-5 ">
                                                <input type="file" class="form-control" id="image" name="foto" onchange="previewImage()"/>
                                                <input type="hidden" name="totalpembayaran" value="{{$hargatotal}}">
                                                <input type="hidden" name="sudahdibayar" value="{{$hargadp}}"> 
                                            <p style="margin-bottom: 0px;margin-top: 20px;">Jumlah yang harus dibayarkan</p>
                                            <H3>Total pembayaran : Rp. <span id="jmlDibayar">{{number_format($hargatotal)}}<span></H3>
                                            <H5>Jumlah pembayaran : Rp. <span id="jmlDibayar">{{number_format($hargadp)}}<span></H5>
                                            <button class="btn btn-success" type="submit">Bayar</button>
                                        </form>
                                    </div>
                                    <div id="kontenQR" style="margin: 20px;">
                                        <div class="border border-primary" style="width: 500px;border-width: 3px;border-style: solid;border-radius: 9px;min-width: 250px;padding: 20px;padding-top: 20px;padding-bottom: 20px;">
                                            <p style="margin-bottom: 0px;margin-top: 20px;">Jumlah yang harus dibayarkan</p>
                                           
                                            <H3>Total pembayaran : Rp. <span id="jmlDibayar">{{number_format($hargatotal)}}<span></H3>
                                            <button id="pay-button" class="btn btn-success">Bayar</button>
                                            <form action="/distributor/success/midtrans" method="post" id="successpayment">
                                                @csrf
                                                <input type="hidden" name="midtranspayment">
                                            </form>
                                            {{-- <img src="{{url('asset/qrbayar.jpeg')}}" alt="" srcset="" width="400px"> --}}
                                        </div>
                                    </div>
                                </div>
                                <a href="/distributor/cart" class="btn btn-warning">Kembali Ke Cart</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
    // For example trigger on button clicked, or any time you need
    var payButton = document.getElementById('pay-button');
    payButton.addEventListener('click', function () {
        // Trigger snap popup. @TODO: Replace TRANSACTION_TOKEN_HERE with your transaction token
        window.snap.pay('{{$snap_token}}', {
        onSuccess: function(result){
            /* You may add your own implementation here */
            $('#successpayment').submit();
        //   alert("payment success!"); console.log(result);
        },
        onPending: function(result){
            /* You may add your own implementation here */
            alert("wating your payment!"); console.log(result);
        },
        onError: function(result){
            /* You may add your own implementation here */
            alert("payment failed!"); console.log(result);
        },
        onClose: function(){
            /* You may add your own implementation here */
            alert('you closed the popup without finishing the payment');
        }
        })
    });
    </script>
    <script type="text/javascript" >
        function previewImage(){
            const image = document.querySelector('#image');
            const imgPreview = document.querySelector('.img-preview');

            imgPreview.style.display = 'block';

            const oFReader = new FileReader();
            oFReader.readAsDataURL(image.files[0]);

            oFReader.onload = function(oFREvent){
                imgPreview.src = oFREvent.target.result;
            }

        } 
        $(document).ready(function(){
            $("#kontenQR").hide();

            $("#btnTunai").click(function(){
                if ($('#kontenTunai').is(':visible')) {  
                    $("#kontenTunai").hide();
                }else{
                    $("#kontenTunai").show();
                    $("#kontenQR").hide();
                }
            });
            $("#btnQR").click(function(){
                if ($('#kontenQR').is(':visible')) {  
                    $("#kontenQR").hide();
                }else{
                    $("#kontenQR").show();
                    $("#kontenTunai").hide()
                }
            });
        });
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="{{url('asset/js/bs-init.js')}}"></script>
    <script src="{{url('asset/js/theme.js')}}"></script>
    <script src="{{url('assets/bootstrap/js/bootstrap.min.css')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/bs-init.js?h=e2b0d57f2c4a9b0d13919304f87f79ae"></script>
    <script src="assets/js/theme.js?h=79f403485707cf2617c5bc5a2d386bb0"></script>
</body>
@endsection