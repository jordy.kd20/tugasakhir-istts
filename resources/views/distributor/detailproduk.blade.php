@extends('template')
<head>
    <title>Detail Produk</title>
    <style>
         .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
            }

            /* Modal Content (Image) */
            .modal-content {
            margin: auto;
            display: block;
            width: 80%;
            max-width: 300px;
            }

            /* Caption of Modal Image (Image Text) - Same Width as the Image */
            #caption {
            margin: auto;
            display: block;
            width: 80%;
            max-width: 300px;
            text-align: center;
            color: #ccc;
            padding: 10px 0;
            height: 150px;
            }

            /* Add Animation - Zoom in the Modal */
            .modal-content, #caption {
            animation-name: zoom;
            animation-duration: 0.6s;
            }

            @keyframes zoom {
            from {transform:scale(0)}
            to {transform:scale(1)}
            }

            /* The Close Button */
            .close {
            position: absolute;
            top: 15px;
            right: 35px;
            color: #f1f1f1;
            font-size: 40px;
            font-weight: bold;
            transition: 0.3s;
            }

            .close:hover,
            .close:focus {
            color: #bbb;
            text-decoration: none;
            cursor: pointer;
            }

            /* 100% Image Width on Smaller Screens */
            @media only screen and (max-height: 300px){
            .modal-content {
                width: 60%;
            }
            }
    </style>
</head>
@section('Content')
<body class="bg-gradient-primary">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-9 col-lg-12 col-xl-10" style="margin-top: 17px;">
                <div class="card shadow-lg o-hidden border-0 my-5">
                    <div class="card-body p-0">
                        <div class="p-5" style="margin-top: 25px;margin-bottom: 25px;">
                            <div class="row">
                                <div class="col d-xxl-flex justify-content-xxl-start align-items-xxl-center"><a class="btn btn-danger" href="/distributor/indexdistributor" role="button">Back</a>
                                    <div></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col d-xxl-flex justify-content-xxl-center align-items-xxl-center" style="padding-top: 20px;padding-bottom: 20px;"><img id="myImg" src={{url($foto_produk)}} style="height: 200px"></div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <h1 style="padding-bottom: 30px;">{{$nama_produk}}</h1>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <h5 style="padding-bottom: 10px;">Rp. {{number_format($harga_produk)}}</h5>
                                    <h6 style="padding-bottom: 10px;">Spesifikasi Produk</h6>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div>
                                        <div class="input-group" style="padding-bottom: 15px;"><span class="input-group-text">Merek</span><input class="form-control" type="text" value="{{$merek}}" readonly></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div>
                                        <div class="input-group" style="padding-bottom: 15px;"><span class="input-group-text">Stok Produk</span><input class="form-control" type="text" value="{{$stok_produk}}" readonly></div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div>
                                        <div class="input-group" style="padding-bottom: 15px;"><span class="input-group-text">Jumlah per Kerdus</span><input class="form-control" type="text" value="{{$jumlahperdus}}" readonly></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div>
                                        <div class="input-group" style="padding-bottom: 15px;"><span class="input-group-text">Daya</span><input class="form-control" type="text" value="{{$daya}}" readonly></div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div>
                                        <div class="input-group" style="padding-bottom: 15px;"><span class="input-group-text">Warna Lampu</span><input class="form-control" type="text" value="{{$warna_lampu}}" readonly></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="padding-top: 30px;">
                                <div class="col">
                                    <div class="d-xxl-flex justify-content-xxl-center align-items-xxl-center"><a class="btn btn-primary" href="/distributor/addtochart/{{$id_produk}}">Tambah Keranjang</a></div>
                                </div>
                            </div>
                            <div id="myModal" class="modal">

                                <!-- The Close Button -->
                                <span class="close">&times;</span>
                              
                                <!-- Modal Content (The Image) -->
                                <img class="modal-content" id="img01">
                              
                                <!-- Modal Caption (Image Text) -->
                                {{-- <div id="caption"></div> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        var modal = document.getElementById("myModal");

        // Get the image and insert it inside the modal - use its "alt" text as a caption
        var img = document.getElementById("myImg");
        var modalImg = document.getElementById("img01");
        // var captionText = document.getElementById("caption");
        img.onclick = function(){
        modal.style.display = "block";
        modalImg.src = this.src;
        captionText.innerHTML = this.alt;
        }

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
        modal.style.display = "none";
        }
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="{{url('asset/js/bs-init.js')}}"></script>
    <script src="{{url('asset/js/theme.js')}}"></script>
    <script src="{{url('assets/bootstrap/js/bootstrap.min.css')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/bs-init.js?h=e2b0d57f2c4a9b0d13919304f87f79ae"></script>
    <script src="assets/js/theme.js?h=79f403485707cf2617c5bc5a2d386bb0"></script>
</body>
@endsection