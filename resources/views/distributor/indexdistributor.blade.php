@extends('template')
@section('Title')
    Halaman Distributor
@endsection
@section('Content')

    <body id="page-top">
        <div id="wrapper">
            <nav class="navbar navbar-dark align-items-start sidebar sidebar-dark accordion bg-gradient-primary p-0"
                style="padding-right: 0px;background: #171717;">
                <div class="container-fluid d-flex flex-column p-0"><a
                        class="navbar-brand d-flex justify-content-center align-items-center sidebar-brand m-0"
                        href="#">
                        <div class="sidebar-brand-icon rotate-n-15" style="margin-right: -11px;"><img
                                src={{ url('asset/img/Logo.png') }}
                                style="width: 40px;height: 40px;transform: rotate(15deg);"></div>
                        <div class="sidebar-brand-text mx-3"><span style="font-size: 9px;">CV. OPTIMUS CAHAYA ABADI</span>
                        </div>
                    </a>
                    <ul class="navbar-nav text-light" id="accordionSidebar">
                        <li class="nav-item"><a class="nav-link active" href="/distributor/indexdistributor"><i class="far fa-money-bill-alt"></i><span>Transaksi</span></a></li>
                    <li class="nav-item"><a class="nav-link" href="/distributor/cart"><i class="fa fa-shopping-cart"></i><span>Keranjang</span></a></li>
                    <li class="nav-item"><a class="nav-link" href="/distributor/pembelian"><i class="fa fa-dollar"></i><span>Pembelian</span></a></li>
                    <li class="nav-item"><a class="nav-link " href="/distributor/listretur"><i class="fa fa-list"></i><span>List Retur</span></a></li>
                    <li class="nav-item"><a class="nav-link" href="/distributor/returproduk"><i class="fa fa-sort"></i><span>Retur Barang</span></a></li>
                    </ul>
                    <hr class="sidebar-divider my-0">
                    <div class="text-center d-none d-md-inline"><button class="btn rounded-circle border-0"
                            id="sidebarToggle" type="button"></button></div>
                </div>
            </nav>
            <div class="d-flex flex-column" id="content-wrapper">
                <div id="content">
                    <nav class="navbar navbar-light navbar-expand bg-white shadow mb-4 topbar static-top">
                        <div class="container-fluid"><button class="btn btn-link d-md-none rounded-circle me-3"
                                id="sidebarToggleTop" type="button"><i class="fas fa-bars"></i></button>
                            <ul class="navbar-nav flex-nowrap ms-auto">
                                <li class="nav-item dropdown d-sm-none no-arrow"><a class="dropdown-toggle nav-link"
                                        aria-expanded="false" data-bs-toggle="dropdown" href="#"><i
                                            class="fas fa-search"></i></a>
                                    <div class="dropdown-menu dropdown-menu-end p-3 animated--grow-in"
                                        aria-labelledby="searchDropdown">
                                        <form class="me-auto navbar-search w-100">
                                            <div class="input-group"><input class="bg-light form-control border-0 small"
                                                    type="text" placeholder="Search for ...">
                                                <div class="input-group-append"><button class="btn btn-primary py-0"
                                                        type="button"><i class="fas fa-search"></i></button></div>
                                            </div>
                                        </form>
                                    </div>
                                </li>
                                <li class="nav-item dropdown no-arrow">
                                    <div class="nav-item dropdown no-arrow"><a class="dropdown-toggle nav-link"
                                            aria-expanded="false" data-bs-toggle="dropdown" href="#"><span
                                                class="d-none d-lg-inline me-2 text-gray-600 small">{{ Session::get('distributoractive') }}</span><img
                                                class="border rounded-circle img-profile" src={{ url($foto) }}></a>
                                        <div class="dropdown-menu shadow dropdown-menu-end animated--grow-in"><a
                                                class="dropdown-item" href="/distributor/profile"><i
                                                    class="fas fa-user fa-sm fa-fw me-2 text-gray-400"></i>&nbsp;Edit
                                                Profile</a><a class="dropdown-item" href="/distributor/logout"><i
                                                    class="fas fa-sign-out-alt fa-sm fa-fw me-2 text-gray-400"></i>&nbsp;Logout</a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </nav>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col d-inline-flex">
                                <form action="/distributor/sortkategori" id="sortkategori" method="post">
                                    @csrf
                                    <div class="input-group"><span class="input-group-text">Kategori</span>
                                        <select class="form-select" name="kategori">
                                            <option value="0">>>Kategori<<</option>
                                            @foreach ($kategori as $item)
                                                <option value="{{ $item->nama_kategori }}">{{ $item->nama_kategori }}
                                                </option>
                                            @endforeach
                                            <option value="reset">Reset Filter</option>
                                        </select>
                                        {{-- <button class="btn btn-primary" type="submit">Button</button> --}}
                                    </div>
                                </form>
                            </div>
                            <div class="col d-inline-flex">
                                <form action="/distributor/searchproduk" method="post">
                                    @csrf
                                    <div class="input-group"><span class="input-group-text">Search</span><input class="form-control" type="search"name="searchproduk"><button class="btn btn-primary" type="submit">Cari</button></div>
                                </form>  
                            </div>
                            
                        </div>
                        {{-- <div class="row">
                        <form action="" method="post">
                            <div class="col d-inline-flex"><select>
                                <optgroup label="This is a group">
                                    <option value="12" selected="">This is item 1</option>
                                    <option value="13">This is item 2</option>
                                    <option value="14">This is item 3</option>
                                </optgroup>
                            </select><button class="btn btn-primary" type="button">Cari</button></div>
                        </form>
                        <form action="/distributor/searchproduk" method="post">
                            @csrf
                            <div class="col d-inline-flex"><input type="search" name="searchproduk" aria-controls="dataTable" placeholder="Search" class="form-control form-control-sm" style="margin-right: 20px;"><button class="btn btn-primary" type="submit">Cari</button></div>
                        </form>
                    </div> --}}
                        <div class="row row-cols-sm-2 row-cols-md-3 row-cols-xxl-4 justify-content-start align-items-start m-auto row-cols-1"
                            style="padding-top: 10px;padding-bottom: 10px;">
                            @if ($isi == 1)
                                @foreach ($dataproduk as $item)
                                    <div class="col" style="padding-top: 10px;padding-bottom: 10px;">
                                        <div class="card">
                                            <a href="/distributor/detailproduk/{{ $item->id_produk }}">
                                                <img src="{{ url($item->foto_produk) }}"
                                                    class="card-img-top w-100 d-block"
                                                    style="height: 200px; object-fit: contain">
                                            </a>
                                            {{-- <img src="{{url($item->foto_produk)}}" class="card-img-top w-100 d-block" style="height: 200px; object-fit: contain"> --}}
                                            <div class="card-body">
                                                <h4 class="card-title">{{ $item->nama_produk }}</h4>
                                                <h5>Jumlah Stok Produk: {{ $item->stok_produk }}/pcs</h5>
                                                <p class="card-text">satu box {{ $item->kategori_produk }} berisi
                                                    {{ $item->jumlahperdus }} biji</p>
                                                <div class="col offset-xxl-0">
                                                    <div class="row">
                                                        <div class="col">
                                                            @php
                                                                $harga = 0;
                                                                $harga = $item->harga_produk * $item->jumlahperdus; 
                                                            @endphp
                                                            <h5>Rp. {{number_format($harga)}}/dus</h5>
                                                        </div>
                                                        <div class="col d-xxl-flex justify-content-xxl-end"><a class="btn btn-primary" href="/distributor/addtochart/{{ $item->id_produk }}">Tambahkan</a></div>
                                                    </div>
                                                </div>
                                                {{-- <div
                                                    class="col d-flex d-sm-flex d-md-flex d-lg-flex d-xl-flex justify-content-end justify-content-sm-end justify-content-md-end justify-content-lg-end justify-content-xl-end">
                                                    <a href="/distributor/addtochart/{{ $item->id_produk }}"
                                                        class="btn btn-primary d-lg-flex justify-content-lg-end align-items-lg-end">Tambahkan</a>
                                                </div> --}}
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
                <footer class="bg-white sticky-footer">
                    <div class="container my-auto">
                        <div class="text-center my-auto copyright"><span>Copyright © Elektronik Home 2021</span></div>
                    </div>
                </footer>
            </div><a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a>
        </div>
        <script>
            $(document).ready(function(){
                $('select[name= "kategori"]').on('change',function(){
                    $('#sortkategori').submit();
                });
            });
        </script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
        <script src="{{ url('asset/js/bs-init.js') }}"></script>
        <script src="{{ url('asset/js/theme.js') }}"></script>
        <script src="{{ url('assets/bootstrap/js/bootstrap.min.css') }}"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
    </body>
@endsection
