@extends('template')
<head>
    <title>
        Pembelian Distributor
    </title>
</head>
@section('Content')
<body id="page-top">
    <div id="wrapper">
        <nav class="navbar navbar-dark align-items-start sidebar sidebar-dark accordion bg-gradient-primary p-0" style="padding-right: 0px;background: #171717;">
            <div class="container-fluid d-flex flex-column p-0"><a class="navbar-brand d-flex justify-content-center align-items-center sidebar-brand m-0" href="#">
                    <div class="sidebar-brand-icon rotate-n-15" style="margin-right: -11px;"><img src={{url('asset/img/Logo.png')}} style="width: 40px;height: 40px;transform: rotate(15deg);"></div>
                    <div class="sidebar-brand-text mx-3"><span style="font-size: 9px;">CV. OPTIMUS CAHAYA ABADI</span></div>
                </a>
                <ul class="navbar-nav text-light" id="accordionSidebar">
                    <li class="nav-item"><a class="nav-link " href="/distributor/indexdistributor"><i class="far fa-money-bill-alt"></i><span>Transaksi</span></a></li>
                    <li class="nav-item"><a class="nav-link" href="/distributor/cart"><i class="fa fa-shopping-cart"></i><span>Keranjang</span></a></li>
                    <li class="nav-item"><a class="nav-link active" href="/distributor/pembelian"><i class="fa fa-dollar"></i><span>Pembelian</span></a></li>
                    <li class="nav-item"><a class="nav-link " href="/distributor/listretur"><i class="fa fa-list"></i><span>List Retur</span></a></li>
                    <li class="nav-item"><a class="nav-link" href="/distributor/returproduk"><i class="fa fa-sort"></i><span>Retur Barang</span></a></li>
                </ul>
                <hr class="sidebar-divider my-0">
                <div class="text-center d-none d-md-inline"><button class="btn rounded-circle border-0" id="sidebarToggle" type="button"></button></div>
            </div>
        </nav>
        <div class="d-flex flex-column" id="content-wrapper">
            <div id="content">
                <nav class="navbar navbar-light navbar-expand bg-white shadow mb-4 topbar static-top">
                    <div class="container-fluid"><button class="btn btn-link d-md-none rounded-circle me-3" id="sidebarToggleTop" type="button"><i class="fas fa-bars"></i></button>
                        <ul class="navbar-nav flex-nowrap ms-auto">
                            <li class="nav-item dropdown d-sm-none no-arrow"><a class="dropdown-toggle nav-link" aria-expanded="false" data-bs-toggle="dropdown" href="#"><i class="fas fa-search"></i></a>
                                <div class="dropdown-menu dropdown-menu-end p-3 animated--grow-in" aria-labelledby="searchDropdown">
                                    <form class="me-auto navbar-search w-100">
                                        <div class="input-group"><input class="bg-light form-control border-0 small" type="text" placeholder="Search for ...">
                                            <div class="input-group-append"><button class="btn btn-primary py-0" type="button"><i class="fas fa-search"></i></button></div>
                                        </div>
                                    </form>
                                </div>
                            </li>
                            <li class="nav-item dropdown no-arrow">
                                <div class="nav-item dropdown no-arrow"><a class="dropdown-toggle nav-link" aria-expanded="false" data-bs-toggle="dropdown" href="#"><span class="d-none d-lg-inline me-2 text-gray-600 small">{{Session::get('distributoractive')}}</span><img class="border rounded-circle img-profile" src={{url(Session::get('distributorfoto'))}}></a>
                                    <div class="dropdown-menu shadow dropdown-menu-end animated--grow-in"><a class="dropdown-item" href="/distributor/profile"><i class="fas fa-user fa-sm fa-fw me-2 text-gray-400"></i>&nbsp;Edit Profile</a><a class="dropdown-item" href="/distributor/logout"><i class="fas fa-sign-out-alt fa-sm fa-fw me-2 text-gray-400"></i>&nbsp;Logout</a></div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
                <div class="container-fluid">
                    <h3 class="text-dark mb-4">Pembelian</h3>
                    <div class="card shadow">
                        <div class="card-header py-3">
                            <div class="row">
                                <div class="col">
                                    <div>
                                        <ul class="nav nav-tabs" role="tablist" style="border-color: rgb(255,255,255);border-top-color: rgb(133,;border-right-color: 135,;border-bottom-color: 150);border-left-color: 135,;">
                                            <li class="nav-item" role="presentation"><a class="nav-link active" role="tab" data-bs-toggle="tab" href="#tab-1" style="color: rgb(0,0,0);">Dikemas</a></li>
                                            <li class="nav-item" role="presentation"><a class="nav-link" role="tab" data-bs-toggle="tab" href="#tab-2" style="color: rgb(0,0,0);">Dikirim</a></li>
                                            <li class="nav-item" role="presentation"><a class="nav-link" role="tab" data-bs-toggle="tab" href="#tab-3" style="border-color: #ffffff;color: rgb(0,0,0);">Selesai</a></li>
                                        </ul>
                                        <div class="tab-content">
                                            {{-- Dikemas --}}
                                            <div class="tab-pane " role="tabpanel" id="tab-1">
                                                <div>
                                                    <div class="row row-cols-sm-2 row-cols-md-3 row-cols-xxl-4 justify-content-start align-items-start m-auto row-cols-1" style="padding-top: 10px;padding-bottom: 10px;">
                                                        @if ($countpembelian!=0)
                                                            @if ($countproses!=0)
                                                                @foreach ($hproses as $item)
                                                                <div class="col" style="padding-top: 10px;padding-bottom: 10px;width: 320px;">
                                                                    <div class="card" style="width: 300px;">
                                                                        <div class="card-header">
                                                                            <h4 style="text-align: center;font-size: 19.376px;">{{$item->id_htransaksi}}</h4>
                                                                        </div>
                                                                        <div class="card-body" style="padding-bottom: 0px;">
                                                                            <div class="row">
                                                                                <div class="col"><span style="font-weight: bold;font-size: 14px;">Tanggal Transaksi:&nbsp;</span></div>
                                                                                <div class="col"><span style="font-weight: bold;font-size: 12px;">{{Date('d F Y', strtotime($item->tgl_transaksi))}}&nbsp;</span></div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col"><span style="font-weight: bold;font-size: 14px;">Tanggal Jatuh Tempo :&nbsp;</span></div>
                                                                                <div class="col"><span style="font-weight: bold;font-size: 12px;">{{Date('d F Y', strtotime($item->tgl_jatuh_tempo))}}&nbsp;</span></div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col"><span style="font-weight: bold;font-size: 14px;">Grand Total :&nbsp;</span></div>
                                                                                <div class="col"><span style="font-weight: bold;font-size: 12px;">Rp. {{number_format($item->grand_total)}}&nbsp;</span></div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col"><span style="font-weight: bold;font-size: 14px;">Kurang Bayar :&nbsp;</span></div>
                                                                                <div class="col"><span style="font-weight: bold;font-size: 12px;">Rp. {{number_format($item->kurang_bayar)}}&nbsp;</span></div>
                                                                            </div>
                                                                            <div class="d-lg-flex justify-content-lg-center align-items-lg-center" style="padding-top: 10px;">
                                                                                @if ($item->status_bayar == 1)                                                                                   
                                                                                    <div class="alert alert-success d-sm-flex d-lg-flex justify-content-sm-center align-items-sm-center justify-content-lg-center align-items-lg-center" role="alert" style="width: 250px;height: 25px;margin-top: 30px;"><span><strong>Pembayaran Lunas</strong></span></div>
                                                                                @else
                                                                                    <div class="alert alert-danger d-sm-flex d-lg-flex justify-content-sm-center align-items-sm-center justify-content-lg-center align-items-lg-center" role="alert" style="width: 250px;height: 25px;margin-top: 30px;"><span><strong>Pembayaran Belum Lunas</strong></span></div>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                        <div class="card-footer">
                                                                            <div class="d-sm-flex d-lg-flex justify-content-sm-center align-items-sm-center justify-content-lg-center align-items-lg-center"><a class="btn btn-primary" href="/detailpesanan/{{$item->id_htransaksi}}">Detail Pembelian</a></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                @endforeach
                                                            @else
                                                            <div class="col" style="padding-top: 10px;padding-bottom: 10px;width: 320px;">
                                                                <div class="card">
                                                                    <div class="card-body">
                                                                    Tidak Ada Barang Diproses
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            @endif
                                                        @else
                                                        <div class="col" style="padding-top: 10px;padding-bottom: 10px;width: 320px;">
                                                            <div class="card">
                                                                <div class="card-body">
                                                                Tidak Ada Barang Diproses
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" role="tabpanel" id="tab-2">
                                                {{-- Dikirim --}}
                                                <div>
                                                    <div class="row row-cols-sm-2 row-cols-md-3 row-cols-xxl-4 justify-content-start align-items-start m-auto row-cols-1" style="padding-top: 10px;padding-bottom: 10px;">
                                                        @if ($countpembelian!=0)
                                                            @if ($countdikirim!=0)
                                                                @foreach ($hdikirim as $item)
                                                                <div class="col" style="padding-top: 10px;padding-bottom: 10px;width: 320px;">
                                                                    <div class="card" style="width: 300px;">
                                                                        <div class="card-header">
                                                                            <h4 style="text-align: center;font-size: 19.376px;">{{$item->id_htransaksi}}</h4>
                                                                        </div>
                                                                        <div class="card-body" style="padding-bottom: 0px;">
                                                                            <div class="row">
                                                                                <div class="col"><span style="font-weight: bold;font-size: 14px;">Tanggal Transaksi:&nbsp;</span></div>
                                                                                <div class="col"><span style="font-weight: bold;font-size: 12px;">{{Date('d F Y', strtotime($item->tgl_transaksi))}}&nbsp;</span></div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col"><span style="font-weight: bold;font-size: 14px;">Tanggal Jatuh Tempo :&nbsp;</span></div>
                                                                                <div class="col"><span style="font-weight: bold;font-size: 12px;">{{Date('d F Y', strtotime($item->tgl_jatuh_tempo))}}&nbsp;</span></div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col"><span style="font-weight: bold;font-size: 14px;">Grand Total :&nbsp;</span></div>
                                                                                <div class="col"><span style="font-weight: bold;font-size: 12px;">Rp. {{number_format($item->grand_total)}}&nbsp;</span></div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col"><span style="font-weight: bold;font-size: 14px;">Kurang Bayar :&nbsp;</span></div>
                                                                                <div class="col"><span style="font-weight: bold;font-size: 12px;">Rp. {{number_format($item->kurang_bayar)}}&nbsp;</span></div>
                                                                            </div>
                                                                            <div class="d-lg-flex justify-content-lg-center align-items-lg-center" style="padding-top: 10px;">
                                                                                @if ($item->status_bayar == 1)                                                                                   
                                                                                    <div class="alert alert-success d-sm-flex d-lg-flex justify-content-sm-center align-items-sm-center justify-content-lg-center align-items-lg-center" role="alert" style="width: 250px;height: 25px;margin-top: 30px;"><span><strong>Pembayaran Lunas</strong></span></div>
                                                                                @else
                                                                                    <div class="alert alert-danger d-sm-flex d-lg-flex justify-content-sm-center align-items-sm-center justify-content-lg-center align-items-lg-center" role="alert" style="width: 250px;height: 25px;margin-top: 30px;"><span><strong>Pembayaran Belum Lunas</strong></span></div>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                        <div class="card-footer">
                                                                            <div class="d-sm-flex d-lg-flex justify-content-sm-center align-items-sm-center justify-content-lg-center align-items-lg-center"><a class="btn btn-primary" href="/detailpesanan/{{$item->id_htransaksi}}">Detail Pembelian</a></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                @endforeach
                                                            @else
                                                            <div class="col" style="padding-top: 10px;padding-bottom: 10px;width: 320px;">
                                                                <div class="card">
                                                                    <div class="card-body">
                                                                        Tidak Ada Barang Dikirim
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            @endif
                                                        @else
                                                        <div class="col" style="padding-top: 10px;padding-bottom: 10px;width: 320px;">
                                                            <div class="card">
                                                                <div class="card-body">
                                                                    Tidak Ada Barang Dikirim
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane active" role="tabpanel" id="tab-3">
                                                {{-- Selesai --}}
                                                <div>
                                                    <div class="row p-3">
                                                        <form action="/distributor/filtertgltransaksi" method="post">
                                                            @csrf
                                                            <div class="input-group">
                                                                <span class="input-group-text">Dari Tanggal</span><input type="date"
                                                                    name="tgldari" id="">&nbsp;
                                                                    - &nbsp;
                                                                <span class="input-group-text">Sampai Tanggal</span><input type="date"
                                                                    name="tglsampai" id="">
                                                                &nbsp; <button type="submit" class="btn btn-primary">Pilih</button>
                                                                &nbsp; <a href="/distributor/pembelian" class="btn btn-warning">Reset filter</a>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <div class="row row-cols-sm-2 row-cols-md-3 row-cols-xxl-4 justify-content-start align-items-start m-auto row-cols-1" style="padding-top: 10px;padding-bottom: 10px;">
                                                        <div class="card-body">
                                                            <div class="row">
                                                                <table class="table table-stripped center" id="myTable" style="text-align: center"> 
                                                                    <thead>
                                                                        <tr>
                                                                            <th style="padding-right: 40px;padding-left: 40px;">ID Pesanan</th>
                                                                            <th>Tanggal Transaksi</th>
                                                                            <th>Tanggal Jatuh Tempo</th>
                                                                            <th>Grand Total</th>
                                                                            <th>Kurang Bayar</th>
                                                                            <th>Status Pembayaran</th>
                                                                            <th>Status</th>
                                                                            <th>Detail Produk</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        @if ($countpembelian!=0)
                                                                            @if ($countselesai!=0)
                                                                               @foreach ($hselesai as $item)
                                                                                   <tr>
                                                                                        <td>{{$item->id_htransaksi}}</td>
                                                                                        <td>{{Date('d F Y', strtotime($item->tgl_transaksi))}}</td>
                                                                                        <td>{{Date('d F Y', strtotime($item->tgl_jatuh_tempo))}}</td>
                                                                                        <td>Rp. {{number_format($item->grand_total)}}</td>
                                                                                        <td>Rp. {{number_format($item->kurang_bayar)}}</td>
                                                                                        <td>
                                                                                            @if ($item->status_bayar==1)
                                                                                                Pemabayaran Lunas
                                                                                            @else
                                                                                                Pembayaran Belum Lunas
                                                                                            @endif
                                                                                        </td>
                                                                                        <td>
                                                                                            @if ($item->status_pengiriman == 2)
                                                                                                <div class="alert alert-success" role="alert">
                                                                                                    <span><strong>Selesai</strong></span>
                                                                                                </div>
                                                                                            @elseif($item->status_pengiriman==3)
                                                                                            <div class="alert alert-danger" role="alert">
                                                                                                <span><strong>Pesanan Ditolak</strong></span>
                                                                                            </div>
                                                                                            @endif
                                                                                        </td>
                                                                                        <td><a class="btn btn-primary" href="/detailpesanan/{{$item->id_htransaksi}}">Detail Pembelian</a></td>
                                                                                   </tr>
                                                                               @endforeach
                                                                            @endif
                                                                        @endif
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="margin-left: 14px;"></div>
            <footer class="bg-white sticky-footer">
                <div class="container my-auto">
                    <div class="text-center my-auto copyright"><span>Copyright © CV. Optimus Cahaya Abadi 2022</span></div>
                </div>
            </footer>
        </div><a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a>
    </div>
    <script>
        $(document).ready(function () {
            $('#myTable').DataTable({
                // scrollX: true,
            });
        });
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="{{url('asset/js/bs-init.js')}}"></script>
    <script src="{{url('asset/js/theme.js')}}"></script>
    <script src="{{url('assets/bootstrap/js/bootstrap.min.css')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
</body>
@endsection
