@extends('template')
<head>
    <title>Register Distributor</title>
</head>
@section('Content')
<body class="bg-gradient-primary">
    <div class="container">
        <div class="card shadow-lg o-hidden border-0 my-5">
            <div class="card-body p-0">
                <div class="row">
                    <div class="col-lg-5 d-none d-lg-flex">
                        <div class="flex-grow-1 bg-register-image" style="background: url(&quot;../assets/img/dogs/H597166e2ea234072af963f6ce11a7a0f8.png&quot;) center / contain no-repeat;"><img src="{{url('asset/img/lampu5w.png')}}" alt="" srcset=""></div>
                    </div>
                    <div class="col-lg-7">
                        <div class="p-5">
                            <div class="text-center">
                                <h4 class="text-dark mb-4">Regristrasi Distributor Baru</h4>
                            </div>
                            <form class="user" action="/distributor/registerdistributor" method="POST">
                                @csrf
                                <div class="row mb-3">
                                    <div class="col-sm-6 mb-3 mb-sm-0"><input class="form-control form-control-user" type="text" id="exampleFirstName" placeholder="Nama " name="nama_distributor" required></div>
                                    <div class="col-sm-6"><input class="form-control form-control-user" type="text" id="exampleFirstName" placeholder="Nama Toko" name="nama_toko" required></div>
                                </div>
                                <div class="mb-3"><input class="form-control form-control-user" type="email" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Email" name="email_distributor" style="margin-bottom: 15px;" required>
                                    <input class="form-control form-control-user" type="text" aria-describedby="emailHelp" placeholder="Alamat" name="alamat_distributor" style="margin-bottom: 15px;">
                                    @error('alamat_distributor')
                                        <span class="invalid-input-mess" style="color: red">*{{$message}}</span>
                                        <br>
                                    @enderror
                                    <input class="form-control form-control-user" type="text" id="exampleInputEmail-2" aria-describedby="emailHelp" placeholder="Nomor Telephone Whatsapp" name="Nomor_distributor" style="margin-bottom: 0px;" inputmode="numeric" onkeypress="return event.charCode >= 48 && event.charCode <=57"></div>
                                    @error('Nomor_distributor')
                                        <span class="invalid-input-mess" style="color: red">*{{$message}}</span>
                                        <br>
                                    @enderror
                                    
                                <div class="row mb-3">
                                    <div class="col-sm-6 mb-3 mb-sm-0"><input class="form-control form-control-user" type="password" id="examplePasswordInput" placeholder="Password" name="password" required></div>
                                    <div class="col-sm-6"><input class="form-control form-control-user" type="password" id="exampleRepeatPasswordInput" placeholder="Repeat Password" name="password_repeat" required></div>
                                </div>
                                @error('password_repeat')
                                        <span class="invalid-input-mess" style="color: red">*{{$message}}</span>
                                        <br>
                                    @enderror
                                <button class="btn btn-primary d-block btn-user w-100" type="submit">Register Account</button>
                                <hr><a class="btn btn-danger d-block btn-google btn-user w-100 mb-2" role="button"><i class="fab fa-google"></i>&nbsp; Register with Google</a>
                                <hr>
                            </form>
                            <div class="text-center"></div>
                            <div class="text-center"><a class="small" href="/">Already have an account? Login!</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="../assets/js/bs-init.js"></script>
    <script src="../assets/js/theme.js"></script>
</body>
@endsection
