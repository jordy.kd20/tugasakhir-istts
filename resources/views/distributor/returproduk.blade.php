@extends('template')
<head>
    <title>Retur Produk</title>
</head>
@section('Content')
<body id="page-top">
    <div id="wrapper">
        <nav class="navbar navbar-dark align-items-start sidebar sidebar-dark accordion bg-gradient-primary p-0" style="padding-right: 0px;background: #171717;">
            <div class="container-fluid d-flex flex-column p-0"><a class="navbar-brand d-flex justify-content-center align-items-center sidebar-brand m-0" href="#">
                    <div class="sidebar-brand-icon rotate-n-15" style="margin-right: -11px;"><img src={{url('asset/img/Logo.png')}} style="width: 40px;height: 40px;transform: rotate(15deg);"></div>
                    <div class="sidebar-brand-text mx-3"><span style="font-size: 9px;">CV. OPTIMUS CAHAYA ABADI</span></div>
                </a>
                <ul class="navbar-nav text-light" id="accordionSidebar">
                    <li class="nav-item"><a class="nav-link " href="/distributor/indexdistributor"><i class="far fa-money-bill-alt"></i><span>Transaksi</span></a></li>
                    <li class="nav-item"><a class="nav-link" href="/distributor/cart"><i class="fa fa-shopping-cart"></i><span>Keranjang</span></a></li>
                    <li class="nav-item"><a class="nav-link" href="/distributor/pembelian"><i class="fa fa-dollar"></i><span>Pembelian</span></a></li>
                    <li class="nav-item"><a class="nav-link " href="/distributor/listretur"><i class="fa fa-list"></i><span>List Retur</span></a></li>
                    <li class="nav-item"><a class="nav-link active" href="/distributor/returproduk"><i class="fa fa-sort"></i><span>Retur Barang</span></a></li>
                </ul>
                <hr class="sidebar-divider my-0">
                <div class="text-center d-none d-md-inline"><button class="btn rounded-circle border-0" id="sidebarToggle" type="button"></button></div>
            </div>
        </nav>
        <div class="d-flex flex-column" id="content-wrapper">
            <div id="content">
                <nav class="navbar navbar-light navbar-expand bg-white shadow mb-4 topbar static-top">
                    <div class="container-fluid"><button class="btn btn-link d-md-none rounded-circle me-3" id="sidebarToggleTop" type="button"><i class="fas fa-bars"></i></button>
                        <ul class="navbar-nav flex-nowrap ms-auto">
                            <li class="nav-item dropdown d-sm-none no-arrow"><a class="dropdown-toggle nav-link" aria-expanded="false" data-bs-toggle="dropdown" href="#"><i class="fas fa-search"></i></a>
                                <div class="dropdown-menu dropdown-menu-end p-3 animated--grow-in" aria-labelledby="searchDropdown">
                                    <form class="me-auto navbar-search w-100">
                                        <div class="input-group"><input class="bg-light form-control border-0 small" type="text" placeholder="Search for ...">
                                            <div class="input-group-append"><button class="btn btn-primary py-0" type="button"><i class="fas fa-search"></i></button></div>
                                        </div>
                                    </form>
                                </div>
                            </li>
                            <li class="nav-item dropdown no-arrow">
                                <div class="nav-item dropdown no-arrow"><a class="dropdown-toggle nav-link" aria-expanded="false" data-bs-toggle="dropdown" href="#"><span class="d-none d-lg-inline me-2 text-gray-600 small">{{Session::get('distributoractive')}}</span><img class="border rounded-circle img-profile" src="{{url(Session::get('distributorfoto'))}}"></a>
                                    <div class="dropdown-menu shadow dropdown-menu-end animated--grow-in"><a class="dropdown-item" href="/distributor/profile"><i class="fas fa-user fa-sm fa-fw me-2 text-gray-400"></i>&nbsp;Edit Profile</a><a class="dropdown-item" href="/distributor/logout"><i class="fas fa-sign-out-alt fa-sm fa-fw me-2 text-gray-400"></i>&nbsp;Logout</a></div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
                <div class="container-fluid">
                    <h3 class="text-dark mb-4">Retur Barang</h3>
                    <div class="card shadow">
                        <div class="card-header py-3">
                            <div class="row">
                                <div class="col">
                                    <form action="/distributor/inputretur" method="post">
                                        @csrf
                                        <div class="input-group">
                                            <span class="input-group-text">Input Nota Pembelian</span>
                                            <input class="form-control" name="nota" type="text"><button class="btn btn-primary" type="submit">Submit</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-fluid" style="padding-top: 10px;">
                    <h5 class="text-dark mb-4">Pilih Barang yang akan diretur</h5>
                    <div class="card shadow">
                        <div class="card-header py-3">
                            <form action="/distributor/addretur" method="post">
                                @csrf
                                <div class="row">
                                    <div class="col">
                                        <div class="input-group"><span class="input-group-text">Pilih Produk</span><select class="form-select" name="idprodukretur">
                                            @if ($inputnota!=0)
                                                @foreach ($datapesanan as $item)
                                                    <option value="{{$item->id_produk}}" >{{DB::table('produk')->where('id_produk',$item->id_produk)->value('nama_produk')}} - {{$item->totalqty}}</option>       
                                                @endforeach
                                            @endif 
                                            </select></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="input-group"><span class="input-group-text">Jumlah Produk Retur</span><input class="form-control" type="number" name="jumlah_retur"></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col d-flex d-sm-flex d-xl-flex justify-content-end align-items-center justify-content-sm-end align-items-sm-center justify-content-xl-end align-items-xl-center"><button class="btn btn-primary d-flex justify-content-end align-items-center" type="submit">Add Retur</button></div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="container-fluid" style="padding-top: 10px;">
                    <h5 class="text-dark mb-4"></h5>
                    <div class="card shadow">
                        <div class="card-header py-3">
                            <form action="/distributor/submitretur" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                   
                                    <div class="p-3">
                                        <table class="table table-striped center" id="myTable"  style="text-align: center"> 
                                            <thead>
                                                <tr>
                                                    <th>Nama Produk</th>
                                                    <th>Jumlah Retur</th>                                   
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if (Session::has('returbarang'))
                                                    @foreach (Session::get('returbarang') as $key => $item)
                                                        @if ($key == Session::get('distributoractive'))
                                                            @foreach ($item as $dataretur)
                                                                <tr>
                                                                    <td>{{DB::table('produk')->where('id_produk',$dataretur['id_produk'])->value('nama_produk')}}</td>
                                                                    <td>{{$dataretur['qtyretur']}}</td>
                                                                    <td><a class="btn btn-danger" href="/distributor/hapusaddretur/{{$dataretur['id_produk']}}"><i class="fa fa-trash"></i></a></td>
                                                                </tr>
                                                            @endforeach
                                                        @endif
                                                    @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                    
                                </div>
                                <div class="row">
                                    <div class="col d-xl-flex justify-content-xl-end align-items-xl-center">
                                        <img class="img-preview img-fluid mb-3 col-sm-5 ">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col d-xl-flex justify-content-xl-end align-items-xl-center">
                                        <div class="input-group"><input class="form-control" type="file" id="image" onchange="previewImage()" name="imageretur"></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col d-flex d-sm-flex d-xl-flex justify-content-end align-items-center justify-content-sm-end align-items-sm-center justify-content-xl-end align-items-xl-center"><button class="btn btn-primary d-flex justify-content-end align-items-center" type="submit">Submit Retur</button></div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div style="margin-left: 14px;"></div>
            <footer class="bg-white sticky-footer">
                <div class="container my-auto">
                    <div class="text-center my-auto copyright"><span>Copyright © CV. Optimus Cahaya Abadi 2022</span></div>
                </div>
            </footer>
        </div><a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a>
    </div>
    <script>
        $(document).ready(function () {
            $('#myTable').DataTable({
                // scrollX: true,
            });
        }); 
    </script>
    <script>
        function previewImage(){
            const image = document.querySelector('#image');
            const imgPreview = document.querySelector('.img-preview');

            imgPreview.style.display = 'block';

            const oFReader = new FileReader();
            oFReader.readAsDataURL(image.files[0]);

            oFReader.onload = function(oFREvent){
                imgPreview.src = oFREvent.target.result;
            }

        } 
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="{{url('asset/js/bs-init.js')}}"></script>
    <script src="{{url('asset/js/theme.js')}}"></script>
    <script src="{{url('assets/bootstrap/js/bootstrap.min.css')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
</body>
@endsection