@extends('template')
<head>
    <title>
        Profile Info
    </title>
</head>
@section('Content')
<body style="background: rgb(78,115,223);">
    <div class="container">
        <div class="card shadow-lg o-hidden border-0 my-5">
            <div class="card-body p-0">
                <div class="row">
                    <div class="col-lg-5 d-none d-lg-flex">
                        <img src={{url('asset/img/lampu5w.png')}} alt="" srcset="" style="center / contain no-repeat;">
                        {{-- <div class="flex-grow-1 bg-register-image" style="background: {{url('asset/img/lampu5w.png')}} center / contain no-repeat;"></div> --}}
                    </div>
                    <div class="col-lg-7">
                        <div class="p-5">
                            <div class="text-center">
                                <h4 class="text-dark mb-4">Update Profile Distributor</h4>
                            </div>
                            <form class="user" action="/distributor/updateprofile" method="POST">
                                @csrf
                                @foreach ($dataprofile as $item)
                                    <div class="row mb-3">
                                        <div class="input-group"><span class="input-group-text">Nama</span><input class="form-control" type="text" name="nama_distributor" value="{{$item->nama_user}}"></div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="input-group"><span class="input-group-text">Nama Toko</span><input class="form-control" type="text" name="nama_toko" value="{{$item->nama_toko}}"></div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="input-group" style="padding-bottom: 15px;"><span class="input-group-text">Email</span><input class="form-control" type="email" name="email_distributor" value="{{$item->email_user}}"></div>
                                        <div class="input-group" style="padding-bottom: 15px;"><span class="input-group-text">Alamat</span><input class="form-control" type="text" name="alamat_distributor" value="{{$item->alamat_toko}}"></div>
                                        <div class="input-group"><span class="input-group-text">Nomor Whatsapp</span><input class="form-control" type="text" placeholder="+6283897564221" name="nomor_distributor" value="{{$item->no_tlp_distributor}}"></div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col-sm-6 mb-3 mb-sm-0">
                                            <div class="input-group"><span class="input-group-text">Ubah Password</span><input class="form-control" type="password"></div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="input-group"><span class="input-group-text">Repeat Password</span><input class="form-control" type="password"></div>
                                        </div>
                                    </div>
                                    
                                    <button class="btn btn-primary d-block btn-user w-100" type="submit">Update Account</button>
                                    <br>
                                    <a href="/distributor/indexdistributor"  class="btn btn-danger d-block btn-user w-100">Back</a>
                                @endforeach
                                <hr>
                            </form>
                            <div class="text-center"></div>
                            <div class="text-center"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="{{url('asset/js/bs-init.js')}}"></script>
    <script src="{{url('asset/js/theme.js')}}"></script>
    <script src="{{url('assets/bootstrap/js/bootstrap.min.css')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/bs-init.js?h=e2b0d57f2c4a9b0d13919304f87f79ae"></script>
    <script src="assets/js/theme.js?h=79f403485707cf2617c5bc5a2d386bb0"></script>
</body>
@endsection