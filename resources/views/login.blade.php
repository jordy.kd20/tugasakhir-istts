@extends('template')
<head>
    <title>Login Distributor</title>
</head>
@section('Content')
<body class="bg-gradient-primary" style="background: rgb(78, 115, 223);">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-9 col-lg-12 col-xl-10" style="margin-top: 17px;">
                <div class="card shadow-lg o-hidden border-0 my-5">
                    <div class="card-body p-0">
                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-flex">
                                <div class="flex-grow-1 bg-login-image" style="background: {{url('asset/img/Logo.png')}} center / contain no-repeat;"><img src="{{url('asset/img/logo.jpg')}}" alt="" srcset="" style="width: 100%; height: 100%; center; padding-top: 40px; padding-bottom: 40px;"></div>
                            </div>
                            <div class="col-lg-6">
                                <div class="d-block p-5" style="margin-top: 75px;margin-bottom: 75px;">
                                    <div class="text-center">
                                        <h4 class="text-dark mb-4">CV. Optimus Cahaya Abadi</h4>
                                    </div>
                                    <form class="user" action="/distributor/logcheck" method="POST">
                                        @csrf
                                        <div class="mb-3"><input class="form-control form-control-user" type="email" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Email" name="email"></div>
                                        <div class="mb-3"><input class="form-control form-control-user" type="password" id="exampleInputPassword" placeholder="Password" name="password"></div>

                                        <div class="mb-3"> <button type="submit" class="btn btn-primary d-block btn-user w-100" style="margin-top: 0;font-size: 16px;height: 46px;">Login</button>
                                            <div class="custom-control custom-checkbox small"></div>
                                        </div>
                                    </form>
                                    <div class="mb-3"><a class="btn btn-primary d-block btn-user w-100" role="button" style="margin-top: 0;padding: 12px 10px;border-radius: 160px;height: 46px;" href="distributor/register">Register</a>
                                        <div class="custom-control custom-checkbox small"></div>
                                    </div>
                                    <div class="text-center"><a class="btn btn-primary d-block float-start btn-user w-100" role="button" style="margin-top: 0;padding: 12px 10px;border-radius: 160px;height: 46px;background: #db4a39;border-color: #db4a39;" href="{{route('google.login')}}">Login with Google<i class="fa fa-google" style="margin: -1px;margin-left: 7px;"></i></a></div>
                                    <div></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="../assets/js/bs-init.js"></script>
    <script src="../assets/js/theme.js"></script>
    </body>
@endsection