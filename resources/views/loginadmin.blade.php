@extends('template')
<head>
    <title>
        login admin 
    </title>
</head>
@section('Content')
<body class="bg-gradient-primary" style="background: rgb(78, 115, 223);">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-9 col-lg-12 col-xl-10" style="margin-top: 17px;">
                <div class="card shadow-lg o-hidden border-0 my-5">
                    <div class="card-body p-0">
                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-flex">
                                <div class="flex-grow-1 bg-login-image" style="background:  center / contain no-repeat;"><img src="{{url('asset/img/logo.jpg')}}" alt="" style="width:100%; center; padding-top: 40px; padding-bottom: 40px; padding-left: 20px"></div>
                            </div>
                            <div class="col-lg-6">
                                <div class="d-block p-5" style="margin-top: 75px;margin-bottom: 75px;">
                                    <div class="text-center">
                                        <h4 class="text-dark mb-4">CV. Optimus Cahaya Abadi</h4>
                                    </div>
                                    <form class="user" action="/loginpegawai" method="POST">
                                        @csrf
                                        <div class="mb-3"><input class="form-control form-control-user" type="text" id="exampleInputEmail"  placeholder="Username" name="username"></div>
                                        <div class="mb-3"><input class="form-control form-control-user" type="password" id="exampleInputPassword" placeholder="Password" name="password"></div>
                                        <div class="mb-3"><button class="btn btn-primary d-block btn-user w-100" type="submit">Login</button>
                                            <div class="custom-control custom-checkbox small"></div>
                                        </div>
                                    </form>
                                    <div class="mb-3">
                                        <div class="custom-control custom-checkbox small"></div>
                                    </div>
                                    <div class="text-center"></div>
                                    <div></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/js/bs-init.js"></script>
    <script src="assets/js/theme.js"></script>
</body>
@endsection