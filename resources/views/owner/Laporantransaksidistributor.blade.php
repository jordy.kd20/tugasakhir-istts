<Html>
<head>
    <title>Laporan Transaksi</title>
    <style>
      table {
  border-collapse: collapse;
  width: 100%;
}

th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {background-color: #f2f2f2;}
    </style>
</head>
    <body>
        <div class="container">
            <div class="row p-3" style="text-align: center">
                <h1>CV. Optimus Cahaya Abadi</h1>
                <h4>Jl. Komp Pergudangan Margomulyo Jaya Blok I-12</h4>
                <h4>Margomulyo - Surabaya</h4>
                <h4>Telp : 03199025157</h4>
            </div>
            <hr>
            <div class="row p-3" style="text-align: center">
                <h4>Laporan Transaksi Distributor</h4>
                <h4>Periode {{Date('d F Y', strtotime(Session::get('tgldari')))}} - {{Date('d F Y', strtotime(Session::get('tglsampai')))}}</h4>
            </div>
            <div class="row p-3">
                <table>
                    <thead class="table table-striped" >
                        <th>No</th>
                        <th>ID Pesanan</th>
                        <th>Nama Toko</th>
                        <th>Tanggal Transaksi</th>
                        <th>Tanggal Jatuh Tempo</th>
                        <th>Grand Total</th>
                        <th>Kurang Bayar</th>
                        <th>Status Pembayaran</th>
                    </thead>
                    <tbody>
                        @php
                            $ctr = 1;
                        @endphp
                        @foreach ($datahtrans as $item)
                            <tr>
                                <td>{{$ctr}}</td>
                                <td>{{$item->id_htransaksi}}</td>
                                <td>{{DB::table('user_distributor')->where('id_user',$item->id_userdistributor)->value('nama_toko')}}</td>
                                <td>{{Date('d F',strtotime($item->tgl_transaksi))}}</td>
                                <td>{{Date('d F',strtotime($item->tgl_jatuh_tempo))}}</td>
                                <td>Rp.{{number_format($item->grand_total)}}</td>
                                <td>Rp.{{number_format($item->kurang_bayar)}}</td>
                                <td>@if ($item->status_bayar == 1)
                                    Lunas
                                @else
                                    Belum Lunas
                                @endif</td>
                            </tr>
                            @php
                                $ctr+=1;
                            @endphp
                        @endforeach
                    </tbody>
                    
                </table>
            </div>
        </div>
    </body>
</Html>