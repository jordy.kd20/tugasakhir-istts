@extends('template')
<head>
    <title>
        List Pembelian Produk
    </title>
</head>
@section('Content')
<body id="page-top">
    <div id="wrapper">
        <nav class="navbar navbar-dark align-items-start sidebar sidebar-dark accordion bg-gradient-primary p-0" style="background: #171717;">
            <div class="container-fluid d-flex flex-column p-0"><a class="navbar-brand d-flex justify-content-center align-items-center sidebar-brand m-0" href="#">
                    <div class="sidebar-brand-icon rotate-n-15"><img src="{{url('asset/img/Logo.png')}}" style="width: 40px;height: 40px;transform: rotate(15deg);"></div>
                    <div class="sidebar-brand-text mx-3"><span style="font-size: 9px;">CV. Optimus Cahata ABADI</span></div>
                </a>
                <hr class="sidebar-divider my-0">
                <ul class="navbar-nav text-light" id="accordionSidebar">
                    <li class="nav-item"><a class="nav-link  " href="/owner"><i class="fas fa-tachometer-alt"></i><span>Transaksi Distributor</span></a></li>
                    <li class="nav-item"><a class="nav-link" href="/owner/listasset"><i class="fa fa-building"></i><span>List Asset</span></a>
                    <li class="nav-item"><a class="nav-link active" href="/owner/listpembelian"><i class="fas fa-table"></i><span>List Pembelian Produk</span></a></li>
                    <li class="nav-item"><a class="nav-link " href="/owner/listkaryawan"><i class="fas fa-user"></i><span>Karyawan</span></a></li>
                    <li class="nav-item"><a class="nav-link" href="/owner/listbarang"><i class="fas fa-table"></i><span>Stok Barang</span></a></li>
                    <li class="nav-item"><a class="nav-link" href="/owner/listdistributor"><i class="fas fa-user"></i><span>Distributor</span></a></li>
                    <li class="nav-item"><a class="nav-link " href="/owner/stokreport"><i class="fa fa-exchange"></i><span>Stok Report</span></a></li>
                    <li class="nav-item"><a class="nav-link " href="/owner/listhutang"><i class="fa fa-file"></i><span>List Hutang</span></a></li>
                </ul>
                <div class="text-center d-none d-md-inline"><button class="btn rounded-circle border-0" id="sidebarToggle" type="button"></button></div>
            </div>
        </nav>
        <div class="d-flex flex-column" id="content-wrapper">
            <div id="content">
                <nav class="navbar navbar-light navbar-expand bg-white shadow mb-4 topbar static-top">
                    <div class="container-fluid"><button class="btn btn-link d-md-none rounded-circle me-3" id="sidebarToggleTop" type="button"><i class="fas fa-bars"></i></button>
                        <ul class="navbar-nav flex-nowrap ms-auto">
                            <li class="nav-item dropdown d-sm-none no-arrow"><a class="dropdown-toggle nav-link" aria-expanded="false" data-bs-toggle="dropdown" href="#"><i class="fas fa-search"></i></a>
                                <div class="dropdown-menu dropdown-menu-end p-3 animated--grow-in" aria-labelledby="searchDropdown">
                                    <form class="me-auto navbar-search w-100">
                                        <div class="input-group"><input class="bg-light form-control border-0 small" type="text" placeholder="Search for ...">
                                            <div class="input-group-append"><button class="btn btn-primary py-0" type="button"><i class="fas fa-search"></i></button></div>
                                        </div>
                                    </form>
                                </div>
                            </li>
                            <li class="nav-item dropdown no-arrow">
                                <div class="nav-item dropdown no-arrow"><a class="dropdown-toggle nav-link" aria-expanded="false" data-bs-toggle="dropdown" href="#"><span class="d-none d-lg-inline me-2 text-gray-600 small">{{Session::get('active')}}</span><img class="border rounded-circle img-profile" src='{{url('/asset/img/avatars/avatar1.jpeg')}}'></a>
                                    <div class="dropdown-menu shadow dropdown-menu-end animated--grow-in"><a class="dropdown-item" href="/showprofilepegawai"><i class="fas fa-user fa-sm fa-fw me-2 text-gray-400"></i>&nbsp;Edit Profile</a><a class="dropdown-item" href="/logout"><i class="fas fa-sign-out-alt fa-sm fa-fw me-2 text-gray-400"></i>&nbsp;Logout</a></div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
                
                <div class="container-fluid">
                    <div class="d-sm-flex justify-content-between align-items-center mb-4">
                        <h3 class="text-dark mb-0">List Pembelian Produk</h3>
                    </div>
                    <div class="row">                       
                        <div class="card shadow mb-4">
                            <div class="card-header">
                                <div class="p-3">
                                    <form action="/owner/filtertglpembelian" method="post">
                                        @csrf
                                        <div class="input-group">
                                            <span class="input-group-text">Dari Tanggal</span><input type="date"
                                                name="tgldari" id="">&nbsp;
                                                - &nbsp;
                                            <span class="input-group-text">Sampai Tanggal</span><input type="date"
                                                name="tglsampai" id="">
                                            &nbsp; <button type="submit" class="btn btn-primary">Pilih</button>
                                            &nbsp; <a href="/owner/listpembelian" class="btn btn-warning">Reset Filter</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="card-body p-5">
                                <div class="row">
                                    <table id="example" class="table table-striped" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Id Pesanan</th>
                                                <th>Tgl Pembelian</th>
                                                <th>Nama Pegawai</th>
                                                <th>Keterangan</th>
                                                <th>status</th>
                                                <th>Action</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if ($isi == 1)
                                                @foreach ($hpembelian as $item)
                                                    <tr>
                                                        <td>{{$item->id_hpembelian}}</td>
                                                        <td>{{Date('d F Y',strtotime($item->tgl_pembelian))}}</td>
                                                        @php
                                                            $namapegawai = DB::table('pegawai')->where('id_pegawai',$item->userpegawai)->value('nama_pegawai');
                                                        @endphp
                                                        <td>{{$namapegawai}}</td>
                                                        <td></td>
                                                        <td>@if ($item->status==0)
                                                            Belum Diterima
                                                        @else
                                                            Sudah Diterima
                                                        @endif</td>
                                                        <td>
                                                            <a href="/admin/listpembelian/detail/{{$item->id_hpembelian}}" class="btn btn-secondary"><i class="fa fa-info"></i></a>
                                                            <a href="/admin/listpembelian/print/{{$item->id_hpembelian}}" class="btn btn-warning"><i class="fa fa-print"></i></a>
                                                        </td>
                                                    </tr>
                                                @endforeach                                                
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        </div><a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a>
    </div>
    <script>
        $(document).ready(function () {
        $('#example').DataTable();
    });
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/bs-init.js?h=e2b0d57f2c4a9b0d13919304f87f79ae"></script>
    <script src="{{url('asset/js/bs-init.js')}}"></script>
    <script src="{{url('asset/js/theme.js')}}"></script>
    <script src="{{url('assets/bootstrap/js/bootstrap.min.css')}}"></script>
    
  
    <script src="assets/js/theme.js?h=79f403485707cf2617c5bc5a2d386bb0"></script>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="/assets/js/theme.js?h=79f403485707cf2617c5bc5a2d386bb0"></script>
</body>
@endsection