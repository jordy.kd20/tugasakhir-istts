@extends('template')
<head>
    <title>List Barang</title>
</head>
@section('Content')
<body id="page-top">
    <div id="wrapper">
        <nav class="navbar navbar-dark align-items-start sidebar sidebar-dark accordion bg-gradient-primary p-0" style="padding-right: 0px;background: #171717;">
            <div class="container-fluid d-flex flex-column p-0"><a class="navbar-brand d-flex justify-content-center align-items-center sidebar-brand m-0" href="#">
                <div class="sidebar-brand-icon rotate-n-15" style="margin-right: -11px;"><img src="{{url('asset/img/logo.png')}}" style="width: 40px;height: 40px;transform: rotate(15deg);"></div>
                <div class="sidebar-brand-text mx-3"><span style="font-size: 9px;">CV. OPTIMUS CAHAYA ABADI</span></div>
            </a>
            <ul class="navbar-nav text-light" id="accordionSidebar">
                <li class="nav-item"><a class="nav-link  " href="/owner"><i class="fas fa-tachometer-alt"></i><span>Transaksi Distributor</span></a></li>
                <li class="nav-item"><a class="nav-link" href="/owner/listasset"><i class="fa fa-building"></i><span>List Asset</span></a>
                <li class="nav-item"><a class="nav-link " href="/owner/listpembelian"><i class="fas fa-table"></i><span>List Pembelian Produk</span></a></li>
                <li class="nav-item"><a class="nav-link " href="/owner/listkaryawan"><i class="fas fa-user"></i><span>Karyawan</span></a></li>
                <li class="nav-item"><a class="nav-link active" href="/owner/listbarang"><i class="fas fa-table"></i><span>Stok Barang</span></a></li>
                <li class="nav-item"><a class="nav-link" href="/owner/listdistributor"><i class="fas fa-user"></i><span>Distributor</span></a></li>
                <li class="nav-item"><a class="nav-link " href="/owner/stokreport"><i class="fa fa-exchange"></i><span>Stok Report</span></a></li>
                <li class="nav-item"><a class="nav-link " href="/owner/listhutang"><i class="fa fa-file"></i><span>List Hutang</span></a></li>
            </ul>
            <hr class="sidebar-divider my-0">
            <div class="text-center d-none d-md-inline"><button class="btn rounded-circle border-0" id="sidebarToggle" type="button"></button></div>
        </div>
        </nav>
        <div class="d-flex flex-column" id="content-wrapper">
            <div id="content">
                <nav class="navbar navbar-light navbar-expand bg-white shadow mb-4 topbar static-top">
                    <div class="container-fluid"><button class="btn btn-link d-md-none rounded-circle me-3" id="sidebarToggleTop" type="button"><i class="fas fa-bars"></i></button>
                        <ul class="navbar-nav flex-nowrap ms-auto">
                            <li class="nav-item dropdown d-sm-none no-arrow"><a class="dropdown-toggle nav-link" aria-expanded="false" data-bs-toggle="dropdown" href="#"><i class="fas fa-search"></i></a>
                                <div class="dropdown-menu dropdown-menu-end p-3 animated--grow-in" aria-labelledby="searchDropdown">
                                    <form class="me-auto navbar-search w-100">
                                        <div class="input-group"><input class="bg-light form-control border-0 small" type="text" placeholder="Search for ...">
                                            <div class="input-group-append"><button class="btn btn-primary py-0" type="button"><i class="fas fa-search"></i></button></div>
                                        </div>
                                    </form>
                                </div>
                            </li>
                            <li class="nav-item dropdown no-arrow">
                                <div class="nav-item dropdown no-arrow"><a class="dropdown-toggle nav-link" aria-expanded="false" data-bs-toggle="dropdown" href="#"><span class="d-none d-lg-inline me-2 text-gray-600 small">{{Session::get('active')}}</span><img class="border rounded-circle img-profile" src={{url('asset/img/avatars/avatar1.jpeg')}}></a>
                                    <div class="dropdown-menu shadow dropdown-menu-end animated--grow-in"><a class="dropdown-item" href="/showprofilepegawai"><i class="fas fa-user fa-sm fa-fw me-2 text-gray-400"></i>&nbsp;Edit Profile</a><a class="dropdown-item" href="/logout"><i class="fas fa-sign-out-alt fa-sm fa-fw me-2 text-gray-400"></i>&nbsp;Logout</a></div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
                <div class="container-fluid">
                    <h3 class="text-dark mb-4">Barang</h3>
                    <div class="card shadow">
                        <div class="card-header d-sm-flex py-3"><button class="btn btn-primary d-flex d-sm-flex d-md-flex d-lg-flex d-xl-flex d-xxl-flex flex-row-reverse justify-content-center align-items-center flex-sm-row-reverse flex-md-row-reverse flex-lg-row-reverse flex-xl-row-reverse justify-content-xl-center align-items-xl-center flex-xxl-row-reverse" type="button" data-bs-target="#modal-insert-barang" data-bs-toggle="modal" style="margin-right: 10px;margin-bottom: 10px;">Tambah Barang<i class="fa fa-plus" style="margin-left: 0px;margin-right: 10px;"></i></button><button class="btn btn-primary d-flex d-sm-flex d-md-flex d-lg-flex d-xl-flex d-xxl-flex flex-row-reverse justify-content-center align-items-center flex-sm-row-reverse flex-md-row-reverse flex-lg-row-reverse flex-xl-row-reverse justify-content-xl-center align-items-xl-center flex-xxl-row-reverse" type="button" data-bs-target="#modal-insert-kategori" data-bs-toggle="modal" style="margin-right: 10px;margin-bottom: 10px;">Tambah Kategori<i class="fa fa-plus" style="margin-left: 0px;margin-right: 10px;"></i></button></div>
                        <div class="card-body">
                            {{-- <div class="row">
                                <div class="col-md-6 text-nowrap">
                                    <div id="dataTable_length" class="dataTables_length" aria-controls="dataTable"><label class="form-label">Show&nbsp;<select class="d-inline-block form-select form-select-sm">
                                                <option value="10" selected="">10</option>
                                                <option value="25">25</option>
                                                <option value="50">50</option>
                                                <option value="100">100</option>
                                            </select>&nbsp;</label></div>
                                </div>
                                <div class="col-md-6">
                                    <div class="text-md-end dataTables_filter" id="dataTable_filter"><label class="form-label"><input type="search" class="form-control form-control-sm" aria-controls="dataTable" placeholder="Search"></label></div>
                                </div>
                            </div> --}}
                            <div class="table-responsive table mt-2" id="dataTable" role="grid" aria-describedby="dataTable_info">
                                <table class="table table-hover table-bordered my-0" id="example">
                                    <thead class="text-nowrap">
                                        <tr>
                                            <th style="padding-right: 40px;padding-left: 40px;">Nama Barang</th>
                                            <th>Stok</th>
                                            <th>Warna Lampu</th>
                                            <th>Kategori</th>
                                            <th>Daya</th>
                                            <th>Harga Produk</th>
                                            <th>Jumlah per Dus</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                            @if ($isi==1)
                                                @foreach ($dataproduk as $item)
                                                <tr>
                                                    <td><img class="rounded-circle me-2" width="30" height="30" src={{url("$item->foto_produk")}}>{{$item->nama_produk}}</td>
                                                    <td>{{$item->stok_produk}}</td>
                                                    <td>{{$item->warna_lampu}}</td>
                                                    <td>{{$item->kategori_produk}}</td>
                                                    <td>{{$item->daya}} Watt</td>
                                                    <td>Rp. {{number_format($item->harga_produk)}}</td>
                                                    <td>{{$item->jumlahperdus}} /dus</td>
                                                    <td class="d-flex justify-content-xxl-center">  
                                                        <!-- <button class="btn btn-success" type="submit">Update</button> -->
                                                        <a class="btn btn-warning" href='/owner/updatebarang/{{$item->id_produk}}'><i class="fa fa-edit"></i></a>
                                                        &nbsp;
                                                        @if ($item->status_produk==0)
                                                            <a href="/owner/disabledbarang/{{$item->id_produk}}" class="btn btn-danger">Disabled</a>
                                                        @else
                                                            <a href="/owner/aktifbarang/{{$item->id_produk}}" class="btn btn-success">Aktif</a>
                                                        @endif
                                                    </td>
                                                </tr>
                                                @endforeach
                                            @endif
                                    </tbody>
                                </table>
                            </div>
                            {{-- <div class="row">
                                <div class="col-md-6 align-self-center">
                                    <p id="dataTable_info" class="dataTables_info" role="status" aria-live="polite">Showing 1 to 10 of 27</p>
                                </div>
                                <div class="col-md-6">
                                    <nav class="d-lg-flex justify-content-lg-end dataTables_paginate paging_simple_numbers">
                                        <ul class="pagination">
                                            <li class="page-item disabled"><a class="page-link" href="#" aria-label="Previous"><span aria-hidden="true">«</span></a></li>
                                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                                            <li class="page-item"><a class="page-link" href="#" aria-label="Next"><span aria-hidden="true">»</span></a></li>
                                        </ul>
                                    </nav>
                                </div>
                            </div> --}}
                        </div>
                    </div>
                    <div class="modal fade" role="dialog" tabindex="-1" id="modal-insert-barang">
                        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">Tambah Barang</h4><button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <form>
                                        <div class="row row-cols-1 row-cols-sm-1 row-cols-md-1 row-cols-lg-2 row-cols-xl-2 row-cols-xxl-2 d-lg-flex d-xl-flex d-xxl-flex align-items-md-end align-items-lg-end align-items-xl-end align-items-xxl-end">
                                            <div class="col">
                                                <div class="input-group"><span class="input-group-text">Nama Barang</span><input class="form-control" type="text"></div>
                                            </div>
                                            <div class="col">
                                                <div class="input-group" style="padding-top: 10px;"><span class="input-group-text">ID Barang</span><input class="form-control" type="text" readonly=""></div>
                                            </div>
                                        </div>
                                        <div class="row row-cols-1 row-cols-sm-1 row-cols-md-1 row-cols-lg-2 row-cols-xl-2 row-cols-xxl-2 d-lg-flex d-xl-flex d-xxl-flex align-items-md-end align-items-lg-end align-items-xl-end align-items-xxl-end">
                                            <div class="col">
                                                <div class="input-group" style="padding-top: 10px;"><span class="input-group-text">Harga Beli</span><input class="form-control" type="text"></div>
                                            </div>
                                            <div class="col">
                                                <div class="input-group" style="padding-top: 10px;"><span class="input-group-text">Harga Jual</span><input class="form-control" type="text"></div>
                                            </div>
                                        </div>
                                        <div class="input-group" style="padding-top: 10px;"><span class="input-group-text">Stok Barang</span><input class="form-control" type="text"></div>
                                        <div class="input-group" style="padding-top: 10px;"><span class="input-group-text">Stok Minimum</span><input class="form-control" type="text"></div><select class="form-select" style="margin-top: 10px;">
                                            <optgroup label="Kategori">
                                                <option value="12">kategori 1</option>
                                                <option value="13">kategori 2</option>
                                                <option value="14">kategori 3</option>
                                            </optgroup>
                                        </select>
                                        <div class="input-group" style="padding-top: 10px;"><span class="input-group-text">Distributor</span><input class="form-control" type="text"></div>
                                        <div class="input-group" style="padding-top: 10px;"><span class="input-group-text">Nomor Distributor</span><input class="form-control" type="text"></div>
                                        <div class="input-group" style="padding-top: 10px;"><span class="input-group-text">Email Dsitributor</span><input class="form-control" type="text"></div>
                                    </form>
                                    <div class="row">
                                        <div class="col"><input type="file" style="margin-top: 10px;"></div>
                                        <div class="col"><img class="d-lg-flex ms-auto" style="margin-top: 10px;width: 200px;height: 200px;"></div>
                                    </div>
                                </div>
                                <div class="modal-footer"><button class="btn btn-light" type="button" data-bs-dismiss="modal">Close</button><button class="btn btn-primary" type="button">Tambah Barang</button></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" role="dialog" tabindex="-1" id="modal-insert-kategori">
                        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">Modal Title</h4><button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <form>
                                        <div class="input-group"><span class="input-group-text">Kategori</span><input class="form-control" type="text"></div>
                                    </form>
                                    <div class="input-group" style="margin-top: 10px;"><span class="input-group-text">ID Kategori</span><input class="form-control" type="text"></div>
                                </div>
                                <div class="modal-footer"><button class="btn btn-light" type="button" data-bs-dismiss="modal">Close</button><button class="btn btn-primary" type="button">Tambah Kategori</button></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" role="dialog" tabindex="-1" id="modal-update-barang">
                        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">Update Barang</h4><button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <form>
                                        <div class="row row-cols-1 row-cols-sm-1 row-cols-md-1 row-cols-lg-2 row-cols-xl-2 row-cols-xxl-2 d-lg-flex d-xl-flex d-xxl-flex align-items-md-end align-items-lg-end align-items-xl-end align-items-xxl-end">
                                            <div class="col">
                                                <div class="input-group"><span class="input-group-text">Nama Barang</span><input class="form-control" type="text" readonly=""></div>
                                            </div>
                                            <div class="col">
                                                <div class="input-group" style="padding-top: 10px;"><span class="input-group-text">ID Barang</span><input class="form-control" type="text" readonly=""></div>
                                            </div>
                                        </div>
                                        <div class="row row-cols-1 row-cols-sm-1 row-cols-md-1 row-cols-lg-2 row-cols-xl-2 row-cols-xxl-2 d-lg-flex d-xl-flex d-xxl-flex align-items-md-end align-items-lg-end align-items-xl-end align-items-xxl-end">
                                            <div class="col">
                                                <div class="input-group" style="padding-top: 10px;"><span class="input-group-text">Harga Beli</span><input class="form-control" type="text"></div>
                                            </div>
                                            <div class="col">
                                                <div class="input-group" style="padding-top: 10px;"><span class="input-group-text">Harga Jual</span><input class="form-control" type="text"></div>
                                            </div>
                                        </div>
                                        <div class="input-group" style="padding-top: 10px;"><span class="input-group-text">Stok Barang</span><input class="form-control" type="text"></div>
                                        <div class="input-group" style="padding-top: 10px;"><span class="input-group-text">Stok Minimum</span><input class="form-control" type="text"></div><select class="form-select" style="margin-top: 10px;" readonly="" disabled="">
                                            <optgroup label="Kategori">
                                                <option value="12">kategori 1</option>
                                                <option value="13">kategori 2</option>
                                                <option value="14">kategori 3</option>
                                            </optgroup>
                                        </select>
                                        <div class="input-group" style="padding-top: 10px;"><span class="input-group-text">Distributor</span><input class="form-control" type="text"></div>
                                        <div class="input-group" style="padding-top: 10px;"><span class="input-group-text">Nomor Distributor</span><input class="form-control" type="text"></div>
                                        <div class="input-group" style="padding-top: 10px;"><span class="input-group-text">Email Dsitributor</span><input class="form-control" type="text"></div>
                                    </form>
                                </div>
                                <div class="modal-footer"><button class="btn btn-light" type="button" data-bs-dismiss="modal">Close</button><button class="btn btn-primary" type="button">Ubah</button></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="bg-white sticky-footer">
                <div class="container my-auto">
                    <div class="text-center my-auto copyright"><span>Copyright © Elektronik Home 2021</span></div>
                </div>
            </footer>
        </div><a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a>
    </div>
    <script>
        $(document).ready(function () {
        $('#example').DataTable();
    });
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/bs-init.js?h=e2b0d57f2c4a9b0d13919304f87f79ae"></script>
    <script src="{{url('asset/js/bs-init.js')}}"></script>
    <script src="{{url('asset/js/theme.js')}}"></script>
    <script src="{{url('assets/bootstrap/js/bootstrap.min.css')}}"></script>
    
  
    <script src="assets/js/theme.js?h=79f403485707cf2617c5bc5a2d386bb0"></script>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="/assets/js/theme.js?h=79f403485707cf2617c5bc5a2d386bb0"></script>
</body>
@endsection