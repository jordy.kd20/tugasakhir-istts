@extends('template')
<head>
    <title>List Hutang</title>
</head>
@section('Content')
<body id="page-top">
    <div id="wrapper">
        <nav class="navbar navbar-dark align-items-start sidebar sidebar-dark accordion bg-gradient-primary p-0" style="background: #171717;">
            <div class="container-fluid d-flex flex-column p-0"><a class="navbar-brand d-flex justify-content-center align-items-center sidebar-brand m-0" href="#">
                    <div class="sidebar-brand-icon rotate-n-15"><img src={{url('asset/img/Logo.png')}} style="width: 40px;height: 40px;transform: rotate(15deg);"></div>
                    <div class="sidebar-brand-text mx-3"><span style="font-size: 9px;">CV. Optimus Cahata ABADI</span></div>
                </a>
                <hr class="sidebar-divider my-0">
                <ul class="navbar-nav text-light" id="accordionSidebar">
                    <li class="nav-item"><a class="nav-link  " href="/owner"><i class="fas fa-tachometer-alt"></i><span>Transaksi Distributor</span></a></li>
                    <li class="nav-item"><a class="nav-link" href="/owner/listasset"><i class="fa fa-building"></i><span>List Asset</span></a>
                <li class="nav-item"><a class="nav-link " href="/owner/listpembelian"><i class="fas fa-table"></i><span>List Pembelian Produk</span></a></li>
                <li class="nav-item"><a class="nav-link " href="/owner/listkaryawan"><i class="fas fa-user"></i><span>Karyawan</span></a></li>
                <li class="nav-item"><a class="nav-link" href="/owner/listbarang"><i class="fas fa-table"></i><span>Stok Barang</span></a></li>
                <li class="nav-item"><a class="nav-link" href="/owner/listdistributor"><i class="fas fa-user"></i><span>Distributor</span></a></li>
                <li class="nav-item"><a class="nav-link " href="/owner/stokreport"><i class="fa fa-exchange"></i><span>Stok Report</span></a></li>
                <li class="nav-item"><a class="nav-link active" href="/owner/listhutang"><i class="fa fa-file"></i><span>List Hutang</span></a></li>
                    
                </ul>
                <div class="text-center d-none d-md-inline"><button class="btn rounded-circle border-0" id="sidebarToggle" type="button"></button></div>
            </div>
        </nav>
        <div class="d-flex flex-column" id="content-wrapper">
            <div id="content">
                <nav class="navbar navbar-light navbar-expand bg-white shadow mb-4 topbar static-top">
                    <div class="container-fluid"><button class="btn btn-link d-md-none rounded-circle me-3" id="sidebarToggleTop" type="button"><i class="fas fa-bars"></i></button>
                        <ul class="navbar-nav flex-nowrap ms-auto">
                            <li class="nav-item dropdown d-sm-none no-arrow"><a class="dropdown-toggle nav-link" aria-expanded="false" data-bs-toggle="dropdown" href="#"><i class="fas fa-search"></i></a>
                                <div class="dropdown-menu dropdown-menu-end p-3 animated--grow-in" aria-labelledby="searchDropdown">
                                    <form class="me-auto navbar-search w-100">
                                        <div class="input-group"><input class="bg-light form-control border-0 small" type="text" placeholder="Search for ...">
                                            <div class="input-group-append"><button class="btn btn-primary py-0" type="button"><i class="fas fa-search"></i></button></div>
                                        </div>
                                    </form>
                                </div>
                            </li>
                            <li class="nav-item dropdown no-arrow">
                                <div class="nav-item dropdown no-arrow"><a class="dropdown-toggle nav-link" aria-expanded="false" data-bs-toggle="dropdown" href="#"><span class="d-none d-lg-inline me-2 text-gray-600 small">{{Session::get('active')}}</span><img class="border rounded-circle img-profile" src={{url('/asset/img/avatars/avatar1.jpeg')}}></a>
                                    <div class="dropdown-menu shadow dropdown-menu-end animated--grow-in"><a class="dropdown-item" href="/showprofilepegawai"><i class="fas fa-user fa-sm fa-fw me-2 text-gray-400"></i>&nbsp;Edit Profile</a><a class="dropdown-item" href="/logout"><i class="fas fa-sign-out-alt fa-sm fa-fw me-2 text-gray-400"></i>&nbsp;Logout</a></div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
                <div class="container-fluid" style="padding-right: 0px;padding-left: 0px;">
                    <div class="row">
                        <div class="col">
                            <h4 class="text-dark mb-1" style="padding-left: 10px;">List Hutang Distributor</h4>
                        </div>
                        <div class="col">
                            {{-- <div class="d-md-flex justify-content-md-end align-items-md-center" style="padding-right: 10px;"><a class="btn btn-warning" href="/admin/reminderdistributor" >Reminder Distributor</a></div> --}}
                        </div>
                    </div>

                    <div class="p-3 pt-5">
                        <table class="table table-striped " id="myTable"  style="text-align: center"> 
                            <thead>
                                <tr>
                                    <th>ID Pesanan</th>
                                    <th>Nama Toko</th>                                   
                                    <th>Tanggal Jatuh Tempo</th>
                                    <th>Kurang Bayar</th>
                                    <th>Detail Produk</th>
                                    {{-- <th>Edit Pembayaran</th> --}}
                                </tr>
                            </thead>
                            <tbody >
                                @if ($count!=0)
                                    @foreach ($listhutang as $item)
                                        <tr>
                                            <td>{{$item->id_htransaksi}}</td>
                                            <td>{{DB::table('user_distributor')->where('id_user',$item->id_userdistributor)->value('nama_toko')}}</td>
                                            <td>{{Date('d F Y',strtotime($item->tgl_jatuh_tempo))}}</td>
                                            <td>Rp. {{number_format($item->kurang_bayar)}}</td>
                                            <td><a class="btn btn-primary" href="/admin/detailpesananhutang/{{$item->id_htransaksi}}">detail pembelian</a></td>
                                            {{-- <td><button class="btn btn-warning" data-id_nota="{{$item->id_htransaksi}}" data-kurang_bayar={{$item->kurang_bayar}} data-bs-toggle="modal" data-bs-target="#edit"><i class="fa fa-edit"></i></button></td> --}}
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="container-fluid" style="padding-right: 0px;padding-left: 0px;">
                    <div class="row">
                        <div class="col">
                            <div>
                                <div class="modal fade" role="dialog" tabindex="-1" id="edit">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">Update Pembayaran</h4><button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <form action="/admin/updatepembayaranhutang" method="post" enctype="multipart/form-data">
                                                @csrf
                                                <div class="modal-body">
                                                    <div class="input-group" style="padding-bottom: 10px;"><span class="input-group-text">ID Nota</span><input class="form-control" id="id_nota" name="id_nota" type="text" readonly=""></div>
                                                    <div class="input-group" style="padding-bottom: 10px;"><span class="input-group-text">Jumlah Kurang Bayar</span><input class="form-control" name="kurang_bayar" id="kurang_bayar" type="text" readonly=""></div>
                                                    <div class="input-group" style="padding-bottom: 10px;"><span class="input-group-text">Jumlah yang dibayarkan</span><input class="form-control" type="number" name="jumlahdibayarkan"></div>
                                                    *Upload Foto Bukti Pembayaran
                                                    <div class="input-group" style="padding-Top: 10px;"><input class="form-control" type="file" name="fotopembayaran"></div>
                                                </div>
                                                <div class="modal-footer"><button class="btn btn-light" type="button" data-bs-dismiss="modal">Close</button><button class="btn btn-primary" type="submit">Submit</button></div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div></div>
                </div>
            </div>
            <footer class="bg-white sticky-footer">
                <div class="container my-auto">
                    <div class="text-center my-auto copyright"><span>Copyright © CV. Optimus Cahaya Abadi 2022</span></div>
                </div>
            </footer>
        </div><a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a>
    </div>
    <script>
        $(document).ready(function () {
        $('#myTable').DataTable();
    });
    </script>
    <script>
        $('#edit').on('show.bs.modal', function(event) {
            var button = $(event.relatedTarget)
            var id_nota = button.data('id_nota')
            var kurang_bayar = button.data('kurang_bayar')

            var modal =$(this)
            modal.find('.modal-body #id_nota').val(id_nota);
            modal.find('.modal-body #kurang_bayar').val(kurang_bayar);
            // modal.find('.modal-body #showpic').val(foto);
            // $('.modal-body #showpic').attr('src') = foto;
        
            
        })
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="{{url('asset/js/bs-init.js')}}"></script>
    <script src="{{url('asset/js/theme.js')}}"></script>
    <script src="{{url('assets/bootstrap/js/bootstrap.min.css')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
</body>
@endsection