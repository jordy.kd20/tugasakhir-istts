@extends('template')
<head>
    <title>Update Pegawai</title>
</head>
@section('Content')
<body style="background: rgb(78,115,223);">
    <div class="container">
        <div class="card shadow-lg o-hidden border-0 my-5">
            <div class="card-body p-0">
                <div class="row d-lg-flex justify-content-lg-center align-items-lg-center">
                    <div class="col-lg-7 d-lg-flex" style="width: 870px;">
                        <div class="p-5">
                            <div class="text-center">
                                <h4 class="text-dark mb-4">Update Profile</h4>
                            </div>
                            <form class="user" method="POST" action="/updateprofilepegawai">
                                @csrf
                                @foreach ($datapegawai as $item)
                                <div class="row mb-3">
                                    <div class="input-group"><span class="input-group-text">Nama</span><input class="form-control" type="text" name="nama_pegawai" value="{{$item->nama_pegawai}}" readonly></div>
                                </div>
                                <div class="row mb-3">
                                    <div class="input-group"><span class="input-group-text">Username</span><input class="form-control" type="text" name="username_pegawai" value="{{$item->username_pegawai}}" readonly></div>
                                </div>
                                <div class="mb-3">
                                    <div class="input-group" style="padding-bottom: 15px;"><span class="input-group-text">Email</span><input class="form-control" type="email" name="email_pegawai" value="{{$item->email_pegawai}}"></div>
                                    <div class="input-group" style="padding-bottom: 15px;"><span class="input-group-text">Alamat</span><input class="form-control" type="text" name="alamat_pegawai" value="{{$item->alamat_pegawai}}"></div>
                                    <div class="input-group"><span class="input-group-text">Nomor Telephone</span><input class="form-control" type="text" placeholder="+6283897564221" name="nomor_pegawai" value="{{$item->telephone_pegawai}}"></div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <div class="input-group"><span class="input-group-text">Ubah Password</span><input class="form-control" type="password" name="password_baru"></div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="input-group"><span class="input-group-text">Repeat Password</span><input class="form-control" type="password" name="confirm_password"></div>
                                    </div>
                                </div><button class="btn btn-primary d-block btn-user w-100" type="submit">Update Account</button>
                                <hr>
                                @endforeach
                            </form>
                            <div class="text-center"></div>
                            <div class="text-center"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="{{url('asset/js/bs-init.js')}}"></script>
    <script src="{{url('asset/js/theme.js')}}"></script>
    <script src="{{url('assets/bootstrap/js/bootstrap.min.css')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
</body>
@endsection