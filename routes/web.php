<?php

use App\Http\Controllers\admincontroler;
use App\Http\Controllers\distributorcontroler;
use App\Http\Controllers\googlecontroler;
use App\Http\Controllers\maincontroler;
use App\Http\Controllers\ownercontroler;
use App\Http\Controllers\whatsappcontroler;
use App\Models\distributor;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/lupapaswword/{id}/{pass}',[maincontroler::class, 'lupapasword']);
Route::get('/cekcart', [\App\Http\Controllers\distributorcontroler::class, 'cekcart']);
Route::get('/province/{id}/cities', [\App\Http\Controllers\distributorcontroler::class, 'getCities']);
Route::get('/cekongkir/{city}/{courierid}/{berat}/submit', [distributorcontroler::class, 'jqongkir']);
Route::post('/cekongkir', [\App\Http\Controllers\distributorcontroler::class, 'submitongkir']);

Route::get('/', function () {
    return view('login');
});
Route::get('/logout', [maincontroler::class, 'logout']);


Route::get("/dashboard", [maincontroler::class, 'loginmasuk']);
Route::post('/loginpegawai', [maincontroler::class, 'loginpegawai']);
Route::get('auth/google/callback', [googlecontroler::class, 'handleGoogleCallback']);

Route::get('/detailpesanan/{id}', [admincontroler::class, 'detailpesanan']);
Route::get('/detailretur/{id}', [admincontroler::class, 'detailretur']);

Route::get('/sentwacoba', [whatsappcontroler::class, 'sentwa']);

Route::get('/cekpassword', [maincontroler::class, 'cekpass']);


Route::get('/showprofilepegawai', [maincontroler::class, 'showprofile']);
Route::post('/updateprofilepegawai', [maincontroler::class, 'updateprofile']);

Route::group(['prefix' => 'distributor'], function () {
    Route::view('/', 'indexdistributor');
    Route::get('/logout', [distributorcontroler::class, 'logoutdistributor']);
    Route::get('auth/google', [googlecontroler::class, 'redirectToGoogle'])->name('google.login');
    Route::get('/indexdistributor', [distributorcontroler::class, 'berhasillogingoogle']);
    Route::post('/searchproduk', [distributorcontroler::class, 'searchproduk']);
    Route::post('/logcheck', [distributorcontroler::class, 'loginemail']);
    Route::view('/register', 'distributor/registerdistributor');
    Route::post('/registerdistributor', [distributorcontroler::class, 'registrasidistributor']);
    Route::get('/profile', [distributorcontroler::class, 'profile']);
    Route::post('/updateprofile', [distributorcontroler::class, 'updateprofile']);
    Route::get('/cart', [distributorcontroler::class, 'cart']);
    Route::get('/detailproduk/{id}', [distributorcontroler::class, 'detailproduk']);
    Route::get('/addtochart/{id}', [distributorcontroler::class, 'addtocart']);
    Route::get('/tambahcart/{id}', [distributorcontroler::class, 'tambahcart']);
    Route::get('/kurangcart/{id}', [distributorcontroler::class, 'kurangcart']);
    Route::get('/hapuscart/{id}',[distributorcontroler::class, 'hapuscarttrans']);
    Route::post('/editcart', [distributorcontroler::class, 'editcart']);
    Route::post('/checkout', [distributorcontroler::class, 'checkout']);
    Route::post('/success/midtrans', [distributorcontroler::class, 'successpayment']);
    Route::post('/pembayaranupload', [distributorcontroler::class, 'pembayaranupload']);
    Route::get('/pembelian', [distributorcontroler::class, 'pembeliandistributor']);
    Route::get('/returproduk', [distributorcontroler::class, 'returproduk']);
    Route::post('/inputretur', [distributorcontroler::class, 'inputretur']);
    Route::post('/addretur', [distributorcontroler::class, 'addreturproduk']);
    Route::get('/hapusaddretur/{id}', [distributorcontroler::class, 'hapusaddproduk']);
    Route::post('/submitretur', [distributorcontroler::class, 'submitretur']);
    Route::get('/listpesanan/cetak/{id}', [distributorcontroler::class, 'cetaknotatransaksi']);
    Route::post('/sortkategori',[distributorcontroler::class, 'sortkategori']);
    Route::get('/listretur',[distributorcontroler::class, 'listretur']);
    Route::post('/filtertglretur',[distributorcontroler::class, 'filtertglretur']);
    Route::post('/filtertgltransaksi',[distributorcontroler::class, 'filtertgltransaksi']);
    Route::get('/cetaknotaretur/{id}',[distributorcontroler::class, 'cetaknotaretur']);

});

Route::group(['prefix' => 'owner'], function () {
    Route::get('/', [ownercontroler::class, 'transaksidistributor']);
    Route::get('/listkaryawan', [\App\Http\Controllers\ownercontroler::class, 'listkaryawan']);
    Route::get('/listbarang', [\App\Http\Controllers\ownercontroler::class, 'listbarang']);
    Route::get('/listdistributor', [ownercontroler::class, 'listdistributor']);
    Route::view('/tambahpegawai', 'owner/regispegawai');
    Route::post('/registercheck', [\App\Http\Controllers\ownercontroler::class, 'registerpegawai']);
    Route::get('/disabledpegawai/{id}', [\App\Http\Controllers\ownercontroler::class, 'disabledpegawai']);
    Route::get('/aktifpegawai/{id}', [\App\Http\Controllers\ownercontroler::class, 'aktifpegawai']);
    Route::get('/disabledbarang/{id}', [\App\Http\Controllers\ownercontroler::class, 'disabledproduk']);
    Route::get('/aktifbarang/{id}', [\App\Http\Controllers\ownercontroler::class, 'aktifproduk']);
    Route::get('/updatebarang/{id}', [\App\Http\Controllers\ownercontroler::class, 'updateproduk']);
    Route::post('/submitproduk', [\App\Http\Controllers\ownercontroler::class, 'submitupdateproduk']);
    Route::get('/aktifdistributor/{id}', [ownercontroler::class, 'aktifdistributor']);
    Route::get('/disableddistributor/{id}', [ownercontroler::class, 'disableddistributor']);
    Route::get('/stokreport', [ownercontroler::class, 'stokreport']);
    Route::get('/listhutang', [ownercontroler::class, 'listhutang']);
    Route::post('/filtertgltstok', [ownercontroler::class, 'filtertgltstok']);
    Route::post('/filtertglttransaksi',[ownercontroler::class, 'filtertglttransaksi']);
    Route::get('/listpembelian',[ownercontroler::class, 'listpembelian']);
    Route::post('/pdftransaksidistributor', [ownercontroler::class, 'pdftransaksidistributor']);
    Route::get('/listasset', [ownercontroler::class, 'listasset']);
});

Route::group(['prefix' => 'admin'], function () {
    Route::get('/', [\App\Http\Controllers\admincontroler::class, 'listbarang']);
    Route::get('/listasset', [\App\Http\Controllers\admincontroler::class, 'listasset']);
    Route::post('/tambahaset', [\App\Http\Controllers\admincontroler::class, 'tambahasset']);
    Route::post('/tambahkategori', [\App\Http\Controllers\admincontroler::class, 'tambahkategori']);
    Route::post('/tambahbarang', [\App\Http\Controllers\admincontroler::class, 'tambahbarang']);
    Route::get('/disabledbarang/{id}', [\App\Http\Controllers\admincontroler::class, 'disabledproduk']);
    Route::get('/aktifbarang/{id}', [\App\Http\Controllers\admincontroler::class, 'aktifproduk']);
    Route::get('/updatebarang/{id}', [\App\Http\Controllers\admincontroler::class, 'updateproduk']);
    Route::post('/submitproduk', [\App\Http\Controllers\admincontroler::class, 'submitupdateproduk']);
    Route::post('/searchproduk', [\App\Http\Controllers\admincontroler::class, 'searchproduk']);
    Route::get('/updateasset/{id}', [\App\Http\Controllers\admincontroler::class, 'updateasset']);
    Route::post('/submitasset', [\App\Http\Controllers\admincontroler::class, 'submitasset']);
    Route::post('/searchasset', [\App\Http\Controllers\admincontroler::class, 'searchasset']);
    Route::get('/pembelianproduk', [\App\Http\Controllers\admincontroler::class, 'pembelianproduk']);
    Route::post('/addbeliproduk', [\App\Http\Controllers\admincontroler::class, 'addbeliproduk']);
    Route::get('/cekpembelian', [\App\Http\Controllers\admincontroler::class, 'cekpembelian']);
    Route::post('/editpembelian', [\App\Http\Controllers\admincontroler::class, 'editpembelian']);
    Route::get('/hapuspembelian/{id}', [\App\Http\Controllers\admincontroler::class, 'hapuspembelian']);
    Route::post('/checkoutpembelian', [\App\Http\Controllers\admincontroler::class, 'checkoutpembelian']);
    Route::get('/listpembelian', [admincontroler::class, 'listpembelian']);
    Route::get('/listpembelian/detail/{id}', [admincontroler::class, 'detailpembelian']);
    Route::get('/listpembelian/print/{id}', [admincontroler::class, 'printdetailpembelian']);
    Route::get('/listpembelian/updatestatus/{id}/1', [admincontroler::class, 'updatestatuslistpembelian']);
    Route::get('/listpembeliandistributor', [admincontroler::class, 'listpembeliandistributor']);
    Route::get('/updatestatuspengiriman/{idhtrans}/{status}', [admincontroler::class, 'statuspengiriman']);
    Route::get('/listhutang', [admincontroler::class, 'listhutang']);
    Route::get('/detailpesananhutang/{id}', [admincontroler::class, 'detailpesananhutang']);
    Route::get('/listretur', [admincontroler::class, 'listretur']);
    Route::get('/updatestatuspengirimanretur/{idhretur}/{status}', [admincontroler::class, 'statuspengirimanretur']);
    Route::post('/updatepembayaranhutang', [admincontroler::class, 'updatepembayaranhutang']);
    Route::get('/reminderdistributor', [whatsappcontroler::class, 'sentreminder']);
    Route::get('/penguranganstok', [admincontroler::class, 'penguranganstok']);
    Route::post('/penguranganstok/updatestok', [admincontroler::class, 'submitpenguranganproduk']);
    Route::get('/listpesanan/cetak/{id}', [admincontroler::class, 'cetaknotatransaksi']);
    Route::post('/filtertgltransaksi',[admincontroler::class, 'filtertgltransaksi']);
    Route::post('/filtertglpembelian',[admincontroler::class, 'filtertglpembelian']);
    Route::get('/cetaknotaretur/{id}',[admincontroler::class, 'cetaknotaretur']);
    Route::get('/stokreport', [admincontroler::class, 'stokreport']);
    Route::post('/filtertgltstok', [admincontroler::class, 'filtertgltstok']);
});
